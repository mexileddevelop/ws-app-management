package com.mexiled.service;

import com.mexiled.exception.SectionException;
import com.mexiled.views.SectionView;

import java.util.List;

/**
 * @author Victor de la Cruz
 * @version 1.0.0
 */
public interface SectionService {

    /**
     * Método para agregar una nueva sección en la base de datos
     * @param sectionView vista con los datos de la seccióna agregar
     * @throws SectionException si ocurre un error al guardar la seccion
     * */
    void addSection(SectionView sectionView) throws SectionException;

    /**
     * Método para editar los datos de una sección en base de datos
     * @param sectionView datos de la sección a editar
     * @throws SectionException si no se encuentra la sección u ocurre un error al editar
     * */
    void updateSection(SectionView sectionView) throws SectionException;

    /**
     * Método para eliminar una sección de base de datos
     * @param sectionId identificador único de la sección
     * @throws SectionException si ocurre un error al eliminar la sección o no es posible eliminarla
     * */
    void deleteSection(Long sectionId) throws SectionException;

    /**
     * Método para obtener lops detalles de una sección
     * @param sectionId identificador único de seccion para seleccionar en base de datos
     * @return detalles de la seccion {@link SectionView}
     * @throws SectionException si ocurre un error al momento de realizar la query
     * */
    SectionView findSection(Long sectionId) throws SectionException;

    /**
     * Método para obtener la lista de secciones, con opciones de filtrado por proyectos y/o activos
     * @param projectId identificador de proyecto para obtener secciones - opcional
     * @param active opcion para filtrar secciones
     * @return Lista de secciones de tipo {@link SectionView}
     * @throws SectionException si ocurre un error al realizar la seleccion de las secciones
     * */
    List<SectionView> findSections(Long projectId,Boolean active) throws SectionException;


}
