package com.mexiled.service;

import com.mexiled.exception.UserAppException;
import com.mexiled.views.UserAppView;

import java.util.List;

public interface UserAppService {


    /**
     * Método para agrgar un nuevo usuario al sistema
     * @param userAppView datos de usuario que se van a guardar
     * @throws UserAppException si ocurre algún error al guardar el usuario
     * */
    void addUserApp(UserAppView userAppView) throws UserAppException;

    /**
     * Método para editar los datos de un usuario, también edita la contraseña si esta no viene vacia
     * @param userAppView datos de usuario que se van a editar
     * @throws UserAppException si ocurre algún error al momento de editar el usuario
     * */
    void updateUserApp(UserAppView userAppView) throws UserAppException;

    /**
     * Método para eliminar un usuario de sistema
     * @param userAppId identificador de usuario que se va a eliminar
     * @throws UserAppException Si ocurre algún error al eliminar el usuario
     * */
    void deleteUserApp(Long userAppId) throws UserAppException;

    /**
     * Método para cambiar el estado de un usuario, de activo a inactivo o viceversa
     * @param userAppId identificador único de usuario
     * @param active estado a establecer TRUE,FALSE
     * @throws UserAppException si no es posible cambiar el estado del usuario en base de datos
     * */
    void changeStatus(Long userAppId,Boolean active) throws UserAppException;

    /**
     * Método para obtener los detalles de un usuario a través del ID
     * @param userAppId ID de usuario a buscar
     * @return Detalles de usuario obtenidos de base de datos
     * @throws UserAppException si no se encuentra el usuario o no se puede seleccionar de base de datos
     * */
    UserAppView findById(Long userAppId) throws UserAppException;

    /**
     * Método para obtener los detalles de un usuario a traves del nombre de usuario
     * @param username nombre de usuario a buscar
     * @return Detalles de usuario obtenidos de base de datos
     * @throws UserAppException si no se encuentra en usuario o no se puede seleccioanr de base de datos
     * */
    UserAppView findByUsername(String username) throws UserAppException;

    /**
     * Método para obtener ls lista completa de los usarios que existen en la base de datos
     * @return Lista de usuarios
     * @throws UserAppException si no es posible realizar la selección de los usuarios
     * */
    List<UserAppView> findAll() throws UserAppException;
}
