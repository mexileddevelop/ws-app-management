package com.mexiled.service;

import com.mexiled.exception.ProfileException;
import com.mexiled.views.NodeTree;
import com.mexiled.views.ProfileView;

import java.util.List;

/**
 * @author Victor de la Cruz
 * @version 1.0.0
 * **/
public interface ProfileService {

    /**
     * Método para guardar un nuevo perfil en base de datos
     * @param profileView información de perfil junto con permisos a guardar
     * @throws ProfileException si ocurre un error al guardar la información de perfil
     * */
    void addProfile(ProfileView profileView) throws ProfileException;

    /**
     * Método para actualizar perfil junto con sus permisos
     * @param profileView información a actualizar
     * @throws ProfileException si ocurre algún error al actualizar la información del perfil
     * */
    void editProfile(ProfileView profileView) throws ProfileException;

    /**
     * Método para eliminar un perfil junto con su lista de permisos
     * @param profileId ID único de perfil a eliminar
     * @throws ProfileException si no es posible eliminar el perfil seleccionado
     * */
    void deleteProfile(Long profileId) throws ProfileException;

    /**
     * Método para obtener el detalle del perfil junto con sus permisos
     * @param profileId ID único de perfil a seleccionar
     * @throws ProfileException si no se encuentra el perfil o no es posible realizar la query
     * @return Vista parseada de perfil
     * */
    ProfileView findProfile(Long profileId) throws ProfileException;

    /**
     * Método para obtener lista completa de los perfiles en base de datos
     * @throws ProfileException si ocurre algún error al realizar la selección en base de datos
     * @return Lista de perfiles
     * */
    List<ProfileView> findAll() throws ProfileException;

    /**
     * Método para obtener la lista de permisos disponibles
     * @param profileId  parametro opcional para marcar los permisos del perfil
     * @return lista de permisos
     * @throws ProfileException si ocurre algún error al generar el arbol de permisos
     * */
    List<NodeTree> findProfileTree(Long profileId) throws ProfileException;
}
