package com.mexiled.service;


import com.mexiled.exception.PolygonException;
import com.mexiled.views.PolygonView;

import java.util.List;

/**
 * @author Victor de la Cruz
 * @version 1.0.0
 */
public interface PolygonService {

    /**
     * Método para agregar un nuevo poligon
     * @param polygonView datos de poligono a guardar
     * @throws PolygonException si no es posible guardar el nuevo poligono
     * */
    void addPolygon(PolygonView polygonView) throws PolygonException;

    /**
     * Método para editar los datos de un poligono
     * @param polygonView datos a editar del poligono
     * @throws PolygonException si ocurre un error al editar el poligono
     * */
    void updatePolygon(PolygonView polygonView) throws PolygonException;

    /**
     * Método para eliminar un poligono de la base de datos
     * @param polygonId identificador para eliminar poligono
     * @throws PolygonException si no es posible eliminar el poligono
     * */
    void deletePolygon(Long polygonId) throws PolygonException;

    /**
     * Método para obtener los detalles de un poligono
     * @param polygonId identificador del poligono a obtener
     * @return detalles seleccionados del poligono
     * @throws PolygonException si no es posible obtener el poligono
     * */
    PolygonView findById(Long polygonId) throws PolygonException;

    /**
     * Método para obtener lista de poligonos filtrados por seccion o activos
     * @param active filtro para buscar activos o  inactivos
     * @param sectionId filtro para buscar por seccion
     * @return lista de poligonos seleccionados
     * @throws PolygonException si no es posible realizar la selección en base de datos
     * */
    List<PolygonView> findPolygons(Long sectionId,Boolean active) throws PolygonException;
}
