package com.mexiled.service.impl;

import com.mexiled.converter.ModuleConverter;
import com.mexiled.domain.Module;
import com.mexiled.domain.ModulePermission;
import com.mexiled.exception.ExceptionCode;
import com.mexiled.exception.ModuleException;
import com.mexiled.repository.ModuleRepository;
import com.mexiled.service.ModuleService;
import com.mexiled.views.ModuleView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Victor de la Cruz
 * @version 1.0.0
 */
@Service()
@Transactional(readOnly = true)
public class ModuleServiceImpl implements ModuleService{

    private final Logger logger = LoggerFactory.getLogger(ModuleServiceImpl.class);

    private ModuleRepository moduleRepository;

    private ModuleConverter moduleConverter;

    @Autowired
    public void setModuleRepository(ModuleRepository moduleRepository) {
        this.moduleRepository = moduleRepository;
    }

    @Autowired
    public void setModuleConverter(ModuleConverter moduleConverter) {  this.moduleConverter = moduleConverter;}

    /**
     * {@inheritDoc}
     * */
    @Override
    @Transactional(readOnly = false,rollbackFor = ModuleException.class)
    public void addModule(ModuleView moduleView) throws ModuleException {
        try{
            if(moduleView.getPermissions().isEmpty())
                throw new ModuleException("La lista de permisos no debe estar vacia",ModuleException.LAYER.SER.name(),ModuleException.ACTION.VAL.name());
            Module module = moduleConverter.convertViewToEntity(moduleView,Boolean.FALSE);
            logger.debug("Insertando módulo a base  de datos: {}",module);
            moduleRepository.save(module);
        }catch (DataIntegrityViolationException dta){
            ModuleException moduleException = new ModuleException("Error al agregar el módulo a la base de datos",ModuleException.LAYER.DAO.name(),ModuleException.ACTION.VAL.name());
            moduleException.addError("Ya existe un módulo o código con el mismo nombre en base de datos");
            logger.error(ExceptionCode.MODULE+" - No fue posible insertar el módulo:{}", moduleView,dta);
            throw  moduleException;
        }catch (ModuleException me){
            throw  me;
        }
    }

    /**
     * {@inheritDoc}
     * */
    @Override
    @Transactional(readOnly = false,rollbackFor = ModuleException.class)
    public void editModule(ModuleView moduleView) throws ModuleException {
        try{
            if(!moduleRepository.exists(moduleView.getModuleId())){
                throw new ModuleException("El módulo no existe en base de datos",ModuleException.LAYER.SER.name(),ModuleException.ACTION.VAL.name());
            }

            Module module = moduleRepository.findOne(moduleView.getModuleId());
            List<ModulePermission> permissions = module.getModulePermissions().stream().collect(Collectors.toList());
            module = moduleConverter.convertViewToEntity(moduleView,true);//replace in memoty


            //eliminar si no vienen en el request
            for(ModulePermission p : permissions){
                if(moduleConverter.indexOfPermission(p.getModulePermissionId(),module.getModulePermissions())==-1){//eliminar el permiso en BD
                    try{
                        moduleRepository.delete(p.getModulePermissionId());
                    }catch (DataIntegrityViolationException dta){
                        ModuleException moduleException= new ModuleException("Error al eliminar permiso: "+p.getCode(),ModuleException.LAYER.DAO.name(),ModuleException.ACTION.DEL.name());
                        moduleException.addError("No es posible eliminar permiso: "+p.getCode()+", ya que se encuentra en uso");
                        throw  moduleException;
                    }catch (Exception ex){
                        ModuleException moduleException= new ModuleException("Error al eliminar permiso: "+p.getCode(),ModuleException.LAYER.DAO.name(),ModuleException.ACTION.DEL.name());
                        moduleException.addError("No es posible eliminar permiso: "+p.getCode()+", intente nuevamente");
                        logger.error(ExceptionCode.MODULE+"- Ocurrio un error al eliminar permiso: {} -  del modulo: {}",p.getCode(),module.getName(),ex);
                        throw  moduleException;
                    }
                }
            }
            moduleRepository.save(module);
        }catch (DataIntegrityViolationException dta){
            ModuleException moduleException= new ModuleException("Error al editar módulo",ModuleException.LAYER.DAO.name(),ModuleException.ACTION.UPD.name());
            moduleException.addError("No es posible editar el permiso");
            throw  moduleException;
        }catch (ModuleException me){
            throw  me;
        }
    }

    /**
     * {@inheritDoc}
     * */
    @Override
    @Transactional(readOnly = false,rollbackFor = ModuleException.class)
    public void deleteModule(Long moduleId) throws ModuleException {
        try{
            if(!moduleRepository.exists(moduleId)){
                throw new ModuleException("El módulo no existe en base de datos",ModuleException.LAYER.SER.name(),ModuleException.ACTION.VAL.name());
            }
            Module m = moduleRepository.findOne(moduleId);
            moduleRepository.delete(m);
        }catch (DataIntegrityViolationException dta){
            ModuleException moduleException = new ModuleException("Error al eliminar el módulo de la base de datos",ModuleException.LAYER.DAO.name(),
                    ModuleException.ACTION.DEL.name());
            moduleException.addError("No es posible eliminar el módulo o sus permisos ya que se encuentran en uso");
            logger.error(ExceptionCode.MODULE+" - No fue posible eliminar el módulo:{}", moduleId,dta);
            throw  moduleException;
        }catch (ModuleException me){
            throw me;
        }catch (Exception e){
            ModuleException moduleException = new ModuleException("Error al eliminar el módulo de la base de datos",ModuleException.LAYER.DAO.name(),
                    ModuleException.ACTION.DEL.name());
            logger.error(ExceptionCode.MODULE+" - No fue posible eliminar el módulo:{}", moduleId,e);
            throw  moduleException;
        }
    }

    /**
     * {@inheritDoc}
     * */
    @Override
    public ModuleView findModule(Long moduleId) throws ModuleException {
        try{
            if(!moduleRepository.exists(moduleId)){
                throw new ModuleException("El módulo no existe en base de datos",ModuleException.LAYER.SER.name(),ModuleException.ACTION.VAL.name());
            }
            Module m = moduleRepository.findOne(moduleId);
            return moduleConverter.convertEntityToView(m,Boolean.FALSE);
        }catch (DataAccessException dta){
            ModuleException moduleException = new ModuleException("Error al seleccionar el módulo en la base de datos",ModuleException.LAYER.DAO.name(),
                    ModuleException.ACTION.SEL.name());
            moduleException.addError("No es posible seleccionar el módulo");
            logger.error(ExceptionCode.MODULE+" - No fue posible seleccionar el módulo:{}", moduleId,dta);
            throw  moduleException;
        }catch (ModuleException me){
            throw me;
        }catch (Exception e){
            ModuleException moduleException = new ModuleException("Error al seleccionar el módulo en la base de datos",ModuleException.LAYER.DAO.name(),
                    ModuleException.ACTION.SEL.name());
            logger.error(ExceptionCode.MODULE+" - No fue posible seleccionar el módulo:{}", moduleId,e);
            throw  moduleException;
        }
    }

    /**
     * {@inheritDoc}
     * */
    @Override
    public List<ModuleView> findAll() throws ModuleException {
        try{
            List<Module> modules = moduleRepository.findAll();
            List<ModuleView> views = new ArrayList<>();
            modules.forEach(m->views.add(moduleConverter.convertEntityToView(m,Boolean.TRUE)));
            return views;
        }catch (DataAccessException dta){
            ModuleException moduleException = new ModuleException("Error al seleccionar el lista de módulos",ModuleException.LAYER.DAO.name(),
                    ModuleException.ACTION.SEL.name());
            logger.error(ExceptionCode.MODULE+" - No fue posible seleccionar lista de módulos",dta);
            throw  moduleException;
        }catch (Exception e){
            ModuleException moduleException = new ModuleException("Error al seleccionar el módulo en la base de datos",ModuleException.LAYER.DAO.name(),
                    ModuleException.ACTION.SEL.name());
            logger.error(ExceptionCode.MODULE+" - No fue posible seleccionar lista de módulos",e);
            throw  moduleException;
        }
    }

}
