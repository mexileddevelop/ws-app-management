package com.mexiled.service.impl;


import com.mexiled.common.MexiledException;
import com.mexiled.converter.UserAppConverter;
import com.mexiled.domain.UserApp;
import com.mexiled.exception.ExceptionCode;
import com.mexiled.exception.UserAppException;
import com.mexiled.repository.ProfileRepository;
import com.mexiled.repository.UserAppRepository;
import com.mexiled.service.UserAppService;
import com.mexiled.views.UserAppView;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional(readOnly = false)
public class UserAppServiceImpl implements UserAppService {

    private final Logger logger = LoggerFactory.getLogger(UserAppServiceImpl.class);

    private UserAppRepository userAppRepository;

    private UserAppConverter userConverter;

    private ProfileRepository profileRepository;

    @Autowired
    public void setUserAppRepository(UserAppRepository userAppRepository) {this.userAppRepository = userAppRepository;}

    @Autowired
    public void setUserConverter(UserAppConverter userConverter) {this.userConverter = userConverter;}

    @Autowired
    public void setProfileRepository(ProfileRepository profileRepository) {this.profileRepository = profileRepository;}

    /**
     * {@inheritDoc}
     * */
    @Override
    @Transactional(readOnly = false,rollbackFor = UserAppException.class)
    public void addUserApp(UserAppView userAppView) throws UserAppException {
        try{
            UserApp userApp = userConverter.fromView(userAppView);
            userAppRepository.save(userApp);
        }catch (DataIntegrityViolationException dta){
            UserAppException userAppException = new UserAppException(
                    "Error al agregar usuario", MexiledException.LAYER.DAO, MexiledException.ACTION.INS
            );
            userAppException.addError("Verifique los datos enviados");
            logger.error(ExceptionCode.USER+"- Error al agregar usuario . {} - CODE: {}",userAppView,userAppException.getExceptionCode(),dta);
            throw userAppException;
        }catch (ConstraintViolationException cve){
            UserAppException userAppException = new UserAppException(
                    "No fue posible agregar el usuario", MexiledException.LAYER.DAO, MexiledException.ACTION.INS
            );
            userAppException.addError("El usuario ya se encuentra en base de datos");
            logger.error(ExceptionCode.USER+"- Error al agregar usuario . {} - CODE: {}",userAppView,userAppException.getExceptionCode(),cve);
            throw userAppException;
        }catch (DataAccessException dae){
            UserAppException userAppException = new UserAppException(
                    "No fue posible agregar el usuario", MexiledException.LAYER.DAO, MexiledException.ACTION.INS
            );
            userAppException.addError("Error en la conexión a la base de datos");
            logger.error(ExceptionCode.USER+"- Error al agregar usuario . {} - CODE: {}",userAppView,userAppException.getExceptionCode(),dae);
            throw userAppException;
        }catch (Exception ex){
            UserAppException userAppException = new UserAppException(
                    "Ocurrio un error al guardar usuario", MexiledException.LAYER.SER, MexiledException.ACTION.INS
            );
            userAppException.addError("Intente nuevamente");
            logger.error(ExceptionCode.USER+"- Error al agregar usuario . {} - CODE: {}",userAppView,userAppException.getExceptionCode(),ex);
            throw userAppException;
        }
    }


    /**
     * {@inheritDoc}
     * */
    @Override
    @Transactional(readOnly = false,rollbackFor = UserAppException.class)
    public void updateUserApp(UserAppView userAppView) throws UserAppException {
        try{
            if(!userAppRepository.exists(userAppView.getUserId())){
                throw new UserAppException(
                        "No se encontro el usuario a editar", MexiledException.LAYER.SER, MexiledException.ACTION.VAL);
            }
            UserApp userApp = userAppRepository.findOne(userAppView.getUserId());
            userApp.setName(userAppView.getName());
            userApp.setEmail(userAppView.getEmail());
            userApp.setActive(userAppView.getActive());
            userApp.setUsername(userAppView.getUsername());
            userApp.setProfile(profileRepository.findOne(userAppView.getProfileId()));
            if(userAppView.getPassword()!=null && !userAppView.getPassword().isEmpty())
                userApp.setPassword(userAppView.getPassword());
            userAppRepository.save(userApp);
        }catch (UserAppException uae){
            throw uae;
        }catch (DataIntegrityViolationException dta){
            UserAppException userAppException = new UserAppException(
                    "No fue posible editar el usuario", MexiledException.LAYER.DAO, MexiledException.ACTION.UPD
            );
            logger.error(ExceptionCode.USER+"- Error al editar usuario . {} - CODE: {}",userAppView,userAppException.getExceptionCode(),dta);
            throw userAppException;
        }catch (DataAccessException dae){
            UserAppException userAppException = new UserAppException(
                    "Error en la conexión a la base de datos", MexiledException.LAYER.DAO, MexiledException.ACTION.UPD
            );
            logger.error(ExceptionCode.USER+"- Error al editar usuario . {} - CODE: {}",userAppView,userAppException.getExceptionCode(),dae);
            throw userAppException;
        }catch (Exception ex){
            UserAppException userAppException = new UserAppException(
                    ex,"No fue posible editar el usuario", MexiledException.LAYER.SER, MexiledException.ACTION.UPD
            );
            logger.error(ExceptionCode.USER+"- Error al editar usuario . {} - CODE: {}",userAppView,userAppException.getExceptionCode(),ex);
            throw userAppException;
        }
    }


    /**
     * {@inheritDoc}
     * */
    @Override
    @Transactional(readOnly = false,rollbackFor = UserAppException.class)
    public void deleteUserApp(Long userAppId) throws UserAppException {
        try{
            if(!userAppRepository.exists(userAppId)){
                throw new UserAppException(
                        "No se encontro el usuario a eliminar", MexiledException.LAYER.SER, MexiledException.ACTION.VAL);
            }
            userAppRepository.delete(userAppId);
        }catch (UserAppException uae){
            throw uae;
        }catch(DataIntegrityViolationException dta){
            UserAppException userAppException = new UserAppException(
                    "No es posible eliminar el usuario", MexiledException.LAYER.DAO, MexiledException.ACTION.DEL
            );
            logger.error(ExceptionCode.USER+"- Error al eliminar usuario . {} - CODE: {}",userAppId,userAppException.getExceptionCode(),dta);
            throw userAppException;
        }catch (ConstraintViolationException cve){
            UserAppException userAppException = new UserAppException(
                    "No es posible eliminar el usuario", MexiledException.LAYER.DAO, MexiledException.ACTION.DEL
            );
            logger.error(ExceptionCode.USER+"- Error al eliminar usuario . {} - CODE: {}",userAppId,userAppException.getExceptionCode(),cve);
            throw userAppException;
        }catch (DataAccessException dae){
            UserAppException userAppException = new UserAppException(
                    "Error en la conexión a la base de datos", MexiledException.LAYER.DAO, MexiledException.ACTION.DEL
            );
            logger.error(ExceptionCode.USER+"- Error al eliminar usuario . {} - CODE: {}",userAppId,userAppException.getExceptionCode(),dae);
            throw userAppException;
        }catch (Exception ex){
            UserAppException userAppException = new UserAppException(
                    "Ocurrio un error al eliminar usuario", MexiledException.LAYER.SER, MexiledException.ACTION.DEL
            );
            logger.error(ExceptionCode.USER+"- Error al eliminar usuario . {} - CODE: {}",userAppId,userAppException.getExceptionCode(),ex);
            throw userAppException;
        }
    }


    /**
     * {@inheritDoc}
     * */
    @Override
    @Transactional(readOnly = false,rollbackFor = UserAppException.class)
    public void changeStatus(Long userAppId, Boolean active) throws UserAppException {
        try{
            if(!userAppRepository.exists(userAppId)){
                throw new UserAppException(
                        "No se encontro el usuario a editar", MexiledException.LAYER.SER, MexiledException.ACTION.VAL);
            }
            UserApp userApp = userAppRepository.findOne(userAppId);
            userApp.setActive(active);
            userAppRepository.save(userApp);
        }catch (UserAppException uae){
            throw uae;
        }catch(DataIntegrityViolationException dta){
            UserAppException userAppException = new UserAppException(
                    "No es posible editar el usuario", MexiledException.LAYER.DAO, MexiledException.ACTION.UPD
            );
            logger.error(ExceptionCode.USER+"- Error al editar usuario . {} - CODE: {}",userAppId,userAppException.getExceptionCode(),dta);
            throw userAppException;
        }catch (ConstraintViolationException cve){
            UserAppException userAppException = new UserAppException(
                    "No es posible editar el usuario", MexiledException.LAYER.DAO, MexiledException.ACTION.UPD
            );
            logger.error(ExceptionCode.USER+"- Error al editar usuario . {} - CODE: {}",userAppId,userAppException.getExceptionCode(),cve);
            throw userAppException;
        }catch (DataAccessException dae){
            UserAppException userAppException = new UserAppException(
                    "Error en la conexión a la base de datos", MexiledException.LAYER.DAO, MexiledException.ACTION.UPD
            );
            logger.error(ExceptionCode.USER+"- Error al editar usuario . {} - CODE: {}",userAppId,userAppException.getExceptionCode(),dae);
            throw userAppException;
        }catch (Exception ex){
            UserAppException userAppException = new UserAppException(
                    "Error al editar usuario", MexiledException.LAYER.SER, MexiledException.ACTION.UPD
            );
            logger.error(ExceptionCode.USER+"- Error al eliminar usuario . {} - CODE: {}",userAppId,userAppException.getExceptionCode(),ex);
            throw userAppException;
        }
    }


    /**
     * {@inheritDoc}
     * */
    @Override
    public UserAppView findById(Long userAppId) throws UserAppException {
        try{
            if(!userAppRepository.exists(userAppId)){
                throw new UserAppException(
                        "No se encontro el usuario", MexiledException.LAYER.SER, MexiledException.ACTION.VAL);
            }
            return userConverter.fromEntity(userAppRepository.findOne(userAppId));
        }catch (UserAppException uae){
            throw  uae;
        }catch (DataAccessException dae){
            UserAppException userAppException = new UserAppException(
                    "Error en la conexión a la base de datos", MexiledException.LAYER.DAO, MexiledException.ACTION.SEL
            );
            logger.error(ExceptionCode.USER+"- Error al seleccionar usuario . {} - CODE: {}",userAppId,userAppException.getExceptionCode(),dae);
            throw userAppException;
        }catch (Exception ex){
            UserAppException userAppException = new UserAppException(
                    "Error al seleccionar usuario", MexiledException.LAYER.SER, MexiledException.ACTION.SEL
            );
            logger.error(ExceptionCode.USER+"- Error al seleccionar usuario . {} - CODE: {}",userAppId,userAppException.getExceptionCode(),ex);
            throw userAppException;
        }
    }


    /**
     * {@inheritDoc}
     * */
    @Override
    public UserAppView findByUsername(String username) throws UserAppException {
        try{
            return userConverter.fromEntity(userAppRepository.findByUsername(username));
        }catch (UserAppException uae){
            throw  uae;
        }catch (DataAccessException dae){
            UserAppException userAppException = new UserAppException(
                    "Error en la conexión a la base de datos", MexiledException.LAYER.DAO, MexiledException.ACTION.SEL
            );
            logger.error(ExceptionCode.USER+"- Error al seleccionar usuario . {} - CODE: {}",username,userAppException.getExceptionCode(),dae);
            throw userAppException;
        }catch (Exception ex){
            UserAppException userAppException = new UserAppException(
                    "Error al seleccionar usuario", MexiledException.LAYER.SER, MexiledException.ACTION.SEL
            );
            logger.error(ExceptionCode.USER+"- Error al seleccionar usuario . {} - CODE: {}",username,userAppException.getExceptionCode(),ex);
            throw userAppException;
        }
    }


    /**
     * {@inheritDoc}
     * */
    @Override
    public List<UserAppView> findAll() throws UserAppException {
        try{
            List<UserApp> users = userAppRepository.findAll();
            List<UserAppView> views = new ArrayList<>();
            users.forEach(u->views.add(userConverter.fromEntity(u)));
            return views;
        }catch (DataAccessException dae){
            UserAppException userAppException = new UserAppException(
                    "Error en la conexión a la base de datos", MexiledException.LAYER.DAO, MexiledException.ACTION.SEL
            );
            logger.error(ExceptionCode.USER+"- Error al seleccionar usuarios - CODE: {}",userAppException.getExceptionCode(),dae);
            throw userAppException;
        }catch (Exception ex){
            UserAppException userAppException = new UserAppException(
                    "Error al seleccionar usuario", MexiledException.LAYER.SER, MexiledException.ACTION.SEL
            );
            logger.error(ExceptionCode.USER+"- Error al seleccionar usuaris - CODE: {}",userAppException.getExceptionCode(),ex);
            throw userAppException;
        }
    }
}
