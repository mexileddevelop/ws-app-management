package com.mexiled.service.impl;


import com.mexiled.common.DeviceEstatusConfig;
import com.mexiled.common.MexiledException;
import com.mexiled.common.TimeUtils;
import com.mexiled.domain.Device;
import com.mexiled.domain.DeviceHistory;
import com.mexiled.domain.DeviceRecord;
import com.mexiled.domain.RecordMeasurement;
import com.mexiled.exception.ExceptionCode;
import com.mexiled.exception.RecordException;
import com.mexiled.repository.DeviceHistoryRepository;
import com.mexiled.repository.DeviceRecordRepository;
import com.mexiled.repository.DeviceRepository;
import com.mexiled.service.DeviceRecordService;
import com.mexiled.views.GraphReportView;
import com.mexiled.views.MeassureFase;
import com.mexiled.views.MeassureGraphView;
import com.mexiled.views.MeassureReportView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.Join;
import java.sql.Time;
import java.text.ParseException;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Service
@Transactional(readOnly = false)
public class DeviceRecordServiceImpl implements DeviceRecordService{

    private final Logger logger = LoggerFactory.getLogger(DeviceRecordServiceImpl.class);

    private DeviceRepository deviceRepository;

    private DeviceRecordRepository recordRepository;

    private DeviceHistoryRepository historyRepository;

    @Value("${device.threshold.on}")
    private final Double threshold=0.0;

    @Value("${device.number.records.to.make.as.disconnected}")
    private final Integer desconnectionTolerance = 0;

    private TimeUtils timeUtils;


    @Autowired
    public void setDeviceRepository(DeviceRepository deviceRepository) {
        this.deviceRepository = deviceRepository;
    }

    @Autowired
    public void setRecordRepository(DeviceRecordRepository recordRepository) {this.recordRepository = recordRepository;}

    @Autowired
    public void setHistoryRepository(DeviceHistoryRepository historyRepository) {this.historyRepository = historyRepository;}

    @Autowired
    public void setTimeUtils(TimeUtils timeUtils) {
        this.timeUtils = timeUtils;
    }

    @Override
    public void addRecords(List<MeassureReportView> records) throws RecordException{
        try{
            List<DeviceRecord> devicesToSave = new ArrayList<>();
            for (MeassureReportView r : records) {
                DeviceRecord deviceRecord = converFromView(r);
                //update revice history for each record
                DeviceHistory dh = deviceRecord.getDevice().getDeviceHistory();

                Double corriente = getAmperes(r);

                //ZoneId z = ZoneId.of("America/Mexico_City");

                ZonedDateTime zdt = ZonedDateTime.now();
                //zdt.toLocalTime();//
                LocalTime currentTime = zdt.toLocalTime();////LocalTime.parse("10:00",DateTimeFormatter.ofPattern("HH:mm"));
                //hora en que deben prender el circuito, esta en su configuracion
                LocalTime start = LocalTime.parse(
                        timeUtils.formatHour(deviceRecord.getDevice().getLigthOnHour(), timeUtils.HOUR_FORMAT),
                        DateTimeFormatter.ofPattern(timeUtils.HOUR_FORMAT)
                );
                //hora en que se debe apagar el circuito, está en su configuración
                LocalTime stop = LocalTime.parse(
                        timeUtils.formatHour(deviceRecord.getDevice().getLigthOffHour(), timeUtils.HOUR_FORMAT),
                        DateTimeFormatter.ofPattern(timeUtils.HOUR_FORMAT)
                );
                if(corriente>threshold){
                    //comparar si esta dentro de horario
                    //nota hace comparaciones por ej 20:30 - 08:30 de dos días diferentes, no se supone que deba encender en el día :O
                    LocalTime onStart = start.minus(Duration.ofMinutes(deviceRecord.getDevice().getTimeBeforeNotify()));
                    LocalTime onStop = stop.plus(Duration.ofMinutes(deviceRecord.getDevice().getTimeBeforeNotify()));
                    Boolean isOnTime = (currentTime.isBefore(onStart) && currentTime.isBefore(onStop)) || (currentTime.isAfter(onStart) &&  currentTime.isAfter(onStop));
                    logger.info("Is on time: {}",isOnTime);
                    deviceRecord.getDevice().setDeviceStatus(isOnTime ? DeviceEstatusConfig.ON : DeviceEstatusConfig.ON_ERROR);
                    deviceRecord.getDevice().setNumberOffRequests(0);//reinicia contador
                }else{
                    //horario en que debería estar apagado
                    LocalTime onStart = stop.plus(Duration.ofMinutes(deviceRecord.getDevice().getTimeBeforeNotify()));
                    LocalTime onStop = start.minus(Duration.ofMinutes(deviceRecord.getDevice().getTimeBeforeNotify()));
                    //apagado de forma normal
                    Boolean isOnTime = currentTime.isAfter(onStart) && currentTime.isBefore(onStop);
                    if(isOnTime){
                        deviceRecord.getDevice().setDeviceStatus(DeviceEstatusConfig.OFF);
                    }else{
                        //comparar si ya se paso de la hora que debería estar encendido o se apago cuando debía estar encendido
                        onStart = start.minus(Duration.ofMinutes(deviceRecord.getDevice().getTimeBeforeNotify()));
                        onStop = stop.plus(Duration.ofMinutes(deviceRecord.getDevice().getTimeBeforeNotify()));
                        isOnTime = (currentTime.isBefore(onStart) && currentTime.isBefore(onStop)) || (currentTime.isAfter(onStart) &&  currentTime.isAfter(onStop));
                        Integer disconnectedRecords = deviceRecord.getDevice().getNumberOffRequests();
                        if(isOnTime && deviceRecord.getDevice().getNumberOffRequests()>= desconnectionTolerance){
                            deviceRecord.getDevice().setDeviceStatus(DeviceEstatusConfig.DISCONNECTED);
                            deviceRecord.getDevice().setNumberOffRequests(++disconnectedRecords);
                        }else{
                            deviceRecord.getDevice().setNumberOffRequests(++disconnectedRecords);
                            deviceRecord.getDevice().setDeviceStatus(DeviceEstatusConfig.OFF_OUT);
                        }
                    }
                }
                if (dh == null) {
                    dh = new DeviceHistory();
                    dh.setDevice(deviceRecord.getDevice());
                    dh.setFirstRegistered(new Date());
                    dh.setFirstRegistered(new Date());
                    dh.setLastActivity(new Date());
                } else {
                    dh.setLastActivity(new Date());
                }
                dh.setHistoryStatus(deviceRecord.getDevice().getDeviceStatus());//solo actualiza o inserta en la tabla para los reportes
                historyRepository.save(dh);
                deviceRecord.setDeviceStatusOnRecord(dh.getHistoryStatus());//se va a gaurdando siempre
                devicesToSave.add(deviceRecord);
            }
            recordRepository.save(devicesToSave);
        }catch (NullPointerException npe){
            RecordException re = new RecordException("Error al agregar nuevo registro", MexiledException.LAYER.SER, MexiledException.ACTION.INS);
            logger.error("Error al agregar nuevo registro");
            throw re;
        }catch (Exception ex){
            RecordException recordException = new RecordException("Error al agregar nuevo registro", MexiledException.LAYER.SER, MexiledException.ACTION.INS);
            logger.error("Error al guardar medición de circuito - {}",records,ex);
            throw recordException;
        }

    }

    @Override
    public GraphReportView findGraphDeviceByIdAndType(Long deviceId, String type) throws RecordException {
        try{
            //obtener dispositivo y detalles de última medición
            //obtener listado de mediciones desde f1 hasta último ID de medición
            if(!deviceRepository.exists(deviceId))
                throw new RecordException("No se encontró circuito", MexiledException.LAYER.SER, MexiledException.ACTION.VAL);
            Device d = deviceRepository.findOne(deviceId);
            GraphReportView graphView = fromEntity(d);
            Specification<DeviceRecord> lastSpec = Specifications.where((root, query, cb) -> {
                Join<DeviceRecord,Device> deviceJoin = root.join("device");
                return cb.equal(deviceJoin.get("device"),deviceId);
            });
            List<DeviceRecord> deviceRecords = recordRepository.findFirst2ByDevice_DeviceIdOrderByCreatedDateDesc(d.getDeviceId());
            DeviceRecord lastRecord = null;
            if(!deviceRecords.isEmpty()){
                //obtener potencia acumulada
                lastRecord = deviceRecords.get(0);
                graphView.setLastRecords(fromRecordEntity(lastRecord));
                //suma de potencias registradas en ambas fases
                lastRecord.getMeasurements().forEach(m->graphView.setLastWatts(graphView.getLastWatts()+m.getWatts()));
                if(deviceRecords.size()>1){
                    //resta de potencias acumuladas
                    graphView.setAcumulattedWatts(lastRecord.getAcumulattedWatts()-deviceRecords.get(0).getAcumulattedWatts());
                }
                Date onReportStart = new Date();
                if(type.equalsIgnoreCase("s")){
                    onReportStart = timeUtils.addTimeToDate(onReportStart,-7,timeUtils.DAY);
                }else if(type.equalsIgnoreCase("m")){
                    onReportStart = timeUtils.addTimeToDate(onReportStart,-28,timeUtils.DAY);
                }else{
                    onReportStart = timeUtils.addTimeToDate(onReportStart,-1,timeUtils.DAY);
                }
                List<DeviceRecord> filteredRecords =recordRepository.findByDevice_DeviceIdAndCreatedDateAfterAndDeviceRecordIdIsLessThanEqualOrderByCreatedDateAsc(
                        deviceId,onReportStart,lastRecord.getDeviceRecordId()
                );
                filteredRecords.forEach(f->graphView.getRecords().add(fromRecordEntity(f)));
            }


            return graphView;
        }catch (RecordException re){
            throw re;
        }catch (Exception e){
            RecordException re = new RecordException("Error al obtener reporte de circuito", MexiledException.LAYER.SER, MexiledException.ACTION.SEL);
            logger.error(ExceptionCode.DEVICE+"- Error al obtener mediciones de dispositivo - {}- tipo: {}",deviceId,type,e);
            throw re;
        }
    }

    private Double getAmperes(MeassureReportView reportView){
        try{
               return reportView.getFase1().getPotencia() + reportView.getFase2().getPotencia();
        }catch (ArithmeticException ar){
                return 0.0;//for infinite representation divisionbyzero
        }
    }

    private DeviceRecord converFromView(MeassureReportView record){
        DeviceRecord deviceRecord = new DeviceRecord();
        deviceRecord.setCreatedDate(new Date());
        deviceRecord.setDevice(deviceRepository.findOne(record.getId()));
        deviceRecord.setAcumulattedWatts(record.getPotenciaAcumulada());

        RecordMeasurement fase1 = new RecordMeasurement();
        fase1.setAmperes(record.getFase1().getCorriente());
        fase1.setDeviceRecord(deviceRecord);
        fase1.setType("Fase 1");
        fase1.setVoltage(record.getFase1().getVoltaje());
        fase1.setWatts(record.getFase1().getPotencia());

        deviceRecord.getMeasurements().add(fase1);

        RecordMeasurement fase2 = new RecordMeasurement();
        fase2.setAmperes(record.getFase2().getCorriente());
        fase2.setDeviceRecord(deviceRecord);
        fase2.setType("Fase 2");
        fase2.setVoltage(record.getFase2().getVoltaje());
        fase2.setWatts(record.getFase2().getPotencia());

        deviceRecord.getMeasurements().add(fase2);
        return deviceRecord;
    }


    private GraphReportView fromEntity(Device device) throws ParseException{
        GraphReportView graphReportView = new GraphReportView();
        graphReportView.setCode(device.getCode());
        graphReportView.setDeviceId(device.getDeviceId());
        graphReportView.setStatus(device.getInstallationStatus());
        if(device.getInstallationStatus().equals(DeviceEstatusConfig.INSTALADO)){
            if(device.getDeviceHistory()!=null && device.getDeviceHistory().getHistoryStatus()!=null && !device.getDeviceHistory().getHistoryStatus().isEmpty()) {
                //si se tienen mediciones
                String status = device.getDeviceHistory().getHistoryStatus();
                //si el último estatus es instalado o apagado, verificar cuanto tiempo tiene sin conexión, no debe superar la tolerancia marcada
                if((status.equals(DeviceEstatusConfig.ON) || status.equals(DeviceEstatusConfig.OFF) || status.equals(DeviceEstatusConfig.DISCONNECTED))){
                    //ZoneId z = ZoneId.of("America/Mexico_City");

                    ZonedDateTime zdt = ZonedDateTime.now();
                    LocalDateTime currentTime = zdt.toLocalDateTime();
                    LocalDateTime last = LocalDateTime.parse(
                            timeUtils.formatHour(device.getDeviceHistory().getLastActivity(), timeUtils.DATE_FORMAT),
                            DateTimeFormatter.ofPattern(timeUtils.DATE_FORMAT)
                    );
                    long minutes = last.until(currentTime, ChronoUnit.MINUTES);
                    logger.info("Minutes since last record: "+minutes);
                    if(minutes>=device.getTimeToMarkAsDisconnected()) {
                        status = DeviceEstatusConfig.DISCONNECTED;
                    }
                }
                graphReportView.setStatus(status);
            }
        }
        return graphReportView;
    }

    private MeassureGraphView fromRecordEntity(DeviceRecord deviceRecord){
        MeassureGraphView meassureGraphView = new MeassureGraphView();
        meassureGraphView.setCreatedDate(deviceRecord.getCreatedDate());
        deviceRecord.getMeasurements().forEach(m->{
            if(m.getType().equalsIgnoreCase("Fase 1")){
                meassureGraphView.setFase1(faseFromEntity(m));
            }else{
                meassureGraphView.setFase2(faseFromEntity(m));
            }
        });
        return meassureGraphView;
    }

    private MeassureFase faseFromEntity(RecordMeasurement recordMeasurement){
        MeassureFase view = new MeassureFase();
        view.setCorriente(recordMeasurement.getAmperes());
        view.setPotencia(recordMeasurement.getWatts());
        view.setVoltaje(recordMeasurement.getVoltage());
        return view;
    }
}
