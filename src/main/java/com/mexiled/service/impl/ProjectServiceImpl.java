package com.mexiled.service.impl;

import com.mexiled.common.MexiledException;
import com.mexiled.converter.ProjectConverter;
import com.mexiled.domain.Project;
import com.mexiled.exception.ExceptionCode;
import com.mexiled.exception.ProjectException;
import com.mexiled.repository.ProjectRepository;
import com.mexiled.service.ProjectService;
import com.mexiled.views.ProjectView;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional(readOnly = true)
public class ProjectServiceImpl implements ProjectService {

    private final Logger logger = LoggerFactory.getLogger(ProfileServiceImpl.class);

    private ProjectRepository projectRepository;

    private ProjectConverter projectConverter;

    @Autowired
    public void setProjectConverter(ProjectConverter projectConverter) {
        this.projectConverter = projectConverter;
    }

    @Autowired
    public void setProjectRepository(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    @Transactional(readOnly = false,rollbackFor = ProjectException.class)
    public void addProject(ProjectView view) throws ProjectException {
        try{
            this.projectRepository.save(projectConverter.fromView(view));
        }catch (ConstraintViolationException cve){
            ProjectException projectException = new ProjectException("Error al agregar proyecto", MexiledException.LAYER.DAO, MexiledException.ACTION.INS);
            projectException.addError("Ya existe un proyeto con el código: "+view.getCode());
            logger.error(ExceptionCode.PROJECT+"- Error al agregar proyecto - {} - CODE: {}",view,projectException.getExceptionCode(),cve);
            throw projectException;
        }catch (DataIntegrityViolationException dta){
            ProjectException projectException = new ProjectException("Ocurrio un error al agregar proyecto", MexiledException.LAYER.DAO, MexiledException.ACTION.INS);
            projectException.addError("Ya existe un proyeto con el código: "+view.getCode());
            logger.error(ExceptionCode.PROJECT+"- Error al agregar proyecto - {} - CODE: {}",view,projectException.getExceptionCode(),dta);
            throw projectException;
        }
        catch (Exception ex){
            ProjectException projectException = new ProjectException("No fue posible agregar el proyecto, intente nuevamente", MexiledException.LAYER.SER, MexiledException.ACTION.INS);
            logger.error(ExceptionCode.PROJECT+"- Error al agregar proyecto - {} - CODE: {}",view,projectException.getExceptionCode(),ex);
            throw projectException;
        }
    }

    @Override
    @Transactional(readOnly = false,rollbackFor = ProjectException.class)
    public void updateProject(ProjectView view) throws ProjectException {
        try{
            if(!projectRepository.exists(view.getProjectId()))
                throw new ProjectException("No se encuentra proyecto a editar", MexiledException.LAYER.SER, MexiledException.ACTION.VAL);
            this.projectRepository.save(projectConverter.fromView(view));
        }catch (ProjectException pe){
            throw pe;
        }catch (DataIntegrityViolationException dta){
            ProjectException projectException = new ProjectException("Ocurrio un error al editar proyecto", MexiledException.LAYER.DAO, MexiledException.ACTION.UPD);
            projectException.addError("Ya existe un proyeto con el código: " + view.getCode());
            logger.error(ExceptionCode.PROJECT + "- Error al editar proyecto - {} - CODE: {}", view, projectException.getExceptionCode(), dta);
            throw projectException;
        }catch (ConstraintViolationException cve){
            ProjectException projectException = new ProjectException("Error al editar proyecto", MexiledException.LAYER.DAO, MexiledException.ACTION.UPD);
            projectException.addError("Ya existe un proyeto con el código: "+view.getCode());
            logger.error(ExceptionCode.PROJECT+"- Error al editar proyecto - {} - CODE: {}",view,projectException.getExceptionCode(),cve);
            throw projectException;
        }catch (Exception ex){
            ProjectException projectException = new ProjectException("Ocurrio un error al editar proyecto", MexiledException.LAYER.SER, MexiledException.ACTION.UPD);
            logger.error(ExceptionCode.PROJECT+"- Error al editar proyecto - {} - CODE: {}",view,projectException.getExceptionCode(),ex);
            throw projectException;
        }
    }

    @Override
    @Transactional(readOnly = false,rollbackFor = ProjectException.class)
    public void deleteProject(Long projectId) throws ProjectException {
        try{
            if(!projectRepository.exists(projectId))
                throw new ProjectException("No se encuentra proyecto a eliminar", MexiledException.LAYER.SER, MexiledException.ACTION.VAL);
            this.projectRepository.delete(projectId);
        }catch (ProjectException pe){ throw pe; }
        catch (ConstraintViolationException cve){
            ProjectException projectException = new ProjectException("Error al eliminar proyecto", MexiledException.LAYER.DAO, MexiledException.ACTION.DEL);
            projectException.addError("No es posible eliminar el proyecto");
            logger.error(ExceptionCode.PROJECT+"- Error al editar proyecto - {} - CODE: {}",projectId,projectException.getExceptionCode(),cve);
            throw projectException;
        }catch (DataIntegrityViolationException dta){
            ProjectException projectException = new ProjectException("Error al eliminar proyecto", MexiledException.LAYER.DAO, MexiledException.ACTION.DEL);
            projectException.addError("No es posible eliminar el proyecto");
            logger.error(ExceptionCode.PROJECT+"- Error al editar proyecto - {} - CODE: {}",projectId,projectException.getExceptionCode(),dta);
            throw projectException;
        }catch (Exception ex){
            ProjectException projectException = new ProjectException("Ocurrio un error al eliminar proyecto", MexiledException.LAYER.DAO, MexiledException.ACTION.DEL);
            logger.error(ExceptionCode.PROJECT+"- Error al editar proyecto - {} - CODE: {}",projectId,projectException.getExceptionCode(),ex);
            throw projectException;
        }
    }

    @Override
    public ProjectView findProject(Long projectId) throws ProjectException {
        try{
            if(!projectRepository.exists(projectId))
                throw new ProjectException("No se encuentra proyecto en la base de datos", MexiledException.LAYER.SER, MexiledException.ACTION.VAL);
            return projectConverter.fromEntity(projectRepository.findOne(projectId));
        }catch (ProjectException cve){
            throw cve;
        }catch (Exception ex){
            ProjectException projectException = new ProjectException("Ocurrio un error al seleccionar proyecto", MexiledException.LAYER.DAO, MexiledException.ACTION.SEL);
            logger.error(ExceptionCode.PROJECT+"- Error al seleccionar proyecto - {} - CODE: {}",projectId,projectException.getExceptionCode(),ex);
            throw projectException;
        }
    }

    @Override
    public List<ProjectView> findProjects(Boolean active) throws ProjectException {
        try{
            List<Project> projects;
            if(active!=null){
                Specification<Project> projectSpec = Specifications.where((root, query, cb) -> cb.equal(root.get("active"),active));
                projects = projectRepository.findAll(projectSpec);
            }else{
                projects = projectRepository.findAll();
            }

            List<ProjectView> views = new ArrayList<>();
            projects.forEach(p->views.add(projectConverter.fromEntity(p)));
            return views;
        }catch (Exception ex){
            ProjectException projectException = new ProjectException("Ocurrio un error al seleccionar lista de proyectos", MexiledException.LAYER.DAO, MexiledException.ACTION.SEL);
            logger.error(ExceptionCode.PROJECT+"- Error al seleccionar lista de proyectos  - CODE: {}",projectException.getExceptionCode(),ex);
            throw projectException;
        }
    }
}
