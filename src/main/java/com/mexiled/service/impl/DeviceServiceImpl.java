package com.mexiled.service.impl;


import com.mexiled.common.DeviceEstatusConfig;
import com.mexiled.common.MexiledException;
import com.mexiled.common.TimeUtils;
import com.mexiled.converter.DeviceConverter;
import com.mexiled.converter.DeviceException;
import com.mexiled.domain.*;
import com.mexiled.exception.ExceptionCode;
import com.mexiled.repository.*;
import com.mexiled.service.DeviceService;
import com.mexiled.views.DeviceReportView;
import com.mexiled.views.DeviceUpdateView;
import com.mexiled.views.DeviceView;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;

@Service
@Transactional(readOnly = true)
public class DeviceServiceImpl implements DeviceService{

    public final Logger logger = LoggerFactory.getLogger(DeviceServiceImpl.class);


    private DeviceRepository deviceRepository;


    private DeviceConverter deviceConverter;

    private ProjectRepository projectRepository;

    private PolygonRepository polygonRepository;

    private SectionRepository sectionRepository;

    private TimeUtils timeUtils;

    private DeviceHistoryRepository historyRepository;

    @Autowired
    public void setDeviceConverter(DeviceConverter deviceConverter) {
        this.deviceConverter = deviceConverter;
    }

    @Autowired
    public void setDeviceRepository(DeviceRepository deviceRepository) {
        this.deviceRepository = deviceRepository;
    }

    @Autowired
    public void setHistoryRepository(DeviceHistoryRepository historyRepository) {this.historyRepository = historyRepository;}

    @Autowired
    public void setPolygonRepository(PolygonRepository polygonRepository) {this.polygonRepository = polygonRepository;}

    @Autowired
    public void setSectionRepository(SectionRepository sectionRepository) {this.sectionRepository = sectionRepository;}

    @Autowired
    public void setProjectRepository(ProjectRepository projectRepository) {this.projectRepository = projectRepository;}

    @Autowired
    public void setTimeUtils(TimeUtils timeUtils) {
        this.timeUtils = timeUtils;
    }

    @Override
    @Transactional(readOnly = false,rollbackFor = DeviceException.class)
    public void addDevice(DeviceView view) throws DeviceException {
        try{
            Device device = deviceConverter.fromView(view);
            device.setProject(projectRepository.findOne(view.getProject().getProjectId()));
            device.setSection(sectionRepository.findOne(view.getSection().getSectionId()));
            device.setPolygon(polygonRepository.findOne(view.getPolygonView().getPolygonId()));
            device.setDeviceStatus(DeviceEstatusConfig.PROYECTADO);
            device.setCode(
                    device.getProject().getCode()+"-"+
                    device.getSection().getCode()+"-"+
                    device.getPolygon().getCode()+"-"+
                    device.getControlNumber()
            );
            deviceRepository.save(device);
        }catch (DataIntegrityViolationException dta){
            DeviceException dev = new DeviceException("Error al guardar nuevo circuito", MexiledException.LAYER.DAO, MexiledException.ACTION.INS);
            dev.addError("Verifique el número de control");
            dev.addError("Verifique dirección MAC");
            logger.error(ExceptionCode.DEVICE+"- Error al guardar circuito - {} - CODE: {}",view,dev.getExceptionCode());
            throw dev;
        }catch (ConstraintViolationException cve){
            DeviceException dev = new DeviceException("Error al guardar nuevo circuito", MexiledException.LAYER.DAO, MexiledException.ACTION.INS);
            dev.addError("Verifique el número de control");
            dev.addError("Verifique dirección MAC");
            logger.error(ExceptionCode.DEVICE+"- Error al guardar circuito - {} - CODE: {}",view,dev.getExceptionCode());
            throw dev;
        }catch (Exception ex){
            DeviceException dev = new DeviceException("Error al guardar nuevo circuito", MexiledException.LAYER.SER, MexiledException.ACTION.INS);
            logger.error(ExceptionCode.DEVICE+"- Error al guardar circuito - {} - CODE: {}",view,dev.getExceptionCode(),ex);
            throw dev;
        }
    }

    @Override
    @Transactional(readOnly = false,rollbackFor = DeviceException.class)
    public void updateDevice(DeviceView view) throws DeviceException {
        try{
            if(!deviceRepository.exists(view.getDeviceId()))
                throw new  DeviceException("No se encontro circuito a editar", MexiledException.LAYER.DAO, MexiledException.ACTION.VAL);
            Device device = deviceConverter.fromView(view);
            device.setProject(projectRepository.findOne(view.getProject().getProjectId()));
            device.setSection(sectionRepository.findOne(view.getSection().getSectionId()));
            device.setPolygon(polygonRepository.findOne(view.getPolygonView().getPolygonId()));
            device.setCode(
                    device.getProject().getCode()+"-"+
                            device.getSection().getCode()+"-"+
                            device.getPolygon().getCode()+"-"+
                            device.getControlNumber()
            );
            deviceRepository.save(device);
        }catch (DeviceException ex){
            throw ex;
        }catch (DataIntegrityViolationException dta){
            DeviceException dev = new DeviceException("Error al editar circuito", MexiledException.LAYER.DAO, MexiledException.ACTION.UPD);
            dev.addError("Verifique el número de control");
            dev.addError("Verifique dirección MAC");
            logger.error(ExceptionCode.DEVICE+"- Error al editar circuito - {} - CODE: {}",view,dev.getExceptionCode());
            throw dev;
        }catch (ConstraintViolationException cve){
            DeviceException dev = new DeviceException("Error al editar circuito", MexiledException.LAYER.DAO, MexiledException.ACTION.UPD);
            dev.addError("Verifique el número de control");
            dev.addError("Verifique dirección MAC");
            logger.error(ExceptionCode.DEVICE+"- Error al editar circuito - {} - CODE: {}",view,dev.getExceptionCode());
            throw dev;
        }catch (Exception ex){
            DeviceException dev = new DeviceException("Error al guardar nuevo circuito", MexiledException.LAYER.SER, MexiledException.ACTION.UPD);
            logger.error(ExceptionCode.DEVICE+"- Error al editar circuito - {} - CODE: {}",view,dev.getExceptionCode());
            throw dev;
        }
    }

    @Override
    @Transactional(readOnly = false,rollbackFor = DeviceException.class)
    public void deleteDevice(Long deviceId) throws DeviceException {
        try{
            if(!deviceRepository.exists(deviceId))
                throw new  DeviceException("No se encontro circuito a eliminar", MexiledException.LAYER.DAO, MexiledException.ACTION.VAL);
            deviceRepository.delete(deviceId);
        }catch (DeviceException ex){
            throw ex;
        }catch (DataIntegrityViolationException dta){
            DeviceException dev = new DeviceException("No es posible eliminar el circuito", MexiledException.LAYER.DAO, MexiledException.ACTION.DEL);
            logger.error(ExceptionCode.DEVICE+"- Error al eliminar circuito - {} - CODE: {}",deviceId,dev.getExceptionCode());
            throw dev;
        }catch (ConstraintViolationException cve){
            DeviceException dev = new DeviceException("No es posible eliminar el circuito", MexiledException.LAYER.DAO, MexiledException.ACTION.DEL);
            logger.error(ExceptionCode.DEVICE+"- Error al eliminar circuito - {} - CODE: {}",deviceId,dev.getExceptionCode());
            throw dev;
        }catch (Exception ex){
            DeviceException dev = new DeviceException("Error al eliminar circuito", MexiledException.LAYER.SER, MexiledException.ACTION.DEL);
            logger.error(ExceptionCode.DEVICE+"- Error al eliminar circuito - {} - CODE: {}",deviceId,dev.getExceptionCode());
            throw dev;
        }
    }

    @Override
    @Transactional(readOnly = false,rollbackFor = DeviceException.class)
    public void updateByProjectSectionOrPoylgonId(DeviceUpdateView updateView) throws DeviceException {
        try{
            if(updateView.getPolygonId()==null && updateView.getProjectId()==null && updateView.getSectionId()==null)
                throw  new DeviceException("Especifique un método de actualización", MexiledException.LAYER.SER, MexiledException.ACTION.VAL);
            //TODO verify JPA doc for massive updates
            Specification<Device> deviceSpec = Specifications.where((root, query, cb) -> {
                Predicate p = null;
                Join<Device,Section> sectionJoin = root.join("section");
                Join<Device,Project> projectJoin = root.join("project");
                Join<Device,Polygon> polygonJoin = root.join("polygon");
                if(updateView.getSectionId()!=null)
                    p = cb.equal(sectionJoin.get("sectionId"),updateView.getSectionId());
                if(updateView.getProjectId()!=null)
                    p = p!=null ? cb.and(p,cb.equal(projectJoin.get("projectId"),updateView.getProjectId())) : cb.equal(projectJoin.get("projectId"),updateView.getProjectId());
                if(updateView.getPolygonId()!=null)
                    p = p!=null ? cb.and(p,cb.equal(polygonJoin.get("polygonId"),updateView.getPolygonId())) : cb.equal(polygonJoin.get("polygonId"),updateView.getPolygonId());
                return p;
            });
            List<Device> devicesUpdate = deviceRepository.findAll(deviceSpec);
            devicesUpdate.forEach(d->{
                if(updateView.getTimeBeforeNotify()!=null)
                    d.setTimeBeforeNotify(updateView.getTimeBeforeNotify());
                if(updateView.getTimeToMarkAsDisconnected()!=null)
                    d.setTimeToMarkAsDisconnected(updateView.getTimeToMarkAsDisconnected());
                if(updateView.getLigthOffHour()!=null && !updateView.getLigthOffHour().isEmpty()){
                    try{
                        d.setLigthOffHour(timeUtils.parseHour(updateView.getLigthOffHour(),timeUtils.HOUR_FORMAT));
                    }catch (ParseException parse){
                        logger.error("Hora no parseada para tiempo de apagado");
                    }
                }
                if(updateView.getLigthOnHour()!= null && !updateView.getLigthOnHour().isEmpty()){
                    try{
                        d.setLigthOnHour(timeUtils.parseHour(updateView.getLigthOnHour(),timeUtils.HOUR_FORMAT));
                    }catch (ParseException parse){
                        logger.error("Hora no parseada para tiempo de encendido");
                    }
                }

            });
        }catch (DeviceException dve){
            throw dve;
        }
        catch (Exception ex){
            DeviceException dev = new DeviceException("Error al actualizar información de circuitos", MexiledException.LAYER.SER, MexiledException.ACTION.UPD);
            logger.error(ExceptionCode.DEVICE+"- Error al actualizar circuitos - {} - CODE: {}",updateView,dev.getExceptionCode());
            throw dev;
        }

    }

    @Override
    @Transactional(readOnly = false,rollbackFor = DeviceException.class)
    public DeviceView findDevice(Long deviceId) throws DeviceException {
        try{
            if(!deviceRepository.exists(deviceId))
                throw new  DeviceException("No se encontro circuito", MexiledException.LAYER.DAO, MexiledException.ACTION.VAL);
            Device device = deviceRepository.findOne(deviceId);
            DeviceView view = deviceConverter.fromEntity(device);
            return view;
        }catch (DeviceException de){
            throw de;
        }catch (ParseException parse){
            DeviceException dev = new DeviceException("Error al seleccionar circuito", MexiledException.LAYER.SER, MexiledException.ACTION.SEL);
            dev.addError("Error al parsear Hora de inicio o apagado");
            logger.error(ExceptionCode.DEVICE+"- Error al seleccionar circuito - {} - CODE: {}",deviceId,dev.getExceptionCode());
            throw dev;
        }catch (Exception ex){
            DeviceException dev = new DeviceException("Error al seleccionar circuito", MexiledException.LAYER.SER, MexiledException.ACTION.SEL);
            logger.error(ExceptionCode.DEVICE+"- Error al seleccionar circuito - {} - CODE: {}",deviceId,dev.getExceptionCode());
            throw dev;
        }
    }

    @Override
    @Transactional(readOnly = false,rollbackFor = DeviceException.class)
    public Map<String, Object> findIdByMacAddress(String macAddress) throws DeviceException {
        Map<String,Object> map = new HashMap<>();
        try{
            Device device = deviceRepository.findByMacAddress(macAddress);
            device.setInstallationStatus(DeviceEstatusConfig.INSTALADO);
            if(device==null){
                map.put("ID",0);
                map.put("message","No se encontro dispositivo en base de datos");
            }else{
                final Long deviceId = device.getDeviceId();
                map.put("ID",deviceId);
                map.put("message","Dispositivo encontrado");
                //add or update history
                DeviceHistory dh = device.getDeviceHistory();
                if(dh==null){
                    dh = new DeviceHistory();
                    dh.setDevice(device);
                    dh.setFirstRegistered(new Date());
                    device.setDeviceHistory(dh);
                }else{
                    dh.setLastRegistered(new Date());//update last registered time
                }
                historyRepository.save(dh);

            }
        }catch (NullPointerException npe){
            map.put("ID",0);
            map.put("message","No se encontro dispositivo en base de datos");
            logger.error(ExceptionCode.DEVICE+"- Error al obtener device por mac: {} ",macAddress,npe);
            throw  new DeviceException("Error al obtener dispositivo", MexiledException.LAYER.SER, MexiledException.ACTION.INS);
        }catch (Exception ex){
            map.put("ID",0);
            map.put("message","No se encontro dispositivo en base de datos");
            logger.error(ExceptionCode.DEVICE+"- Error al obtener device por mac: {} ",macAddress,ex);
            throw  new DeviceException("Error al obtener dispositivo", MexiledException.LAYER.SER, MexiledException.ACTION.INS);
        }
        return map;
    }

    @Override
    public Collection<DeviceReportView> findDevices(Long projectId, Long sectionId, Long polygonId) {
        try{
            List<Device> devices;
            if(projectId==null && sectionId==null && polygonId==null)
                devices = deviceRepository.findAll();
            else{
                Specification<Device> deviceSpecification = Specifications.where((root, criteriaQuery, cb) -> {
                    Predicate p = null;
                    if(projectId!=null){
                        Join<Device,Project> projectJoin = root.join("project");
                        p = cb.equal(projectJoin.get("projectId"),projectId);
                    }
                    if(sectionId!=null){
                        Join<Device,Section> sectionJoin = root.join("section");
                        p = p!=null ? cb.and(p,cb.equal(sectionJoin.get("sectionId"),sectionId)) : cb.equal(sectionJoin.get("sectionId"),sectionId);
                    }
                    if(polygonId!=null){
                        Join<Device,Polygon> polygonJoin = root.join("polygon");
                        p = p!=null ? cb.and(p,cb.equal(polygonJoin.get("polygonId"),polygonId)) : cb.equal(polygonJoin.get("polygonId"),polygonId);
                    }
                    return p;
                });
                devices = deviceRepository.findAll(deviceSpecification);
            }
            List<DeviceReportView> reportViewList = new ArrayList<>();
            devices.forEach(d->{
                try {
                    reportViewList.add(deviceConverter.reportFromEntity(d));
                }catch (ParseException pa){
                    logger.error("Error al parsear horas - ON:  {} - OFF: {} ",d.getLigthOffHour(),d.getLigthOnHour());
                }
            });
            return reportViewList;
        }catch (Exception ex){
            logger.error("Error al obtener listado de circuitos",ex);
            return Collections.emptyList();
        }
    }
}
