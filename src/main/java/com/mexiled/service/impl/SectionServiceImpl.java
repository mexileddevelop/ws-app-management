package com.mexiled.service.impl;

import com.mexiled.common.MexiledException;
import com.mexiled.converter.SectionConverter;
import com.mexiled.domain.Project;
import com.mexiled.domain.Section;
import com.mexiled.exception.ExceptionCode;
import com.mexiled.exception.SectionException;
import com.mexiled.repository.ProjectRepository;
import com.mexiled.repository.SectionRepository;
import com.mexiled.service.SectionService;
import com.mexiled.views.SectionView;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional(readOnly = true)
public class SectionServiceImpl implements SectionService{

    private final Logger logger = LoggerFactory.getLogger(SectionServiceImpl.class);

    private SectionRepository sectionRepository;

    private SectionConverter sectionConverter;

    private ProjectRepository projectRepository;

    @Autowired
    public void setSectionConverter(SectionConverter sectionConverter) {
        this.sectionConverter = sectionConverter;
    }

    @Autowired
    public void setSectionRepository(SectionRepository sectionRepository) {
        this.sectionRepository = sectionRepository;
    }

    @Autowired
    public void setProjectRepository(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    @Transactional(readOnly = false,rollbackFor = SectionException.class)
    public void addSection(SectionView sectionView) throws SectionException {
        try{
            if(sectionView.getProject().getProjectId()==null)
                throw new NullPointerException("Debe proporcionar proyecto");
            if(!projectRepository.exists(sectionView.getProject().getProjectId()))
                throw new SectionException("No existe el proyecto para agregar sección", MexiledException.LAYER.SER, MexiledException.ACTION.VAL);
            sectionRepository.save(sectionConverter.fromView(sectionView));
        }catch (SectionException se){
            throw se;
        }catch (NullPointerException npe){
            throw new SectionException("No existe el proyecto para agregar sección", MexiledException.LAYER.SER, MexiledException.ACTION.VAL);
        }catch (DataIntegrityViolationException dta){
            SectionException sectionException = new SectionException("Ocurrio un error al agregar Sección", MexiledException.LAYER.DAO, MexiledException.ACTION.INS);
            logger.error(ExceptionCode.SECTION+"- Error al agregar sección - {} - CODE: {} ",sectionView,sectionException.getExceptionCode(),dta);
            throw sectionException;
        }catch (ConstraintViolationException cve){
            SectionException sectionException = new SectionException("Ocurrio un error al agregar Sección", MexiledException.LAYER.DAO, MexiledException.ACTION.INS);
            logger.error(ExceptionCode.SECTION+"- Error al agregar sección - {} - CODE: {} ",sectionView,sectionException.getExceptionCode(),cve);
            throw sectionException;
        }catch (Exception ex){
            SectionException sectionException = new SectionException("Ocurrio un error al agregar Sección", MexiledException.LAYER.DAO, MexiledException.ACTION.INS);
            logger.error(ExceptionCode.SECTION+"- Error al agregar sección - {} - CODE: {} ",sectionView,sectionException.getExceptionCode(),ex);
            throw sectionException;
        }
    }

    @Override
    @Transactional(readOnly = false,rollbackFor = SectionException.class)
    public void updateSection(SectionView sectionView) throws SectionException {
        try{
            if(sectionView.getProject().getProjectId()==null)
                throw new NullPointerException("Debe proporcionar proyecto");
            if(!projectRepository.exists(sectionView.getProject().getProjectId()))
                throw new SectionException("No existe el proyecto para editar sección", MexiledException.LAYER.SER, MexiledException.ACTION.VAL);
            if(!sectionRepository.exists(sectionView.getSectionId()))
                throw new SectionException("No existe el sección a editar", MexiledException.LAYER.SER, MexiledException.ACTION.VAL);
            sectionRepository.save(sectionConverter.fromView(sectionView));
        }catch (SectionException se){
            throw se;
        }catch (NullPointerException npe){
            throw new SectionException("No existe el proyecto para editar sección", MexiledException.LAYER.SER, MexiledException.ACTION.VAL);
        }catch (DataIntegrityViolationException dta){
            SectionException sectionException = new SectionException("Ocurrio un error al editar Sección", MexiledException.LAYER.DAO, MexiledException.ACTION.UPD);
            logger.error(ExceptionCode.SECTION+"- Error al editar sección - {} - CODE: {} ",sectionView,sectionException.getExceptionCode(),dta);
            throw sectionException;
        }catch (ConstraintViolationException cve){
            SectionException sectionException = new SectionException("Ocurrio un error al editar Sección", MexiledException.LAYER.DAO, MexiledException.ACTION.UPD);
            logger.error(ExceptionCode.SECTION+"- Error al editar sección - {} - CODE: {} ",sectionView,sectionException.getExceptionCode(),cve);
            throw sectionException;
        }catch (Exception ex){
            SectionException sectionException = new SectionException("Ocurrio un error al editar Sección", MexiledException.LAYER.DAO, MexiledException.ACTION.UPD);
            logger.error(ExceptionCode.SECTION+"- Error al editar sección - {} - CODE: {} ",sectionView,sectionException.getExceptionCode(),ex);
            throw sectionException;
        }
    }

    @Override
    @Transactional(readOnly = false,rollbackFor = SectionException.class)
    public void deleteSection(Long sectionId) throws SectionException {
        try{
            if(!sectionRepository.exists(sectionId))
                throw new SectionException("No existe el sección a eliminar", MexiledException.LAYER.SER, MexiledException.ACTION.VAL);
            sectionRepository.delete(sectionId);
        }catch (SectionException se){
            throw se;
        }catch (DataIntegrityViolationException dta){
            SectionException sectionException = new SectionException("No es posible eliminar la Sección", MexiledException.LAYER.DAO, MexiledException.ACTION.DEL);
            logger.error(ExceptionCode.SECTION+"- Error al eliminar sección - {} - CODE: {} ",sectionId,sectionException.getExceptionCode(),dta);
            throw sectionException;
        }catch (ConstraintViolationException cve){
            SectionException sectionException = new SectionException("No es posible eliminar la Sección", MexiledException.LAYER.DAO, MexiledException.ACTION.DEL);
            logger.error(ExceptionCode.SECTION+"- Error al eliminar sección - {} - CODE: {} ",sectionId,sectionException.getExceptionCode(),cve);
            throw sectionException;
        }catch (Exception ex){
            SectionException sectionException = new SectionException("Ocurrio un error al eliminar Sección", MexiledException.LAYER.DAO, MexiledException.ACTION.DEL);
            logger.error(ExceptionCode.SECTION+"- Error al eliminar sección - {} - CODE: {} ",sectionId,sectionException.getExceptionCode(),ex);
            throw sectionException;
        }
    }

    @Override
    public SectionView findSection(Long sectionId) throws SectionException {
        try{
            if(!sectionRepository.exists(sectionId))
                throw new SectionException("No existe el sección en base de datos", MexiledException.LAYER.SER, MexiledException.ACTION.VAL);
            return sectionConverter.fromEntity(sectionRepository.findOne(sectionId));
        }catch (SectionException se){
            throw se;
        }catch (Exception ex){
            SectionException sectionException = new SectionException("Ocurrio un error al seleccionar Sección", MexiledException.LAYER.DAO, MexiledException.ACTION.SEL);
            logger.error(ExceptionCode.SECTION+"- Error al eliminar sección - {} - CODE: {} ",sectionId,sectionException.getExceptionCode(),ex);
            throw sectionException;
        }
    }

    @Override
    public List<SectionView> findSections(Long projectId, Boolean active) throws SectionException {
        try{
            List<Section> sections;
            if(projectId==null && active==null)
                sections = sectionRepository.findAll();
            else{
                Specification<Section> sectionSpecification = Specifications.where((root, query, cb) -> {
                    Predicate p = null;
                    Join<Section,Project> projectJoin = root.join("project");
                    if(projectId!=null)
                        p = cb.equal(projectJoin.get("projectId"),projectId);
                    if(active!=null)
                        p = p != null ? cb.and(p,cb.equal(root.get("active"),active)) : cb.equal(root.get("active"),active);
                    return p;
                });
                sections = sectionRepository.findAll(sectionSpecification);
            }

            List<SectionView> views = new ArrayList<>();
            sections.forEach(s->views.add(sectionConverter.fromEntity(s)));
            return views;
        }catch (Exception ex){
            SectionException sectionException = new SectionException("Ocurrio un error al seleccionar secciones", MexiledException.LAYER.DAO, MexiledException.ACTION.SEL);
            logger.error(ExceptionCode.SECTION+"- Error al eliminar sección - projectId: {} active: {} - CODE: {} ",projectId,active,sectionException.getExceptionCode(),ex);
            throw sectionException;
        }
    }
}
