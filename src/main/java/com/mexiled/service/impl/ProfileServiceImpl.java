package com.mexiled.service.impl;

import com.mexiled.common.MexiledException;
import com.mexiled.domain.Module;
import com.mexiled.domain.Profile;
import com.mexiled.domain.ProfilePermission;
import com.mexiled.domain.ProfilePermissionId;
import com.mexiled.exception.ExceptionCode;
import com.mexiled.exception.ProfileException;
import com.mexiled.repository.ModulePermissionRepository;
import com.mexiled.repository.ModuleRepository;
import com.mexiled.repository.ProfilePermissionRepository;
import com.mexiled.repository.ProfileRepository;
import com.mexiled.service.ProfileService;
import com.mexiled.views.NodeTree;
import com.mexiled.views.ProfileView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional(readOnly = true)
public class ProfileServiceImpl implements ProfileService{

    private final Logger logger = LoggerFactory.getLogger(ProfileServiceImpl.class);

    private ProfileRepository profileRepository;
    private ProfilePermissionRepository permissionRepository;
    private ModulePermissionRepository modulePermissionRepository;
    private ModuleRepository moduleRepository;

    @Autowired
    public void setPermissionRepository(ProfilePermissionRepository permissionRepository) {
        this.permissionRepository = permissionRepository;
    }

    @Autowired
    public void setProfileRepository(ProfileRepository profileRepository) {
        this.profileRepository = profileRepository;
    }

    @Autowired
    public void setModulePermissionRepository(ModulePermissionRepository modulePermissionRepository) {
        this.modulePermissionRepository = modulePermissionRepository;
    }

    @Autowired
    public void setModuleRepository(ModuleRepository moduleRepository) {
        this.moduleRepository = moduleRepository;
    }

    /**
     * {@inheritDoc}
     * */
    @Override
    @Transactional(readOnly = false,rollbackFor = ProfileException.class)
    public void addProfile(ProfileView profileView) throws ProfileException {
        try{
            if(profileView.getPermissions()==null || profileView.getPermissions().isEmpty())
                throw  new ProfileException(new Exception("Lista de permisios vacia"),"Agregue al menos un permiso a la lista", MexiledException.LAYER.SER.name(), MexiledException.ACTION.VAL.name());
            Profile p = convertFromView(profileView,null);
            profileRepository.save(p);
        }catch (ProfileException pe){
            throw pe;
        }catch (DataIntegrityViolationException dta){
            ProfileException pfe = new ProfileException(dta,"Error al agregar Perfil", MexiledException.LAYER.DAO.name(),
                    MexiledException.ACTION.INS.name());
            pfe.getErrorList().add("Se tienen un permiso duplicado para el perfil");
            throw pfe;
        }catch (Exception ex){
            ProfileException pfe = new ProfileException(ex,"Error al agregar Perfil",
            MexiledException.LAYER.SER.name(), MexiledException.ACTION.INS.name());
            pfe.getErrorList().add("Ocurrio un error al agregar Perfil, intente nuevamente");
            logger.error(ExceptionCode.PERMISSION+" - Ocurrio un error al insertar Perfil - CODE: {} - PROFILE: {}",pfe.getExceptionCode(),ex);
            throw pfe;
        }
    }


    /**
     * {@inheritDoc}
     * */
    @Override
    @Transactional(readOnly = false,rollbackFor = ProfileException.class)
    public void editProfile(ProfileView profileView) throws ProfileException {
        try{
            if(profileView.getPermissions()==null || profileView.getPermissions().isEmpty())
                throw  new ProfileException(new Exception("Lista de permisios vacia"),"Agregue al menos un permiso a la lista", 
                        MexiledException.LAYER.SER.name(), MexiledException.ACTION.VAL.name());
            if(!profileRepository.exists(profileView.getProfileId()))
                throw  new ProfileException(new Exception("Perfil no encontrado"),"El Perfil que desea editar no se encuentra en la base de datos",
                        MexiledException.LAYER.SER.name(), MexiledException.ACTION.VAL.name());
            Profile p = profileRepository.findOne(profileView.getProfileId());
            List<ProfilePermission> permissions = p.getPermissionList().stream().collect(Collectors.toList());
            p.setPermissionList(new ArrayList<>());
            p = convertFromView(profileView,p);
            for(ProfilePermission pf : permissions){
                if(!isPermissionInList(pf,p.getPermissionList())) {
                    permissionRepository.delete(pf);
                }
            }
            profileRepository.save(p);
        }catch (ProfileException pe){
            throw pe;
        }catch (DataIntegrityViolationException dta){
            ProfileException pfe = new ProfileException(dta,"Error al actualizar Perfil",
                    MexiledException.LAYER.DAO.name(), MexiledException.ACTION.UPD.name());
            pfe.getErrorList().add("Se tienen un permiso duplicado para el perfil");
            throw pfe;
        }catch (Exception ex){
            ProfileException pfe = new ProfileException(ex,"Error al actualizar Perfil", MexiledException.LAYER.SER.name(),
                    MexiledException.ACTION.UPD.name());
            pfe.getErrorList().add("Ocurrio un error al actualizar Perfil, intente nuevamente");
            logger.error(ExceptionCode.PROFILE+" - Ocurrio un error al actualizar Perfil - CODE: {} - PROFILE: {}",pfe.getExceptionCode(),ex);
            throw pfe;
        }
    }

    /**
     * {@inheritDoc}
     * */
    @Override
    @Transactional(readOnly = false,rollbackFor = ProfileException.class)
    public void deleteProfile(Long profileId) throws ProfileException {
        try{
            if(!profileRepository.exists(profileId))
                throw  new ProfileException(new Exception("Perfil no encontrado"),
                        "El Perfil que desea eliminar no se encuentra en la base de datos", MexiledException.LAYER.SER.name(),
                        MexiledException.ACTION.VAL.name());
            profileRepository.delete(profileId);
        }catch (ProfileException pe){
            throw  pe;
        }catch (DataIntegrityViolationException dta){
            ProfileException pfe = new ProfileException(dta,"Error al eliminar Perfil",
                    MexiledException.LAYER.DAO.name(), MexiledException.ACTION.DEL.name());
            pfe.getErrorList().add("No es posible eliminar perfil");
            throw pfe;
        }catch (Exception ex){
            ProfileException pfe = new ProfileException(ex,"Error al eliminar Perfil",
                    MexiledException.LAYER.SER.name(), MexiledException.ACTION.DEL.name());
            pfe.getErrorList().add("Ocurrio un error al eliminar Perfil, intente nuevamente");
            logger.error(ExceptionCode.PROFILE+" - Ocurrio un error al eliminar Perfil - CODE: {} - PROFILE: {}",pfe.getExceptionCode(),ex);
            throw pfe;
        }
    }

    /**
     * {@inheritDoc}
     * */
    @Override
    public ProfileView findProfile(Long profileId) throws ProfileException {
        try{
            if(!profileRepository.exists(profileId))
                throw  new ProfileException(new Exception("Perfil no encontrado"),
                        "El Perfil que desea eliminar no se encuentra en la base de datos",
                        MexiledException.LAYER.SER.name(), MexiledException.ACTION.VAL.name());
            return convertFromEntity(profileRepository.findOne(profileId),Boolean.TRUE);
        }catch (ProfileException pe){
            throw pe;
        }catch (Exception ex){
            ProfileException pfe = new ProfileException(ex,"Error al seleccionar Perfil",
                    MexiledException.LAYER.SER.name(), MexiledException.ACTION.SEL.name());
            pfe.getErrorList().add("Ocurrio un error al seleccionar detalles de Perfil, intente nuevamente");
            logger.error(ExceptionCode.PROFILE+" - Ocurrio un error al obtener Perfil - CODE: {} - PROFILE: {}",pfe.getExceptionCode(),ex);
            throw pfe;
        }
    }


    /**
     * {@inheritDoc}
     * */
    @Override
    public List<ProfileView> findAll() throws ProfileException {
        try{
            List<Profile> profiles = profileRepository.findAll();
            List<ProfileView> views = new ArrayList<>();
            for (Profile p: profiles) {
                views.add(convertFromEntity(p,Boolean.FALSE));
            }
            return views;
        }catch (Exception ex){
            ProfileException pfe = new ProfileException(ex,"Error al seleccionar Perfiles",
                    MexiledException.LAYER.SER.name(), MexiledException.ACTION.SEL.name());
            pfe.getErrorList().add("Ocurrio un error al seleccionar lista de perfiles, intente nuevamente");
            logger.error(ExceptionCode.PROFILE+" - Ocurrio un error al obtener lista de perfiles - CODE: {} - PROFILE: {}",pfe.getExceptionCode(),ex);
            throw pfe;
        }
    }


    /**
     * {@inheritDoc}
     * */
    @Override
    public List<NodeTree> findProfileTree(Long profileId) throws ProfileException {
        try{
            //obtiene la lista de módulos en el sistema
            List<Module> modules = moduleRepository.findAll();
            //lista de nodos a regresar
            List<NodeTree> nodes = new ArrayList<>();

            //estructura para no regresar nodos repetidos
            Map<Long, Object> temporal = new HashMap<>();

            modules.forEach(module -> {
                NodeTree node = new NodeTree();
                node.setActive(false);
                node.setChildren(new ArrayList<>());//se inicia arraylist
                if(!module.getModulePermissions().isEmpty()){//agregar los permisos al nodo del arbol
                    node.setChildCount(module.getModulePermissions().size());
                    module.getModulePermissions().forEach(modulePermission -> {
                        NodeTree nodePermission = new NodeTree();
                        nodePermission.setActive(false);
                        if(profileId != null ){
                            //obtener los permisos del perfil
                            Profile p = profileRepository.findOne(profileId);
                            if(!p.getPermissionList().isEmpty()){
                                //busca el permiso dentro de los permisos que tiene el perfil
                                List<ProfilePermission> permission = p.getPermissionList().stream().filter(profilePermission->profilePermission.getModulePermission()
                                        .getModulePermissionId().compareTo(modulePermission.getModulePermissionId())==0)
                                        .collect(Collectors.toList());
                                if(permission.size()!=0){
                                    nodePermission.setActive(true);
                                }
                            }
                        }
                        nodePermission.setContent(modulePermission.getName());
                        nodePermission.setId(modulePermission.getModulePermissionId());
                        nodePermission.setModule(false);
                        node.getChildren().add(nodePermission);
                    });
                }
                node.setContent(module.getName());
                node.setId(module.getModuleId());
                node.setModule(true);
                //comparacion de la cantidad tortal de permisos contra los permisos asignados en el perfil
                if(node.getChildCount().compareTo(node.getChildSelected()) == 0 && node.getChildCount() != 0){
                    node.setExpanded(true);
                    node.setActive(true);
                }

                temporal.put(module.getModuleId(), node);// agrego nodos padre

            });

            //agregar los nodos obtenidos a la lista
            temporal.keySet().stream().forEach(key-> {
                NodeTree node = (NodeTree) temporal.get(key);
                nodes.add(node);
                if(!node.getExpanded()){
                    node.setChildCount(node.getChildren().size());//numero de hijos
                    node.setChildSelected(countActiveNodes(node));//hijos seleccionados
                    if(node.getChildCount().compareTo(node.getChildSelected()) == 0 && node.getChildCount() != 0){
                        node.setExpanded(true);//expande el nod
                        node.setActive(true);//activa el nodo
                    }
                }
            });
            return nodes;
        }catch (Exception ex){
            ProfileException profileException = new ProfileException(new Exception("No fue posible obtener arbol de permisos") ,"Error al obtener arbol de permisos",
                    MexiledException.LAYER.SER.name(), MexiledException.ACTION.SEL.name());
            logger.error("Error al obtener arbol de permisos",ex);
            throw  profileException;
        }
    }

    /**
     * Método para contar el numero de elementos activos de un nodo
     * @param nodeTree
     * @return numero de nodos activos
     * */
    private Integer countActiveNodes(@NotNull NodeTree nodeTree){
        Integer count = 0;
        for(int i= 0; i< nodeTree.getChildren().size();i++){
            if(nodeTree.getChildren().get(i).getActive().booleanValue())
                count++;
        }
        return count;
    }
    private Profile convertFromView(ProfileView profileView,Profile p){
        if(p==null)
            p = new Profile();
        p.setActive(profileView.getActive());
        p.setProfileId(profileView.getProfileId());
        p.setProfileName(profileView.getProfileName());
        for (Long permission: profileView.getPermissions()) {
            ProfilePermission pf = new ProfilePermission();
            ProfilePermissionId pfid = new ProfilePermissionId();
            pfid.setModulePermission(modulePermissionRepository.findOne(permission));
            pfid.setProfile(p);
            pf.setCreatedDate(new Date());
            pf.setPermissionId(pfid);
            pf.setProfile(p);
            p.getPermissionList().add(pf);
        }


        return p;
    }

    private Boolean isPermissionInList(ProfilePermission pp,List<ProfilePermission> permissions){
        List<ProfilePermission> permission = permissions.stream().filter(p-> p.getModulePermission().getModulePermissionId().compareTo(pp.getModulePermission().getModulePermissionId()) == 0).collect(Collectors.toList());
        return !permission.isEmpty();
    }

    private ProfileView convertFromEntity(Profile p,Boolean fullConvert) throws ProfileException{
        ProfileView pv = new ProfileView();
        pv.setCreatedDate(p.getCreatedDate());
        pv.setActive(p.getActive());
        pv.setProfileName(p.getProfileName());
        pv.setProfileId(p.getProfileId());
        //add permissions
        if(fullConvert){
            pv.setTree(findProfileTree(p.getProfileId()));
        }
        return pv;
    }
}
