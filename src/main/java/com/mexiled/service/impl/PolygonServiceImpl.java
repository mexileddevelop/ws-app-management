package com.mexiled.service.impl;

import com.mexiled.common.MexiledException;
import com.mexiled.converter.PolygonConverter;
import com.mexiled.domain.Polygon;
import com.mexiled.domain.Section;
import com.mexiled.exception.ExceptionCode;
import com.mexiled.exception.PolygonException;
import com.mexiled.repository.PolygonRepository;
import com.mexiled.repository.SectionRepository;
import com.mexiled.service.PolygonService;
import com.mexiled.views.PolygonView;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Victor de la Cruz
 * @version 1.0.0
 */
@Service
@Transactional(readOnly = true)
public class PolygonServiceImpl implements PolygonService {

    private final Logger logger = LoggerFactory.getLogger(PolygonServiceImpl.class);

    private PolygonRepository polygonRepository;

    private SectionRepository sectionRepository;

    private PolygonConverter polygonConverter;

    @Autowired
    public void setSectionRepository(SectionRepository sectionRepository) {
        this.sectionRepository = sectionRepository;
    }

    @Autowired
    public void setPolygonRepository(PolygonRepository polygonRepository) {
        this.polygonRepository = polygonRepository;
    }

    @Autowired
    public void setPolygonConverter(PolygonConverter polygonConverter) {
        this.polygonConverter = polygonConverter;
    }

    @Override
    @Transactional(readOnly = false,rollbackFor = PolygonException.class)
    public void addPolygon(PolygonView polygonView) throws PolygonException {
        try{
            Polygon polygon = polygonConverter.fromView(polygonView);
            polygon.setSection(sectionRepository.findOne(polygonView.getSection().getSectionId()));
            polygonRepository.save(polygon);
        }catch (Exception ex){
            PolygonException polygonException = new PolygonException("Ocurrio un error al agregar el poligono", MexiledException.LAYER.DAO, MexiledException.ACTION.INS);
            logger.info(ExceptionCode.POLYGON+"- Error al agregar poligono - {} - CODE: {} ",polygonView,polygonException.getExceptionCode(),ex);
            throw polygonException;
        }
    }

    @Override
    @Transactional(readOnly = false,rollbackFor = PolygonException.class)
    public void updatePolygon(PolygonView polygonView) throws PolygonException {
        try{
            if(!polygonRepository.exists(polygonView.getPolygonId()))
                throw new PolygonException("No existe el poligono a editar", MexiledException.LAYER.SER, MexiledException.ACTION.VAL);
            Polygon polygon = polygonConverter.fromView(polygonView);
            polygon.setSection(sectionRepository.findOne(polygonView.getSection().getSectionId()));
            polygonRepository.save(polygon);
        }catch (PolygonException e){
            throw e;
        }catch (Exception ex){
            PolygonException polygonException = new PolygonException("Ocurrio un error al editar el poligono", MexiledException.LAYER.DAO, MexiledException.ACTION.UPD);
            logger.info(ExceptionCode.POLYGON+"- Error al editar poligono - {} - CODE: {} ",polygonView,polygonException.getExceptionCode(),ex);
            throw polygonException;
        }
    }

    @Override
    @Transactional(readOnly = false,rollbackFor = PolygonException.class)
    public void deletePolygon(Long polygonId) throws PolygonException {
        try{
            if(!polygonRepository.exists(polygonId))
                throw new PolygonException("No existe el poligono a eliminar", MexiledException.LAYER.SER, MexiledException.ACTION.VAL);
            polygonRepository.delete(polygonId);
        }catch (PolygonException e){
            throw e;
        }catch (DataIntegrityViolationException dta){
            PolygonException polygonException = new PolygonException("No es posible eliminar el poligono", MexiledException.LAYER.DAO, MexiledException.ACTION.DEL);
            logger.info(ExceptionCode.POLYGON+"- Error al eliminar poligono - {} - CODE: {} ",polygonId,polygonException.getExceptionCode(),dta);
            throw polygonException;
        }catch (ConstraintViolationException cve){
            PolygonException polygonException = new PolygonException("No es posible eliminar el poligono", MexiledException.LAYER.DAO, MexiledException.ACTION.DEL);
            logger.info(ExceptionCode.POLYGON+"- Error al eliminar poligono - {} - CODE: {} ",polygonId,polygonException.getExceptionCode(),cve);
            throw polygonException;
        }catch (Exception e){
            PolygonException polygonException = new PolygonException("Ocurrio un error al eliminar el poligono", MexiledException.LAYER.DAO, MexiledException.ACTION.DEL);
            logger.info(ExceptionCode.POLYGON+"- Error al eliminar poligono - {} - CODE: {} ",polygonId,polygonException.getExceptionCode(),e);
            throw polygonException;
        }
    }

    @Override
    public PolygonView findById(Long polygonId) throws PolygonException {
        try{
            if(!polygonRepository.exists(polygonId))
                throw new PolygonException("No existe el poligono en base de datos", MexiledException.LAYER.SER, MexiledException.ACTION.VAL);
            return polygonConverter.fromEntity(polygonRepository.findOne(polygonId));
        }catch (PolygonException pe){
            throw pe;
        }catch (Exception e){
            PolygonException polygonException = new PolygonException("Ocurrio un error al seleccionar el poligono", MexiledException.LAYER.SER, MexiledException.ACTION.SEL);
            logger.info(ExceptionCode.POLYGON+"- Error al seleccionar poligono - {} - CODE: {} ",polygonId,polygonException.getExceptionCode(),e);
            throw polygonException;
        }
    }

    @Override
    public List<PolygonView> findPolygons(Long sectionId, Boolean active) throws PolygonException {
        try{
            List<PolygonView> views = new ArrayList<>();
            List<Polygon> poligons;
            if(sectionId==null && active==null){
                poligons = polygonRepository.findAll();
            }else{
                Specification<Polygon> polySpec= Specifications.where((root, query, cb) -> {
                    Predicate p = null;
                    if(sectionId!=null){
                        Join<Polygon,Section> polyJoin = root.join("section");
                        p = cb.equal(polyJoin.get("sectionId"),sectionId);
                    }
                    if(active!=null)
                        p = p!= null ? cb.and(p,cb.equal(root.get("active"),active)): cb.equal(root.get("active"),active);
                    return p;
                });
                poligons = polygonRepository.findAll(polySpec);
            }
            poligons.forEach(p->views.add(polygonConverter.fromEntity(p)));
            return views;
        }catch (Exception ex){
            PolygonException polygonException = new PolygonException("Ocurrio un error al seleccionar la lista de poligonos", MexiledException.LAYER.SER, MexiledException.ACTION.SEL);
            logger.info(ExceptionCode.POLYGON+"- Error al seleccionar lista de poligonos - CODE: {} ",polygonException.getExceptionCode(),ex);
            throw polygonException;
        }
    }
}
