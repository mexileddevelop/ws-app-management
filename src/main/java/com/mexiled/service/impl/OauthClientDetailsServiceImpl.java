package com.mexiled.service.impl;

import com.mexiled.common.MexiledException;
import com.mexiled.converter.OauthClientDetailsConverter;
import com.mexiled.domain.OauthClientDetails;
import com.mexiled.exception.ExceptionCode;
import com.mexiled.exception.OauthException;
import com.mexiled.repository.OauthClientDetailsRepository;
import com.mexiled.service.OauthClientDetailsService;
import com.mexiled.views.OauthClientDetailsView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
 * @author Victor de la Cruz
 * @version 1.0.0
 */
@Service
@Transactional(readOnly = true)
public class OauthClientDetailsServiceImpl implements OauthClientDetailsService{

    private final Logger logger = LoggerFactory.getLogger(OauthClientDetailsServiceImpl.class);

    private OauthClientDetailsRepository oauthRepository;

    private OauthClientDetailsConverter converter;

    @Autowired
    public void setOauthRepository(OauthClientDetailsRepository oauthRepository) {
        this.oauthRepository = oauthRepository;
    }

    @Autowired
    public void setConverter(OauthClientDetailsConverter converter) {
        this.converter = converter;
    }

    /**
     * {@inheritDoc}
     * */
    @Override
    @Transactional(readOnly = false,rollbackFor = OauthException.class)
    public void addOauthClientDetail(OauthClientDetailsView oauthClientDetails) throws OauthException {
        try{
            oauthRepository.save(converter.fromView(oauthClientDetails));
        }catch (DataAccessException dae){
            OauthException oauthException = new OauthException("Error de acceso a la base de datos", MexiledException.LAYER.DAO, MexiledException.ACTION.INS);
            logger.error(ExceptionCode.OAUTH+"- Error al agregar nueva autorización - {} ",oauthClientDetails,dae);
            throw oauthException;
        }catch (Exception ex){
            OauthException oauthException = new OauthException("Error agregar nuevo acceso, intente nuevamente", MexiledException.LAYER.SER, MexiledException.ACTION.INS);
            logger.error(ExceptionCode.OAUTH+"- Error al agregar nueva autorización - {} ",oauthClientDetails,ex);
            throw oauthException;
        }
    }

    /**
     * {@inheritDoc}
     * */
    @Override
    @Transactional(readOnly = false,rollbackFor = OauthException.class)
    public void editOauthClientDetail(OauthClientDetailsView clientDetails) throws OauthException {
        try{
            if(!oauthRepository.exists(clientDetails.getId()))
                throw new OauthException("No se encontro objeto a editar", MexiledException.LAYER.SER, MexiledException.ACTION.VAL);
            oauthRepository.save(converter.fromView(clientDetails));
        }catch (OauthException oauth){
            throw oauth;
        }catch (DataAccessException dae){
            OauthException oauthException = new OauthException("Error de acceso a la base de datos", MexiledException.LAYER.DAO, MexiledException.ACTION.UPD);
            logger.error(ExceptionCode.OAUTH+"- Error al editar autorización - {} ",clientDetails,dae);
            throw oauthException;
        }catch (Exception ex){
            OauthException oauthException = new OauthException("Error editar acceso, intente nuevamente", MexiledException.LAYER.SER, MexiledException.ACTION.UPD);
            logger.error(ExceptionCode.OAUTH+"- Error al editar autorización - {} ",clientDetails,ex);
            throw oauthException;
        }
    }


    /**
     * {@inheritDoc}
     * */
    @Override
    public OauthClientDetailsView finOauthClientDetails(Long id) throws OauthException {
        try{
            if(!oauthRepository.exists(id))
                throw new OauthException("No se encontro objeto a seleccionar", MexiledException.LAYER.SER, MexiledException.ACTION.VAL);
            return converter.fromEntity(oauthRepository.findOne(id));
        }catch (OauthException oauth){
            throw oauth;
        }catch (DataAccessException dae){
            OauthException oauthException = new OauthException("Error de acceso a la base de datos", MexiledException.LAYER.DAO, MexiledException.ACTION.SEL);
            logger.error(ExceptionCode.OAUTH+"- Error al seleccionar autorización - {} ",id,dae);
            throw oauthException;
        }catch (Exception ex){
            OauthException oauthException = new OauthException("Error al seleccionar acceso, intente nuevamente", MexiledException.LAYER.SER, MexiledException.ACTION.SEL);
            logger.error(ExceptionCode.OAUTH+"- Error al seleccionar autorización - {} ",id,ex);
            throw oauthException;
        }
    }


    /**
     * {@inheritDoc}
     * */
    @Override
    public List<OauthClientDetailsView> findAll() throws OauthException {
        try{
            List<OauthClientDetails> entities = oauthRepository.findAll();
            List<OauthClientDetailsView> views = new ArrayList<>();
            entities.forEach(e->views.add(converter.fromEntity(e)));
            return views;
        }catch (DataAccessException dae){
            OauthException oauthException = new OauthException("Error de acceso a la base de datos", MexiledException.LAYER.DAO, MexiledException.ACTION.SEL);
            logger.error(ExceptionCode.OAUTH+"- Error al seleccionar lista de autorizaciiones - {} ",dae);
            throw oauthException;
        }catch (Exception ex){
            OauthException oauthException = new OauthException("Ocurrrio un error al seleccionar la lista de accesos", MexiledException.LAYER.SER, MexiledException.ACTION.SEL);
            logger.error(ExceptionCode.OAUTH+"- Error al seleccionar lista de autorizaciiones - {} ",ex);
            throw oauthException;
        }
    }

    /**
     * {@inheritDoc}
     * */
    @Override
    @Transactional(readOnly = false,rollbackFor = OauthException.class)
    public void deleteClientDetail(Long id) throws OauthException {
        try{
            if(!oauthRepository.exists(id))
                throw new OauthException("No se encontro objeto a eliminar", MexiledException.LAYER.SER, MexiledException.ACTION.VAL);
            oauthRepository.delete(id);
        }catch (OauthException oauth){
            throw oauth;
        }catch (DataAccessException dae){
            OauthException oauthException = new OauthException("Error de acceso a la base de datos", MexiledException.LAYER.DAO, MexiledException.ACTION.DEL);
            logger.error(ExceptionCode.OAUTH+"- Error al eliminar autorización - {} ",id,dae);
            throw oauthException;
        }catch (Exception ex){
            OauthException oauthException = new OauthException("Error al eliminar acceso, intente nuevamente", MexiledException.LAYER.SER, MexiledException.ACTION.DEL);
            logger.error(ExceptionCode.OAUTH+"- Error al eliminar autorización - {} ",id,ex);
            throw oauthException;
        }
    }
}
