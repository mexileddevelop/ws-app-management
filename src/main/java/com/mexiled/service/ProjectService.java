package com.mexiled.service;


import com.mexiled.exception.ProjectException;
import com.mexiled.views.ProjectView;

import java.util.List;

/**
 * @author Victor de la Cruz
 * @version 1.0.0
 * **/
public interface ProjectService {

    /**
     * Método para agregar nuevos proyectos
     * @param view datos de projecto a guardar
     * @throws ProjectException si ocurre algún error al guardar el proyecto<br/>
     * No pueden existir dos proyectos con el mismo código
     * */
    void addProject(ProjectView view) throws ProjectException;

    /**
     * Método para editar datos de un proyecto
     * @param view datos de projecto a editar
     * @throws ProjectException si ocurre algún error al editar el proyecto<br/>
     * No pueden existir dos proyectos con el mismo código
     * */
    void updateProject(ProjectView view) throws ProjectException;

    /**
     * Método para eliminar un proyecto de la base de datos
     * @param projectId Identificador único de proyecto
     * @throws ProjectException Si ocurre un error al eliminar el proyecto o se tiene referencias en algúna tabla
     * */
    void deleteProject(Long projectId) throws ProjectException;

    /**
     * Método para obtener los detalles de un proyecto
     * @param projectId Identificador único de proyecto
     * @throws ProjectException si ocurre un error al seleccionar el proyecto en la base de datos
     * @return detalles de proyecto
     * */
    ProjectView findProject(Long projectId) throws ProjectException;

    /**
     * Método para obtener la lista de proyectos
     * @param active - opcional para filtrado por activos o inactivos
     * @throws ProjectException si ocurre un error al seleccionar la lista de proyectos
     * @return lista de proyectos seleccionados sin paginar
     * */
    List<ProjectView> findProjects(Boolean active) throws ProjectException;
}
