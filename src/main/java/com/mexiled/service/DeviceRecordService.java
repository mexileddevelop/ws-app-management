package com.mexiled.service;

import com.mexiled.exception.RecordException;
import com.mexiled.views.GraphReportView;
import com.mexiled.views.MeassureReportView;

import java.util.List;

public interface DeviceRecordService {
    void addRecords(List<MeassureReportView> record) throws RecordException;
    /**
     * Método para obtener reporte de mediciones de un dispositivo
     * @param deviceId identificador único de dispositivo para extraer mediciones
     * @param type tipo de reporte - Dia 'D', Semana 'S' - Mes 'M'
     * @return GraphReportView reporte obtenido de dispositivo
     * @throws RecordException si ocurre algún error al obtener reporte de dispositivo
     * */
    GraphReportView findGraphDeviceByIdAndType(Long deviceId,String type) throws RecordException;
}
