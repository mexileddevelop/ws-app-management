package com.mexiled.service;


import com.mexiled.exception.ModuleException;
import com.mexiled.views.ModuleView;

import java.util.List;

/**
 * @author Victor de la Cruz
 * @version 1.0.0
 */
public interface ModuleService {

    /**
     * Método para crear un nuevo modulo en base de datos
     * @param moduleView el modulo a insertar junto con los permisos
     * @throws ModuleException si ocurre algún error al guardar el módulo
     * */
    void addModule(ModuleView moduleView) throws ModuleException;
    /**
     * Método para editar un modulo en base de datos
     * @param moduleView el modulo a editar junto con los permisos
     * @throws ModuleException si ocurre algún error al editar el módulo
     * */
    void editModule(ModuleView moduleView)throws ModuleException;
    /**
     * Método para eliminar un módulo junto con sus permisos
     * @param moduleId identificador único de módulo
     * @throws ModuleException Si no es posible eliminar el módulo
     * */
    void deleteModule(Long moduleId) throws ModuleException;
    /**
     * Método para obtener los detalles de un módulo por id
     * @param moduleId identificador único de módulo
     * @throws ModuleException si no es posible seleccionar el módulo, o no existe
     * @return módulo encontrado en base de datos
     * */
    ModuleView findModule(Long moduleId) throws ModuleException;
    /**
     * Método para obtener la lista de módulos en base de datos
     * @throws ModuleException si ocurre algún error al seleccionar la lista de modulos
     * @return lista de módulos en base de datos sin paginar
     * */
    List<ModuleView> findAll() throws ModuleException;
}
