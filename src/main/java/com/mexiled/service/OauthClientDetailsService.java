package com.mexiled.service;

import com.mexiled.exception.OauthException;
import com.mexiled.views.OauthClientDetailsView;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author Victor de la Cruz
 * @version 1.0.0
 */
public interface OauthClientDetailsService{

    /**
     * Método para agregar nueva autorización oauth a la base de datos
     * @param oauthClientDetails la nueva autorización que se va a guardar
     * @throws OauthException si ocurre alguna excepción al guardar el objeto en base de datos
     * */
    void addOauthClientDetail(OauthClientDetailsView oauthClientDetails) throws OauthException;

    /**
     * Método para editar detalles de autorizacion oatuh de cliente
     * @param clientDetails objeto a editar en base de datos
     * @throws OauthException
     * <br/> 1.- Si el objeto a editar no se encuentra en base de datos
     * <br/> 2.- Si ocurre algpun error al editar el objeto en base de datos
     * */
    void editOauthClientDetail(OauthClientDetailsView clientDetails) throws OauthException;

    /**
     * Método para obtener detalles de autorización
     * @param id ID único del cual se obtendrn los detalles
     * @return Objeto con la información requerida
     * @throws OauthException
     * <br/> 1.- Si el objeto a seleccionar no se encuentra en base de datos
     * <br/> 2.- Si ocurrre algún error al momento de hacer la selección en la base de datos
     * */
    OauthClientDetailsView finOauthClientDetails(Long id) throws OauthException;


    /**
     * Método para obtener lista de autorizaciones de forma paginada
     * @return lista de autorizaciones oauth
     * @throws OauthException cuando no se puede obtener el resultado en la base de datos
     * */
    List<OauthClientDetailsView> findAll() throws OauthException;

    /**
     * Método para eliminar una autorización en la base de datos
     * @param id ID único del objeto que se va a eliminar de la base de datos
     * @throws OauthException
     * <br/> 1.- Si no se encuentra la autorización en base de datos
     * <br/> 2.- Si ocurre algún errro al eliminar la autoptización
     * */
    void deleteClientDetail(@NotNull Long id) throws OauthException;
}
