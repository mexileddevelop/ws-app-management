package com.mexiled.service;

import com.mexiled.converter.DeviceException;
import com.mexiled.views.DeviceReportView;
import com.mexiled.views.DeviceUpdateView;
import com.mexiled.views.DeviceView;

import java.util.Collection;
import java.util.Map;

public interface DeviceService {

    void addDevice(DeviceView view) throws DeviceException;
    void updateDevice(DeviceView view) throws DeviceException;
    void deleteDevice(Long deviceId) throws DeviceException;
    void updateByProjectSectionOrPoylgonId(DeviceUpdateView updateView) throws DeviceException;
    DeviceView findDevice(Long deviceId) throws DeviceException;
    Map<String,Object> findIdByMacAddress(String macAddress) throws DeviceException;
    Collection<DeviceReportView> findDevices(Long projectId,Long sectionId,Long polygonId);
}
