package com.mexiled.configuration;

import com.mexiled.domain.UserApp;
import com.mexiled.repository.UserAppRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.stereotype.Component;

import java.util.*;


/**
 * @author Victor de la Cruz
 * @version 1.0.0
 */
@Component
public class AccessTokenExtraAttributesEnhancer implements TokenEnhancer {

    private final Logger logger = LoggerFactory.getLogger(AccessTokenExtraAttributesEnhancer.class);

    private List<TokenEnhancer> delegates = Collections.emptyList();

    private UserAppRepository userAppRepository;

    @Autowired
    public void setUserAppRepository(UserAppRepository userAppRepository) {
        this.userAppRepository = userAppRepository;
    }

    public void setTokenEnhancers(List<TokenEnhancer> delegates) {
        this.delegates = delegates;
    }

    /**
     * Se sobreescribe método para agregar más detalles a la respuesta de login por oauth, se agrega información extra a la sesión generada por oauth
     * **/
    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication){
        DefaultOAuth2AccessToken tempResult = (DefaultOAuth2AccessToken) accessToken;
        final Map<String, Object> additionalInformation = new HashMap<>();
        Collection<GrantedAuthority> infoValue=authentication.getAuthorities();
        List<String> roles = new ArrayList<>();
        for (GrantedAuthority authority : infoValue) {
            /**
             *  Se obtiene la lista de permisos
             * */
            roles.addAll(Arrays.asList(authority.getAuthority().split(",")));
        }
        /**
         * Se agrega la lista de permisos
         * */
        additionalInformation.put("roles",roles );

        try{
            User principal = (User) authentication.getPrincipal();
            UserApp userApp = userAppRepository.findByUsername(principal.getUsername());
            additionalInformation.put("username",userApp.getUsername());
            additionalInformation.put("name",userApp.getName());
            additionalInformation.put("profileId",userApp.getProfile().getProfileId());
        }catch (Exception ex){
            logger.error("Error al obtener detalles de usuario",ex);
        }

        tempResult.setAdditionalInformation(additionalInformation);

        OAuth2AccessToken result = tempResult;
        for (TokenEnhancer enhancer : delegates) {
            result = enhancer.enhance(result, authentication);
        }
        return result;
    }
}
