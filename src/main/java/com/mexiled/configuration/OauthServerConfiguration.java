package com.mexiled.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;
import org.springframework.security.provisioning.JdbcUserDetailsManager;

import javax.sql.DataSource;


/**
 * @author Victor de la Cruz
 * @version 1.0.0
 */
@Configuration
@EnableAuthorizationServer
public class OauthServerConfiguration extends AuthorizationServerConfigurerAdapter{

    private AuthenticationManager authenticationManager;

    @Autowired
    private DataSource dataSource;

    @Autowired
    private AccessTokenExtraAttributesEnhancer acessTokenExtraAttributesEnhancer;

    @Autowired
    public void setAuthenticationManager(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients.jdbc(dataSource);
    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        endpoints.approvalStoreDisabled()
                .tokenEnhancer(acessTokenExtraAttributesEnhancer)
                .authenticationManager(authenticationManager)
                .tokenStore(new JdbcTokenStore(dataSource))
                .userDetailsService(userDetailsService());
    }

    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
        security.tokenKeyAccess("permitAll")
                .checkTokenAccess("isAuthenticated()").allowFormAuthenticationForClients();
    }



    @Bean
    public UserDetailsService userDetailsService(){
        JdbcUserDetailsManager jdbcUserDetailsManager = new JdbcUserDetailsManager();
        jdbcUserDetailsManager.setDataSource(dataSource);
        jdbcUserDetailsManager.setUsersByUsernameQuery("SELECT username,password,active FROM user_app WHERE username=?");
        jdbcUserDetailsManager.setAuthoritiesByUsernameQuery("SELECT u.username,m.code FROM user_app u " +
                "INNER JOIN profile p ON u.profile_profile_id = p.profile_id " +
                "INNER JOIN profile_permission uh ON p.profile_id = uh.profile_id " +
                "INNER JOIN module_permission m ON uh.module_permission_id = m.module_permission_id " +
                "WHERE username=?");
        return jdbcUserDetailsManager;
    }

}
