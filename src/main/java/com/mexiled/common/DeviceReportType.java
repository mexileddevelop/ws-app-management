package com.mexiled.common;

public class DeviceReportType {
    public static final String DAY="D";
    public static final String WEEK = "S";
    public static final String MONTH = "M";
}
