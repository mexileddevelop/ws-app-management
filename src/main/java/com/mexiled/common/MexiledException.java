package com.mexiled.common;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Victor de la Cruz
 * @version 1.0.0
 * */
public abstract class MexiledException extends Exception{

    public static enum LAYER  {DAO,SER,CNT};
    public static enum ACTION {INS,SEL,DEL,UPD,VAL,PAR};

    public MexiledException(){}

    public MexiledException(Exception ex){
        super(ex);
    }

    public MexiledException(String message){
        super(message);
    }

    public MexiledException(Exception ex,String message){
        super(ex);
    }

    /** Specific error code*/
    private String errorCode = "";
    /** Specific error list messages*/
    private List<String> errorList = new ArrayList<>();
    /**
     * Gets specific error code
     * @return Error code
     */
    public String getErrorCode() {
        return errorCode;
    }
    /**
     * Sets specific error code
     * @param nErrorCode
     */
    private void setErrorCode(String nErrorCode) {
        this.errorCode = nErrorCode;
    }

    /** This is used by the controller advice */
    public void addError(String error){
        this.errorList.add(error);
    }
    /** Abstract method to Gets the Exception Codes */
    public abstract String getExceptionCode();

    /** Get List of error messages */
    public List<String> getErrorList() {
        return errorList;
    }


    @Override
    public String toString() {
        return "MexiledException{" +
                "errorCode='" + errorCode + '\'' +
                ", errorList=" + errorList +", ExceptionCode: "+getExceptionCode()+
                '}';
    }
}
