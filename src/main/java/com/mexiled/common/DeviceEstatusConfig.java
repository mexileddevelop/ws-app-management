package com.mexiled.common;

public class DeviceEstatusConfig {
    //installation
    public static final String PROYECTADO = "Proyectado";
    public static final String INSTALADO  = "Instalado";
    //status after install
    public static final String ON = "Encendido";
    public static final String OFF = "Apagado";
    public static final String ON_ERROR = "Encendido Fuera de Horario";
    public static final String DISCONNECTED = "Desconectado";
    public static final String OFF_OUT = "Dispositivo sin encender";

}
