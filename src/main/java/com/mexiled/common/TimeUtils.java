package com.mexiled.common;

import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Component
public class TimeUtils {

    public final String HOUR_FORMAT = "HH:mm";
    public final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public Integer MINUTE = Calendar.MINUTE;
    public Integer DAY = Calendar.DAY_OF_YEAR;
    public Integer HOUR = Calendar.HOUR;
    public Integer WEEK = Calendar.WEEK_OF_YEAR;
    public Integer MONTH = Calendar.MONTH;
    public Date parseHour(String hour,String format)throws ParseException{
        DateFormat df = new SimpleDateFormat(format);
        Calendar c = Calendar.getInstance();
        c.setTime(df.parse(hour));
        return c.getTime();
    }

    public String formatHour(Date c,String format) throws ParseException{
        DateFormat df = new SimpleDateFormat(format);
        return df.format(c);
    }

    public Date addTimeToDate(Date date,Integer timeToAdd,Integer addTo){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(addTo,timeToAdd);
        return calendar.getTime();
    }


}
