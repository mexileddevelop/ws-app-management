package com.mexiled.rest;

import com.mexiled.common.MexiledController;
import com.mexiled.common.MexiledException;
import com.mexiled.exception.ExceptionCode;
import com.mexiled.service.ProfileService;
import com.mexiled.views.NodeTree;
import com.mexiled.views.ProfileView;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Victor de la Cruz
 * @version 1.0.0
 */
@RestController
@RequestMapping("profiles")
@Api(description = "Perfiles de usuario",consumes = "application/json",produces = "application/json")
public class ProfileRest extends MexiledController{

    private final Logger logger = LoggerFactory.getLogger(ProfileRest.class);

    private ProfileService profileService;

    @Autowired
    public void setProfileService(ProfileService profileService) {
        this.profileService = profileService;
    }

    /**
     * Método para guardar un nuevo perfil en base de datos
     * @param profileView información de perfil junto con permisos a guardar
     * @throws MexiledException si ocurre un error al guardar la información de perfil
     * */
    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public void addProfile(@RequestBody @Validated ProfileView profileView) throws MexiledException{
        logger.info(ExceptionCode.PROFILE+"- Crear nuevo perfil - {}",profileView);
        this.profileService.addProfile(profileView);
    }

    /**
     * Método para actualizar perfil junto con sus permisos
     * @param profileView información a actualizar
     * @throws MexiledException si ocurre algún error al actualizar la información del perfil
     * */
    @RequestMapping(method = RequestMethod.PUT,value = "{profileId}")
    @ResponseStatus(HttpStatus.OK)
    public void editProfile(@RequestBody @Validated ProfileView profileView, @PathVariable()Long profileId) throws MexiledException{
        logger.info(ExceptionCode.PROFILE+"- Editar perfil - {} - {}",profileId,profileView);
        profileView.setProfileId(profileId);
        this.profileService.editProfile(profileView);
    }

    /**
     * Método para eliminar un perfil junto con su lista de permisos
     * @param profileId ID único de perfil a eliminar
     * @throws MexiledException si no es posible eliminar el perfil seleccionado
     * */
    @RequestMapping(method = RequestMethod.DELETE,value = "{profileId}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteProfile(@PathVariable()Long profileId) throws MexiledException{
        logger.info(ExceptionCode.PROFILE+"- Eliminar perfil: {}",profileId);
        this.profileService.deleteProfile(profileId);
    }

    /**
     * Método para obtener el detalle del perfil junto con sus permisos
     * @param profileId ID único de perfil a seleccionar
     * @throws MexiledException si no se encuentra el perfil o no es posible realizar la query
     * @return Vista parseada de perfil
     * */
    @RequestMapping(method = RequestMethod.GET,value = "{profileId}")
    @ResponseStatus(HttpStatus.OK)
    public ProfileView findProfile(@PathVariable()Long profileId) throws MexiledException{
        logger.info(ExceptionCode.PROFILE+"- Obtener detalles de perfil: {}",profileId);
        return this.profileService.findProfile(profileId);
    }

    /**
     * Método para obtener lista completa de los perfiles en base de datos
     * @throws MexiledException si ocurre algún error al realizar la selección en base de datos
     * @return Lista de perfiles
     * */
    @RequestMapping(method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public List<ProfileView> findAll() throws MexiledException{
        logger.info(ExceptionCode.PROFILE+"- Obtener lista de perfiles");
        return this.profileService.findAll();
    }

    /**
     * Método para obtener la lista de permisos disponibles
     * @return lista de permisos
     * @throws MexiledException si ocurre algún error al generar el arbol de permisos
     * */
    @RequestMapping(method = RequestMethod.GET,value = "/permissions/tree")
    @ResponseStatus(HttpStatus.OK)
    public List<NodeTree> findProfileTree() throws MexiledException{
        logger.info(ExceptionCode.PROFILE+"- obtener arbol de permisos");
        return  this.profileService.findProfileTree(null);
    }
}
