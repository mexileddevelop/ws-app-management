package com.mexiled.rest;


import com.mexiled.common.MexiledController;
import com.mexiled.exception.ExceptionCode;
import com.mexiled.exception.PolygonException;
import com.mexiled.service.PolygonService;
import com.mexiled.views.PolygonView;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("polygons")
@Api(description = "Manejo de poligonos")
public class PolygonRest extends MexiledController{

    private final Logger logger = LoggerFactory.getLogger(PolygonRest.class);

    private PolygonService polygonService;

    @Autowired
    public void setPolygonService(PolygonService polygonService) {
        this.polygonService = polygonService;
    }


    /**
     * Método para agregar un nuevo poligon
     * @param polygonView datos de poligono a guardar
     * @throws PolygonException si no es posible guardar el nuevo poligono
     * */
    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void addPolygon(@RequestBody @Validated PolygonView polygonView) throws PolygonException{
        logger.info(ExceptionCode.POLYGON+"- Agregar nuevo poligono - {}",polygonView);
        polygonService.addPolygon(polygonView);
    }

    /**
     * Método para editar los datos de un poligono
     * @param polygonView datos a editar del poligono
     * @throws PolygonException si ocurre un error al editar el poligono
     * */
    @RequestMapping(method = RequestMethod.PUT,value = "{polygonId}")
    @ResponseStatus(HttpStatus.OK)
    public void updatePolygon(@RequestBody @Validated PolygonView polygonView,@PathVariable Long polygonId) throws PolygonException{
        logger.info(ExceptionCode.POLYGON+"- Editar poligono: {} - ID: {}",polygonView,polygonId);
        polygonService.updatePolygon(polygonView);
    }

    /**
     * Método para eliminar un poligono de la base de datos
     * @param polygonId identificador para eliminar poligono
     * @throws PolygonException si no es posible eliminar el poligono
     * */
    @RequestMapping(method = RequestMethod.DELETE,value = "{polygonId}")
    @ResponseStatus(HttpStatus.OK)
    void deletePolygon(@PathVariable  Long polygonId) throws PolygonException{
        logger.info(ExceptionCode.POLYGON+"- Eliminar poligono . {} ",polygonId);
        polygonService.deletePolygon(polygonId);
    }

    /**
     * Método para obtener los detalles de un poligono
     * @param polygonId identificador del poligono a obtener
     * @return detalles seleccionados del poligono
     * @throws PolygonException si no es posible obtener el poligono
     * */
    @RequestMapping(method = RequestMethod.GET,value = "{polygonId}")
    @ResponseStatus(HttpStatus.OK)
    public PolygonView findById(@PathVariable Long polygonId) throws PolygonException{
        logger.info(ExceptionCode.POLYGON+"- Obtener poligono - {}",polygonId);
        return polygonService.findById(polygonId);
    }

    /**
     * Método para obtener lista de poligonos filtrados por seccion o activos
     * @param active filtro para buscar activos o  inactivos
     * @param sectionId filtro para buscar por seccion
     * @return lista de poligonos seleccionados
     * @throws PolygonException si no es posible realizar la selección en base de datos
     * */
    @RequestMapping(method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public List<PolygonView> findPolygons(Long sectionId, Boolean active) throws PolygonException{
        logger.info(ExceptionCode.POLYGON+"- Obtener lista de poligonos - ID: {} -active: {}",sectionId,active);
        return polygonService.findPolygons(sectionId,active);
    }
}

