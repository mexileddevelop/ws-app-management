package com.mexiled.rest;

import com.mexiled.common.MexiledController;
import com.mexiled.common.MexiledException;
import com.mexiled.exception.ExceptionCode;
import com.mexiled.exception.ModuleException;
import com.mexiled.service.ModuleService;
import com.mexiled.views.ModuleView;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Victor de la Cruz
 * @version 1.0.0
 */
@RestController
@RequestMapping("modules")
@Api(description = "Módulos de sistema",consumes = "application/json",produces = "application/json")
public class ModuleRest extends MexiledController{

    private final Logger logger = LoggerFactory.getLogger(ModuleRest.class);

    private ModuleService moduleService;

    @Autowired
    public void setModuleService(ModuleService moduleService) {
        this.moduleService = moduleService;
    }

    /**
     * Método para crear un nuevo modulo en base de datos
     * @param moduleView el modulo a insertar junto con los permisos
     * @throws ModuleException si ocurre algún error al guardar el módulo
     * */
    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void addModule(@RequestBody() @Validated() ModuleView moduleView) throws MexiledException{
        logger.info(ExceptionCode.MODULE+"- Agregar nuevo módulo: {}",moduleView);
        this.moduleService.addModule(moduleView);
    }
    /**
     * Método para editar un modulo en base de datos
     * @param moduleView el modulo a editar junto con los permisos
     * @throws ModuleException si ocurre algún error al editar el módulo
     * */
    @RequestMapping(method = RequestMethod.PUT,value = "{moduleId}")
    @ResponseStatus(HttpStatus.OK)
    public void editModule(@RequestBody() @Validated() ModuleView moduleView,@PathVariable()Long moduleId)throws MexiledException{
        logger.info(ExceptionCode.MODULE+"- Actualizar modulo  ID: {} - {}",moduleId,moduleView);
        moduleView.setModuleId(moduleId);
        this.moduleService.editModule(moduleView);
    }
    /**
     * Método para eliminar un módulo junto con sus permisos
     * @param moduleId identificador único de módulo
     * @throws ModuleException Si no es posible eliminar el módulo
     * */
    @RequestMapping(method = RequestMethod.DELETE,value = "{moduleId}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteModule(@PathVariable()Long moduleId) throws MexiledException{
        logger.info(ExceptionCode.MODULE+"- Eliminar módulo: {}",moduleId);
        this.moduleService.deleteModule(moduleId);
    }
    /**
     * Método para obtener los detalles de un módulo por id
     * @param moduleId identificador único de módulo
     * @throws ModuleException si no es posible seleccionar el módulo, o no existe
     * @return módulo encontrado en base de datos
     * */
    @RequestMapping(method = RequestMethod.GET,value = "{moduleId}")
    @ResponseStatus(HttpStatus.OK)
    public ModuleView findModule(@PathVariable() Long moduleId) throws MexiledException{
        logger.info(ExceptionCode.MODULE+"- Obtener módulo: {}",moduleId);
        return this.moduleService.findModule(moduleId);
    }
    /**
     * Método para obtener la lista de módulos en base de datos
     * @throws ModuleException si ocurre algún error al seleccionar la lista de modulos
     * @return lista de módulos en base de datos sin paginar
     * */
    @RequestMapping(method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public List<ModuleView> findAll() throws MexiledException{
        logger.info(ExceptionCode.MODULE+"- Obtener lista de módulos");
        return this.moduleService.findAll();
    }
}
