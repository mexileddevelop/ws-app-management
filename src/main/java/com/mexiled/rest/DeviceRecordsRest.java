package com.mexiled.rest;

import com.mexiled.common.ErrorMessage;
import com.mexiled.common.MexiledController;
import com.mexiled.common.MexiledException;
import com.mexiled.exception.RecordException;
import com.mexiled.service.DeviceRecordService;
import com.mexiled.views.GraphReportView;
import com.mexiled.views.MeassureReportView;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("records")
@Api(description = "Endpoint para guardar mediciones")
public class DeviceRecordsRest extends MexiledController{

    private final Logger logger = LoggerFactory.getLogger(DeviceRecordsRest.class);

    private DeviceRecordService recordService;

    @Autowired
    public void setRecordService(DeviceRecordService recordService) {
        this.recordService = recordService;
    }


    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public void addRecord(@RequestBody List<MeassureReportView> records) throws RecordException{
        try{
            logger.info("Guardar nueva medición: {}",records);
            recordService.addRecords(records);
        }catch (RecordException re){
            throw re;
        }catch (Exception ex){
            RecordException recordException = new RecordException("Error al insertar registro",RecordException.LAYER.CNT, MexiledException.ACTION.INS);
            logger.error("Error al ingresar medición",ex);
            throw recordException;
        }
    }

    /**
     * This method catch the {@link MexiledException} to make a custom response
     * @param ex the exception to be formated
     * @return {@link ErrorMessage} custom response with code and message
     * */
    @ExceptionHandler(value=RecordException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public void error(RecordException ex){
        logger.error("No fue posible ingresar la medicion");
    }

    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    public GraphReportView findGraph(@RequestParam(required = true)Long id,@RequestParam(defaultValue = "D") String type) throws RecordException{
        return recordService.findGraphDeviceByIdAndType(id,type);
    }

}
