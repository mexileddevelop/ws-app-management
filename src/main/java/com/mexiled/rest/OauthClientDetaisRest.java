package com.mexiled.rest;


import com.mexiled.common.MexiledController;
import com.mexiled.common.MexiledException;
import com.mexiled.exception.ExceptionCode;
import com.mexiled.service.OauthClientDetailsService;
import com.mexiled.views.OauthClientDetailsView;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;

@RestController()
@RequestMapping("tokens")
@Api(description = "Manejo de tokens OAUTH")
public class OauthClientDetaisRest extends MexiledController{
    
    private final Logger logger = LoggerFactory.getLogger(OauthClientDetaisRest.class);
    
    private OauthClientDetailsService service;

    @Autowired
    public void setService(OauthClientDetailsService service) {
        this.service = service;
    }

    /**
     * Método para agregar nueva autorización oauth a la base de datos
     * @param oauthClientDetails la nueva autorización que se va a guardar
     * @throws MexiledException si ocurre alguna excepción al guardar el objeto en base de datos
     * */
    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void addOauthClientDetail(@RequestBody @Validated OauthClientDetailsView oauthClientDetails) throws MexiledException{
        logger.info(ExceptionCode.OAUTH+"- Generar nuevo acceso - {}",oauthClientDetails);
        service.addOauthClientDetail(oauthClientDetails);
    }

    /**
     * Método para editar detalles de autorizacion oatuh de cliente
     * @param clientDetails objeto a editar en base de datos
     * @throws MexiledException
     * <br/> 1.- Si el objeto a editar no se encuentra en base de datos
     * <br/> 2.- Si ocurre algpun error al editar el objeto en base de datos
     * */
    @RequestMapping(method = RequestMethod.PUT,value = "{id}")
    @ResponseStatus(HttpStatus.OK)
    public void editOauthClientDetail(@RequestBody @Validated OauthClientDetailsView clientDetails,@PathVariable Long id) throws MexiledException{
        logger.info(ExceptionCode.OAUTH+"- Editar acceso - {} - {}",id,clientDetails);
        clientDetails.setId(id);
        service.editOauthClientDetail(clientDetails);
    }

    /**
     * Método para obtener detalles de autorización
     * @param id ID único del cual se obtendrn los detalles
     * @return Objeto con la información requerida
     * @throws MexiledException
     * <br/> 1.- Si el objeto a seleccionar no se encuentra en base de datos
     * <br/> 2.- Si ocurrre algún error al momento de hacer la selección en la base de datos
     * */
    @RequestMapping(method = RequestMethod.GET,value = "{id}")
    @ResponseStatus(HttpStatus.OK)
    public OauthClientDetailsView finOauthClientDetails(@PathVariable Long id) throws MexiledException{
        logger.info(ExceptionCode.OAUTH+"- Obtener acceso - {}",id);
        return service.finOauthClientDetails(id);
    }


    /**
     * Método para obtener lista de autorizaciones de forma paginada
     * @return lista de autorizaciones oauth
     * @throws MexiledException cuando no se puede obtener el resultado en la base de datos
     * */
    @RequestMapping(method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public List<OauthClientDetailsView> findAll() throws MexiledException{
        logger.info(ExceptionCode.OAUTH+"- Obtener lista de accesos");
        return service.findAll();
    }

    /**
     * Método para eliminar una autorización en la base de datos
     * @param id ID único del objeto que se va a eliminar de la base de datos
     * @throws MexiledException
     * <br/> 1.- Si no se encuentra la autorización en base de datos
     * <br/> 2.- Si ocurre algún errro al eliminar la autoptización
     * */
    @RequestMapping(method = RequestMethod.DELETE,value = "{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteClientDetail(@PathVariable Long id) throws MexiledException{
        logger.info(ExceptionCode.OAUTH+"- Eliminar autorización - {}",id);
        service.deleteClientDetail(id);
    }
}
