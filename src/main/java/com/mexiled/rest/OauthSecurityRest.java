package com.mexiled.rest;

import com.mexiled.common.MexiledController;
import com.mexiled.common.MexiledException;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.oauth2.provider.token.ConsumerTokenServices;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@Api(description = "Logout Endpoint")
public class OauthSecurityRest extends MexiledController{

    private final Logger logger = LoggerFactory.getLogger(OauthSecurityRest.class);

    private ConsumerTokenServices consumerTokenServices;

    @Autowired
    public void setConsumerTokenServices(ConsumerTokenServices consumerTokenServices) {
        this.consumerTokenServices = consumerTokenServices;
    }

    @RequestMapping(method= RequestMethod.POST,value="oauth/logout")
    @ResponseStatus(HttpStatus.OK)
    public void logout(HttpServletRequest request) throws MexiledException{
        String authHeader = request.getHeader("Authorization");
        logger.info("Remove token from session: "+authHeader);
        if (authHeader != null) {
            String tokenValue = authHeader.replace("Bearer", "").trim();
            consumerTokenServices.revokeToken(tokenValue);
        }
    }


}
