package com.mexiled.rest;

import com.mexiled.common.MexiledController;
import com.mexiled.common.MexiledException;
import com.mexiled.exception.ExceptionCode;
import com.mexiled.service.UserAppService;
import com.mexiled.views.UserAppView;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("users")
@Api(description = "Manejo de usuarios",produces = "application/json",consumes = "application/json",value = "Usuarios",basePath = "/users")
public class UserAppRest extends MexiledController{
    
    private final Logger logger = LoggerFactory.getLogger(UserAppRest.class);
    
    private UserAppService userAppService;

    @Autowired
    public void setUserAppService(UserAppService userAppService) {
        this.userAppService = userAppService;
    }

    /**
     * Método para agrgar un nuevo usuario al sistema
     * @param userAppView datos de usuario que se van a guardar
     * @throws MexiledException si ocurre algún error al guardar el usuario
     * */
    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public void addUserApp(@RequestBody @Validated UserAppView userAppView) throws MexiledException{
        logger.info(ExceptionCode.USER+"- Crear nuevo usuario - {}",userAppView);
        this.userAppService.addUserApp(userAppView);
    }

    /**
     * Método para editar los datos de un usuario, también edita la contraseña si esta no viene vacia
     * @param userAppView datos de usuario que se van a editar
     * @throws MexiledException si ocurre algún error al momento de editar el usuario
     * */
    @RequestMapping(method = RequestMethod.PUT,value = "{userId}")
    @ResponseStatus(HttpStatus.OK)
    public void updateUserApp(@RequestBody @Validated UserAppView userAppView,@PathVariable Long userId) throws MexiledException{
        logger.info(ExceptionCode.USER+"- Editar usuario - {} - {}",userId,userAppView);
        userAppView.setUserId(userId);
        this.userAppService.updateUserApp(userAppView);
    }

    /**
     * Método para eliminar un usuario de sistema
     * @param userId identificador de usuario que se va a eliminar
     * @throws MexiledException Si ocurre algún error al eliminar el usuario
     * */
    @RequestMapping(method = RequestMethod.DELETE,value = "{userId}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteUserApp(Long userId) throws MexiledException{
        logger.info(ExceptionCode.USER+"- Eliminar usuario- {}",userId);
        this.userAppService.deleteUserApp(userId);
    }

    /**
     * Método para cambiar el estado de un usuario, de activo a inactivo o viceversa
     * @param userId identificador único de usuario
     * @param status estado a establecer TRUE,FALSE
     * @throws MexiledException si no es posible cambiar el estado del usuario en base de datos
     * */
    @RequestMapping(method = RequestMethod.PUT,value = "{userId}/{status}")
    @ResponseStatus(HttpStatus.OK)
    public void changeStatus(@PathVariable Long userId,@PathVariable Boolean status) throws MexiledException{
        logger.info(ExceptionCode.USER+"- Actualizar estado de usuario - {} - {}",userId,status);
        this.userAppService.changeStatus(userId,status);
    }

    /**
     * Método para obtener los detalles de un usuario a través del ID
     * @param userId ID de usuario a buscar
     * @return Detalles de usuario obtenidos de base de datos
     * @throws MexiledException si no se encuentra el usuario o no se puede seleccionar de base de datos
     * */
    @RequestMapping(method = RequestMethod.GET,value = "findById/{userId}")
    @ResponseStatus(HttpStatus.OK)
    public UserAppView findById(@PathVariable Long userId) throws MexiledException{
        logger.info(ExceptionCode.USER+"- Obtener usuario por ID - {}",userId);
        return this.userAppService.findById(userId);
    }

    /**
     * Método para obtener los detalles de un usuario a traves del nombre de usuario
     * @param username nombre de usuario a buscar
     * @return Detalles de usuario obtenidos de base de datos
     * @throws MexiledException si no se encuentra en usuario o no se puede seleccioanr de base de datos
     * */
    @RequestMapping(method = RequestMethod.GET,value = "findByUsername/{username}")
    @ResponseStatus(HttpStatus.OK)
    public UserAppView findByUsername(String username) throws MexiledException{
        logger.info(ExceptionCode.USER+"- Obtener usuario - {}",username);
        return this.userAppService.findByUsername(username);
    }

    /**
     * Método para obtener ls lista completa de los usarios que existen en la base de datos
     * @return Lista de usuarios
     * @throws MexiledException si no es posible realizar la selección de los usuarios
     * */
    @RequestMapping(method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public List<UserAppView> findAll() throws MexiledException{
        logger.info(ExceptionCode.USER+"- Obtener lista de usuarios");
        return this.userAppService.findAll();
    }
}
