package com.mexiled.rest;

import com.mexiled.common.MexiledController;
import com.mexiled.common.MexiledException;
import com.mexiled.exception.ExceptionCode;
import com.mexiled.service.SectionService;
import com.mexiled.views.SectionView;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("sections")
@Api(description = "Manejo de secciones")
public class SectionRest extends MexiledController{

    private final Logger logger = LoggerFactory.getLogger(SectionRest.class);

    private SectionService sectionService;

    @Autowired
    public void setSectionService(SectionService sectionService) {
        this.sectionService = sectionService;
    }

    /**
     * Método para agregar una nueva sección en la base de datos
     * @param sectionView vista con los datos de la seccióna agregar
     * @throws MexiledException si ocurre un error al guardar la seccion
     * */
    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void addSection(@RequestBody @Validated SectionView sectionView) throws MexiledException{
        logger.info(ExceptionCode.SECTION+"- Agregar nueva sección - {}",sectionView);
        sectionService.addSection(sectionView);
    }

    /**
     * Método para editar los datos de una sección en base de datos
     * @param sectionView datos de la sección a editar
     * @param sectionId Identificador único de sección
     * @throws MexiledException si no se encuentra la sección u ocurre un error al editar
     * */
    @RequestMapping(method = RequestMethod.PUT,value = "{sectionId}")
    @ResponseStatus(HttpStatus.OK)
    public void updateSection(@RequestBody @Validated SectionView sectionView,@PathVariable Long sectionId) throws MexiledException{
        logger.info(ExceptionCode.SECTION+"- Editar sección - {} -ID: {}",sectionView,sectionId);
        sectionView.setSectionId(sectionId);
        sectionService.updateSection(sectionView);
    }

    /**
     * Método para eliminar una sección de base de datos
     * @param sectionId identificador único de la sección
     * @throws MexiledException si ocurre un error al eliminar la sección o no es posible eliminarla
     * */
    @RequestMapping(method = RequestMethod.DELETE,value = "{sectionId}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteSection(@PathVariable Long sectionId) throws MexiledException{
        logger.info(ExceptionCode.SECTION+"- Eliminar sección de base de datos {}",sectionId);
        sectionService.deleteSection(sectionId);
    }

    /**
     * Método para obtener lops detalles de una sección
     * @param sectionId identificador único de seccion para seleccionar en base de datos
     * @return detalles de la seccion {@link SectionView}
     * @throws MexiledException si ocurre un error al momento de realizar la query
     * */
    @RequestMapping(method = RequestMethod.GET,value = "{sectionId}")
    @ResponseStatus(HttpStatus.OK)
    public SectionView findSection(@PathVariable Long sectionId) throws MexiledException{
        logger.info(ExceptionCode.SECTION+"- Obtener detalles de seccion: {}",sectionId);
        return sectionService.findSection(sectionId);
    }

    /**
     * Método para obtener la lista de secciones, con opciones de filtrado por proyectos y/o activos
     * @param projectId identificador de proyecto para obtener secciones - opcional
     * @param active opcion para filtrar secciones
     * @return Lista de secciones de tipo {@link SectionView}
     * @throws MexiledException si ocurre un error al realizar la seleccion de las secciones
     * */
    @RequestMapping(method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public List<SectionView> findSections(@RequestParam(required = false) Long projectId,@RequestParam(required = false) Boolean active) throws MexiledException{
        logger.info(ExceptionCode.SECTION+"- Obtener lista de perfiles - projectId: {} - active: {}",projectId,active);
        return sectionService.findSections(projectId,active);
    }
}
