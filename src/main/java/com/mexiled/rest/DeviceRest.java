package com.mexiled.rest;


import com.mexiled.common.MexiledController;
import com.mexiled.converter.DeviceException;
import com.mexiled.exception.ExceptionCode;
import com.mexiled.service.DeviceService;
import com.mexiled.views.DeviceReportView;
import com.mexiled.views.DeviceUpdateView;
import com.mexiled.views.DeviceView;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("devices")
@Api(description = "Manejo de circuitos")
public class DeviceRest extends MexiledController {

    public final Logger logger = LoggerFactory.getLogger(DeviceRest.class);

    private DeviceService deviceService;

    @Autowired
    public void setDeviceService(DeviceService deviceService) {
        this.deviceService = deviceService;
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void addDevice(@RequestBody @Validated DeviceView view) throws DeviceException {
        logger.info("agregar circuito : {}", view);
        deviceService.addDevice(view);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "{deviceId}")
    @ResponseStatus(HttpStatus.OK)
    public void updateDevice(@RequestBody @Validated DeviceView view, @PathVariable Long deviceId) throws DeviceException {
        logger.info("agregar circuito : {} - ID: {}", view, deviceId);
        view.setDeviceId(deviceId);
        deviceService.updateDevice(view);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "{deviceId}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteDevice(@PathVariable Long deviceId) throws DeviceException {
        logger.info("Eliminar circuito: {}", deviceId);
        deviceService.deleteDevice(deviceId);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "updateByProjectSectionOrPolygon")
    @ResponseStatus(HttpStatus.CREATED)
    public void updateByProjectSectionOrPoylgonId(@RequestBody @Validated DeviceUpdateView updateView) throws DeviceException {
        logger.info("Actualizar circuitos: {}", updateView);
        deviceService.updateByProjectSectionOrPoylgonId(updateView);
    }

    @RequestMapping(method = RequestMethod.GET, value = "{deviceId}")
    @ResponseStatus(HttpStatus.OK)
    public DeviceView findDevice(@PathVariable() Long deviceId) throws DeviceException {
        logger.info("Obtener detalles de circuito: {}", deviceId);
        return deviceService.findDevice(deviceId);
    }

    @RequestMapping(method = RequestMethod.GET, value = "findAddress")
    @ResponseStatus(HttpStatus.CREATED)
    public Map<String, Object> findIdByMacAddress(@RequestParam String macAddress) {
        try {
            return deviceService.findIdByMacAddress(macAddress);
        } catch (Exception ex) {
            Map<String, Object> map = new HashMap<>();
            map.put("ID", 0);
            map.put("message", "Error al buscar ID");
            return map;
        }

    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public Collection<DeviceReportView> findReport(@RequestParam(required = false)Long projectId,
                                                   @RequestParam(required=false)Long sectionId,
                                                   @RequestParam(required = false)Long polygonId) throws DeviceException{
        return deviceService.findDevices(projectId,sectionId,polygonId);
    }
}
