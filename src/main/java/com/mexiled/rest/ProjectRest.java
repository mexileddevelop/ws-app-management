package com.mexiled.rest;

import com.mexiled.common.MexiledController;
import com.mexiled.common.MexiledException;
import com.mexiled.exception.ExceptionCode;
import com.mexiled.exception.ProjectException;
import com.mexiled.service.ProjectService;
import com.mexiled.views.ProjectView;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("projects")
@Api(description = "Manejo de proyectos")
public class ProjectRest extends MexiledController{

    private final Logger logger = LoggerFactory.getLogger(ProjectRest.class);

    private ProjectService projectService;

    @Autowired
    public void setProjectService(ProjectService projectService) {
        this.projectService = projectService;
    }

    /**
     * Método para agregar nuevos proyectos
     * @param view datos de projecto a guardar
     * @throws MexiledException si ocurre algún error al guardar el proyecto<br/>
     * No pueden existir dos proyectos con el mismo código
     * */
    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void addProject(@RequestBody @Validated ProjectView view) throws MexiledException{
        logger.info(ExceptionCode.PROJECT+"- Agregar nuevo proyecto - {} ",view);
        projectService.addProject(view);
    }

    /**
     * Método para editar datos de un proyecto
     * @param view datos de projecto a editar
     * @param projectId ID de proyecto a editar
     * @throws MexiledException si ocurre algún error al editar el proyecto<br/>
     * No pueden existir dos proyectos con el mismo código
     * */
    @RequestMapping(method = RequestMethod.PUT,value = "{projectId}")
    @ResponseStatus(HttpStatus.OK)
    public void updateProject(@RequestBody @Validated ProjectView view,@PathVariable Long projectId) throws MexiledException{
        logger.info(ExceptionCode.PROJECT+"- Editar proyecto - {}  - {}",view,projectId);
        view.setProjectId(projectId);
        try {
            projectService.updateProject(view);
        }catch (DataIntegrityViolationException da){
            ProjectException projectException = new ProjectException("Error al editar proyecto", MexiledException.LAYER.DAO, MexiledException.ACTION.UPD);
            projectException.addError("Ya existe un proyeto con el código: "+view.getCode());
            logger.error(ExceptionCode.PROJECT+"- Error al editar proyecto - {} - CODE: {}",view,projectException.getExceptionCode(),da);
            throw projectException;
        }
    }

    /**
     * Método para eliminar un proyecto de la base de datos
     * @param projectId ID de proyecto a eliminar
     * @throws MexiledException Si ocurre un error al eliminar el proyecto o se tiene referencias en algúna tabla
     * */
    @RequestMapping(method = RequestMethod.DELETE,value = "{projectId}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteProject(@PathVariable Long projectId) throws MexiledException{
        logger.info(ExceptionCode.PROJECT+"- Eliminar proyecto - {} ",projectId);
        projectService.deleteProject(projectId);
    }

    /**
     * Método para obtener los detalles de un proyecto
     * @param projectId Identificador único de proyecto
     * @throws MexiledException si ocurre un error al seleccionar el proyecto en la base de datos
     * @return detalles de proyecto
     * */
    @RequestMapping(method = RequestMethod.GET,value = "{projectId}")
    @ResponseStatus(HttpStatus.OK)
    public ProjectView findProject(@PathVariable Long projectId) throws MexiledException{
        logger.info(ExceptionCode.PROJECT+"- Obtener detalles de proyecto: {}",projectId);
        return projectService.findProject(projectId);
    }

    /**
     * Método para obtener la lista de proyectos
     * @param active - opcional para filtrado por activos o inactivos
     * @throws MexiledException si ocurre un error al seleccionar la lista de proyectos
     * @return lista de proyectos seleccionados sin paginar
     * */
    @RequestMapping(method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public List<ProjectView> findProjects(@RequestParam(required = false) Boolean active) throws MexiledException{
        logger.info(ExceptionCode.PROJECT+"- Obtener lista de proyectos - {}",active);
        return projectService.findProjects(active);
    }
}
