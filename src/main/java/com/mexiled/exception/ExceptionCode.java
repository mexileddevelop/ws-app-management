package com.mexiled.exception;
/**
 * @author Victor de la Cruz
 * @version 1.0.0
 */
public enum ExceptionCode {
    MODULE("MOD"),PROFILE("PRO"),
    PERMISSION("PERM"),USER("USR"),
    RECORD("REC"),
    OAUTH("AUTH"),PROJECT("PROJ"),SECTION("SEC"),POLYGON("POLY"),DEVICE("DEV");

    private final String code;
    ExceptionCode(String code){
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
