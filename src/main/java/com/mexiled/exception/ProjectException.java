package com.mexiled.exception;

import com.mexiled.common.MexiledException;


/**
 * @author Victor de la Cruz
 * @version 1.0.0
 */
public class ProjectException extends MexiledException {

    private final ExceptionCode MODULE_CODE = ExceptionCode.PROFILE;

    private String layer;

    private String action;

    public ProjectException(Exception ex,String message,String layer,String action){
        super(ex,message);
        this.layer = layer;
        this.action = action;
    }

    public ProjectException(String message,LAYER layer,ACTION action){
        super(message);
        this.layer = layer.name();
        this.action = action.name();
    }

    public String getLayer() {return layer;}

    public void setLayer(String layer) {this.layer = layer;}

    public String getAction() {return action;}

    public void setAction(String action) {this.action = action;}

    @Override
    public String getExceptionCode() {
        return new StringBuffer(layer).append(MODULE_CODE).append(action).toString();
    }
}
