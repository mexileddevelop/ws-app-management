package com.mexiled.exception;

import com.mexiled.common.MexiledException;


/**
 * @author Victor de la Cruz
 * @version 1.0.0
 */
public class PolygonException extends MexiledException {

    public final ExceptionCode MODULE_CODE = ExceptionCode.USER;

    private String layer;

    private String action;

    public PolygonException(Exception ex, String message, MexiledException.LAYER layer, MexiledException.ACTION action){
        super(ex,message);
        this.layer = layer.name();
        this.action = action.name();
    }

    public PolygonException(String message, MexiledException.LAYER layer, MexiledException.ACTION action){
        super(message);
        this.layer = layer.name();
        this.action = action.name();
    }

    public String getLayer() {return layer;}

    public void setLayer(String layer) {this.layer = layer;}

    public String getAction() {return action;}

    public void setAction(String action) {this.action = action;}

    @Override
    public String getExceptionCode() {
        return layer + MODULE_CODE + action;
    }
}
