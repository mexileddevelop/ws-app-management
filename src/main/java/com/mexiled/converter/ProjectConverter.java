package com.mexiled.converter;

import com.mexiled.domain.Project;
import com.mexiled.views.ProjectView;
import org.springframework.stereotype.Component;

@Component
public class ProjectConverter {

    public Project fromView(ProjectView view){
        Project project = new Project();
        project.setActive(view.getActive());
        project.setCode(view.getCode());
        project.setCreatedDate(view.getCreatedDate());
        project.setName(view.getName());
        project.setProjectId(view.getProjectId());
        return project;
    }

    public ProjectView fromEntity(Project project){
        ProjectView view = new ProjectView();
        view.setActive(project.getActive());
        view.setCode(project.getCode());
        view.setCreatedDate(project.getCreatedDate());
        view.setName(project.getName());
        view.setProjectId(project.getProjectId());
        return view;
    }
}
