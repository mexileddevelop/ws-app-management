package com.mexiled.converter;

import com.mexiled.common.MexiledException;
import com.mexiled.exception.ExceptionCode;


public class DeviceException extends MexiledException{

    private final ExceptionCode MODULE_CODE = ExceptionCode.DEVICE;

    private String layer;

    private String action;

    public DeviceException(Exception ex,String message,String layer,String action){
        super(ex,message);
        this.layer = layer;
        this.action = action;
    }

    public DeviceException(String message,LAYER layer,ACTION action){
        super(message);
        this.layer = layer.name();
        this.action = action.name();
    }

    public String getLayer() {return layer;}

    public void setLayer(String layer) {this.layer = layer;}

    public String getAction() {return action;}

    public void setAction(String action) {this.action = action;}

    @Override
    public String getExceptionCode() {
        return new StringBuffer(layer).append(MODULE_CODE).append(action).toString();
    }
}
