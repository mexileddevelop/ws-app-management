package com.mexiled.converter;

import com.mexiled.domain.Module;
import com.mexiled.domain.ModulePermission;
import com.mexiled.views.ModulePermissionView;
import com.mexiled.views.ModuleView;
import org.springframework.stereotype.Component;

import java.util.List;

@Component()
public class ModuleConverter {

    /**
     * Método para convertir una vista a su equivalente Entity para ser guardado o editado en la base de datos
     * @param moduleView la vista a ser convertida
     * @param update bandera pra saber si se esta convirtiendo desde un método para guardar un nuevo objeto o para editarlo<br/>
     * necesario para agregar la fecha de creación del objeto
     * @return Nueva entidad obtenida desde la vista
     * */
    public Module convertViewToEntity(ModuleView moduleView, Boolean update){
        Module module = new Module();
        module.setModuleId(moduleView.getModuleId());
        module.setName(moduleView.getName());

        if(!moduleView.getPermissions().isEmpty()){
            for(ModulePermissionView p: moduleView.getPermissions()) {
                if(p!=null) {
                    ModulePermission permission = new ModulePermission();
                    permission.setModule(module);
                    permission.setCode(p.getCode());
                    permission.setModulePermissionId(p.getModulePermissionId());
                    permission.setName(p.getName());
                    module.getModulePermissions().add(permission);
                }
            }
        }

        return module;
    }

    /**
     * Método para convertir una entidad a una vista
     * @param module  la entidad a ser convertida
     * @param shortConvert bandera boleana para saber el tipo de conversión que se esta realizando
     * <br/>Cuando el parametro <strong>shortConvert</strong> es verdadero, se incluye la lista de permisos del módulo
     * @return la vista obtenida de la conversión con o sin lista de permisos dependiendo de <strong>shortConvert</strong>
     * **/
    public ModuleView convertEntityToView(Module module,Boolean shortConvert){
        ModuleView moduleView = new ModuleView();
        moduleView.setName(module.getName());
        moduleView.setModuleId(module.getModuleId());

        if(!shortConvert){
            //agregar permisos del módulo
            for(ModulePermission p : module.getModulePermissions()){
                ModulePermissionView permissionView = new ModulePermissionView();
                permissionView.setName(p.getName());
                permissionView.setCode(p.getCode());
                permissionView.setModulePermissionId(p.getModulePermissionId());
                moduleView.getPermissions().add(permissionView);
            }
        }

        return moduleView;
    }

    /**
     * Método para obtener el indice en el que se encuentra un ID en la lista de permisos
     * @param permissionId ID único a buscar
     * @param permissions  lista de permisos
     * @return el indice encontrado en el arreglo, si el valor es igual a <strong>-1</strong>, significa que no se encontro el elemento
     * **/
    public Integer indexOfPermission(Long permissionId,List<ModulePermission> permissions){
        for(int i=0;i<permissions.size();i++){
            if(permissions.get(i).getModulePermissionId().compareTo(permissionId)==0){
                return i;
            }
        }
        return -1;
    }
}
