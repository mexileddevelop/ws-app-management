package com.mexiled.converter;

import com.mexiled.domain.Project;
import com.mexiled.domain.Section;
import com.mexiled.views.SectionView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SectionConverter {

    private ProjectConverter projectConverter;

    @Autowired
    public void setProjectConverter(ProjectConverter projectConverter) {
        this.projectConverter = projectConverter;
    }

    public Section fromView(SectionView view){
        Section section = new Section();
        section.setActive(view.getActive());
        section.setCode(view.getCode());
        section.setCreatedDate(view.getCreatedDate());
        section.setName(view.getName());
        section.setSectionId(view.getSectionId());
        Project project = new Project();
        project.setProjectId(view.getProject().getProjectId());
        section.setProject(project);
        return section;
    }

    public SectionView fromEntity(Section section){
        SectionView view = new SectionView();
        view.setActive(section.getActive());
        view.setCode(section.getCode());
        view.setCreatedDate(section.getCreatedDate());
        view.setName(section.getName());
        if(section.getProject()!=null)
            view.setProject(projectConverter.fromEntity(section.getProject()));
        view.setSectionId(section.getSectionId());
        return view;
    }
}
