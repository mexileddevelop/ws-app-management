package com.mexiled.converter;

import com.mexiled.common.DeviceEstatusConfig;
import com.mexiled.common.TimeUtils;
import com.mexiled.domain.Device;
import com.mexiled.repository.DeviceRecordRepository;
import com.mexiled.views.DeviceReportView;
import com.mexiled.views.DeviceView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

@Component
public class DeviceConverter {

    private ProjectConverter projectConverter;

    private SectionConverter sectionConverter;

    private PolygonConverter polygonConverter;

    private TimeUtils timeUtils;

    private final Logger logger = LoggerFactory.getLogger(DeviceConverter.class);

    @Autowired
    public void setPolygonConverter(PolygonConverter polygonConverter) {
        this.polygonConverter = polygonConverter;
    }

    @Autowired
    public void setSectionConverter(SectionConverter sectionConverter) {
        this.sectionConverter = sectionConverter;
    }

    @Autowired
    public void setTimeUtils(TimeUtils timeUtils) {
        this.timeUtils = timeUtils;
    }

    @Autowired
    public void setProjectConverter(ProjectConverter projectConverter) {
        this.projectConverter = projectConverter;
    }



    public Device fromView(DeviceView view) throws ParseException {
        Device device = new Device();
        device.setCode(view.getCode());
        device.setInstallationStatus(view.getInstallationStatus());
        device.setCreatedDate(view.getCreatedDate());
        device.setDeviceId(view.getDeviceId());
        device.setLamps(view.getLamps());
        device.setLatitude(view.getLatitude());
        device.setLongitude(view.getLongitude());
        device.setMacAddress(view.getMacAddress());
        device.setTimeBeforeNotify(view.getTimeBeforeNotify());
        device.setTimeToMarkAsDisconnected(view.getTimeToMarkAsDisconnected());
        //horas iniciales y finales cuando se prende el dispositivo
        if(view.getLigthOffHour()!=null && !view.getLigthOffHour().isEmpty())
            device.setLigthOffHour(timeUtils.parseHour(view.getLigthOffHour(),timeUtils.HOUR_FORMAT));
        if(view.getLigthOnHour()!=null && !view.getLigthOnHour().isEmpty())
            device.setLigthOnHour(timeUtils.parseHour(view.getLigthOnHour(),timeUtils.HOUR_FORMAT));
        device.setControlNumber(view.getControlNumber());
        return device;
    }

    public DeviceView fromEntity(Device device) throws ParseException{
        DeviceView view = new DeviceView();
        view.setCode(device.getCode());
        view.setCreatedDate(device.getCreatedDate());
        view.setDeviceId(device.getDeviceId());
        view.setInstallationStatus(device.getInstallationStatus());
        view.setLamps(device.getLamps());
        view.setLatitude(device.getLatitude());
        view.setLongitude(device.getLongitude());
        if(device.getLigthOffHour()!=null)
            view.setLigthOffHour(timeUtils.formatHour(device.getLigthOffHour(),timeUtils.HOUR_FORMAT));
        if(device.getLigthOnHour()!=null)
            view.setLigthOnHour(timeUtils.formatHour(device.getLigthOnHour(),timeUtils.HOUR_FORMAT));
        if(device.getInstallationStatus().equals(DeviceEstatusConfig.INSTALADO)){
            if(device.getDeviceHistory()!=null && device.getDeviceHistory().getHistoryStatus()!=null && !device.getDeviceHistory().getHistoryStatus().isEmpty()) {
                //si el último estatus es instalado o apagado, verificar cuanto tiempo tiene sin conexión, no debe superar la tolerancia marcada
                view.setInstallationStatus(getStatus(device));
            }
        }
        view.setMacAddress(device.getMacAddress());
        view.setPolygonView(polygonConverter.fromEntity(device.getPolygon()));
        view.setProject(projectConverter.fromEntity(device.getProject()));
        view.setSection(sectionConverter.fromEntity(device.getSection()));
        view.setTimeBeforeNotify(device.getTimeBeforeNotify());
        view.setTimeToMarkAsDisconnected(device.getTimeToMarkAsDisconnected());
        view.setControlNumber(device.getControlNumber());
        return view;
    }

    public DeviceReportView reportFromEntity(Device device) throws ParseException{
        DeviceReportView view = new DeviceReportView();
        view.setCode(device.getCode());
        view.setCreatedDate(device.getCreatedDate());
        view.setDeviceId(device.getDeviceId());
        view.setInstallationStatus(device.getInstallationStatus());
        view.setLamps(device.getLamps());
        view.setLatitude(device.getLatitude());
        view.setLongitude(device.getLongitude());
        if(device.getLigthOffHour()!=null)
            view.setLigthOffHour(timeUtils.formatHour(device.getLigthOffHour(),timeUtils.HOUR_FORMAT));
        if(device.getLigthOnHour()!=null)
            view.setLigthOnHour(timeUtils.formatHour(device.getLigthOnHour(),timeUtils.HOUR_FORMAT));
        view.setMacAddress(device.getMacAddress());
        view.setPolygon(polygonConverter.fromEntity(device.getPolygon()));
        view.setTimeBeforeNotify(device.getTimeBeforeNotify());
        view.setTimeToMarkAsDisconnected(device.getTimeToMarkAsDisconnected());
        view.setControlNumber(device.getControlNumber());
        if(device.getInstallationStatus().equals(DeviceEstatusConfig.INSTALADO)){
            if(device.getDeviceHistory()!=null && device.getDeviceHistory().getHistoryStatus()!=null && !device.getDeviceHistory().getHistoryStatus().isEmpty()) {
                //si el último estatus es instalado o apagado, verificar cuanto tiempo tiene sin conexión, no debe superar la tolerancia marcada
                view.setInstallationStatus(getStatus(device));
            }
        }
        if(device.getDeviceHistory()!=null)
            view.setLastActivity(device.getDeviceHistory().getLastActivity());//las meassurement
        return view;
    }

    private String getStatus(Device device) throws ParseException{
        if(device.getInstallationStatus().equals(DeviceEstatusConfig.INSTALADO)){
            if(device.getDeviceHistory()!=null && device.getDeviceHistory().getHistoryStatus()!=null && !device.getDeviceHistory().getHistoryStatus().isEmpty()) {
                //si se tienen mediciones
                String status = device.getDeviceHistory().getHistoryStatus();
                //si el último estatus es instalado o apagado, verificar cuanto tiempo tiene sin conexión, no debe superar la tolerancia marcada
                if((status.equals(DeviceEstatusConfig.ON) || status.equals(DeviceEstatusConfig.OFF) || status.equals(DeviceEstatusConfig.DISCONNECTED))){
                    //ZoneId z = ZoneId.of("America/Mexico_City");

                    ZonedDateTime zdt = ZonedDateTime.now();
                    LocalDateTime currentTime = zdt.toLocalDateTime();
                    LocalDateTime last = LocalDateTime.parse(
                            timeUtils.formatHour(device.getDeviceHistory().getLastActivity(), timeUtils.DATE_FORMAT),
                            DateTimeFormatter.ofPattern(timeUtils.DATE_FORMAT)
                    );
                    long minutes = last.until(currentTime, ChronoUnit.MINUTES);
                    logger.info("Minutes since last record: "+minutes);
                    if(minutes>=device.getTimeToMarkAsDisconnected()) {
                        device.setDeviceStatus(DeviceEstatusConfig.DISCONNECTED);
                        status = device.getDeviceStatus();
                    }
                }
                return status;
            }
        }
        return "";
    }
}
