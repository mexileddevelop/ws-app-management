package com.mexiled.converter;

import com.mexiled.domain.OauthClientDetails;
import com.mexiled.views.OauthClientDetailsView;
import org.springframework.stereotype.Component;


/**
 * @author Victor de la Cruz
 * @version 1.0.0
 * **/
@Component
public class OauthClientDetailsConverter {

    public OauthClientDetails fromView(OauthClientDetailsView view){
        OauthClientDetails details = new OauthClientDetails();
        details.setAccessTokenValidity(view.getAccessTokenValidity());
        if(!view.getAdditionaInformation().equals(""))
            details.setAdditionaInformation(view.getAdditionaInformation());
        details.setAuthorities(view.getAuthorities());
        details.setAuthorizedGrantTypes(view.getAuthorizedGrantTypes());
        details.setAutoaprove(view.getAutoaprove());
        details.setClientId(view.getClientId());
        details.setId(view.getId());
        details.setClientSecret(view.getClientSecret());
        details.setRefreshTokenValidity(view.getRefreshTokenValidity());
        details.setResourceIds(view.getResourceIds());
        details.setScope(view.getScope());
        details.setWebServerRedirectUri(view.getWebServerRedirectUri());
        return details;
    }

    public OauthClientDetailsView fromEntity(OauthClientDetails clientDetails){
        OauthClientDetailsView view = new OauthClientDetailsView();
        view.setAccessTokenValidity(clientDetails.getAccessTokenValidity());
        if(!clientDetails.getAdditionaInformation().equals(""))
            view.setAdditionaInformation(clientDetails.getAdditionaInformation());
        view.setAuthorities(clientDetails.getAuthorities());
        view.setAuthorizedGrantTypes(clientDetails.getAuthorizedGrantTypes());
        view.setAutoaprove(clientDetails.getAutoaprove());
        view.setClientId(clientDetails.getClientId());
        view.setId(clientDetails.getId());
        view.setClientSecret(clientDetails.getClientSecret());
        view.setRefreshTokenValidity(clientDetails.getRefreshTokenValidity());
        view.setResourceIds(clientDetails.getResourceIds());
        view.setScope(clientDetails.getScope());
        view.setWebServerRedirectUri(clientDetails.getWebServerRedirectUri());
        return view;
    }
}
