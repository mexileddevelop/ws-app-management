package com.mexiled.converter;

import com.mexiled.domain.Profile;
import com.mexiled.domain.UserApp;
import com.mexiled.views.UserAppView;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class UserAppConverter {

    public UserApp fromView(UserAppView view){
        UserApp userApp = new UserApp();
        userApp.setActive(view.getActive());
        userApp.setCreatedDate(new Date());
        userApp.setEmail(view.getEmail());
        userApp.setName(view.getName());
        userApp.setUsername(view.getUsername());
        userApp.setPassword(view.getPassword());
        Profile p = new Profile();
        p.setProfileId(view.getProfileId());
        userApp.setProfile(p);
        userApp.setUserId(view.getUserId());
        return userApp;
    }

    public UserAppView fromEntity(UserApp userApp){
        UserAppView view = new UserAppView();
        view.setActive(userApp.getActive());
        view.setCreatedDate(userApp.getCreatedDate());
        view.setEmail(userApp.getEmail());
        view.setName(userApp.getName());
        view.setUsername(userApp.getUsername());
        view.setPassword("");
        view.setProfileId(userApp.getProfile().getProfileId());
        view.setProfileName(userApp.getProfile().getProfileName());
        view.setUserId(userApp.getUserId());
        return view;
    }
}
