package com.mexiled.converter;


import com.mexiled.domain.Polygon;
import com.mexiled.views.PolygonView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * @author Victor de la Cruz
 * @version 1.0.0
 */
@Component
public class PolygonConverter {

    private SectionConverter sectionConverter;

    @Autowired
    public void setSectionConverter(SectionConverter sectionConverter) {
        this.sectionConverter = sectionConverter;
    }

    public Polygon fromView(PolygonView view){
        Polygon polygon = new Polygon();
        polygon.setActive(view.getActive());
        polygon.setCode(view.getCode());
        polygon.setCreatedDate(view.getCreatedDate());
        polygon.setName(view.getName());
        polygon.setPolygonId(view.getPolygonId());
        return polygon;
    }

    public PolygonView fromEntity(Polygon polygon){
        PolygonView view = new PolygonView();
        view.setActive(polygon.getActive());
        view.setCode(polygon.getCode());
        view.setCreatedDate(polygon.getCreatedDate());
        view.setName(polygon.getName());
        view.setPolygonId(polygon.getPolygonId());
        view.setSection(sectionConverter.fromEntity(polygon.getSection()));
        return view;
    }
}
