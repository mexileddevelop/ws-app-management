package com.mexiled.views;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Victor de la Cruz
 * @version 1.0.0
 * **/
public class NodeTree implements Serializable{
    private String content;
    private Long id;
    private Boolean active;
    private Boolean module;
    private Boolean expanded = Boolean.TRUE;
    private List<NodeTree> children = new ArrayList<>();
    @JsonIgnore
    private Integer childCount = 0;
    @JsonIgnore
    private Integer childSelected = 0;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean getModule() {
        return module;
    }

    public void setModule(Boolean module) {
        this.module = module;
    }

    public Boolean getExpanded() {
        return expanded;
    }

    public void setExpanded(Boolean expanded) {
        this.expanded = expanded;
    }

    public List<NodeTree> getChildren() {
        return children;
    }

    public void setChildren(List<NodeTree> children) {
        this.children = children;
    }

    public Integer getChildCount() {
        return childCount;
    }

    public void setChildCount(Integer childCount) {
        this.childCount = childCount;
    }

    public Integer getChildSelected() {
        return childSelected;
    }

    public void setChildSelected(Integer childSelected) {
        this.childSelected = childSelected;
    }

    @Override
    public String toString() {
        return "NodeTree{" +
                "content='" + content + '\'' +
                ", id=" + id +
                ", active=" + active +
                ", module=" + module +
                ", expended=" + expanded +
                ", children=" + children +
                ", childCount=" + childCount +
                ", childSelected=" + childSelected +
                '}';
    }
}
