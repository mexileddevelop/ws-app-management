package com.mexiled.views;

import java.io.Serializable;
import java.util.Date;

public class MeassureGraphView implements Serializable {
    private Date createdDate;
    private MeassureFase fase1;
    private MeassureFase fase2;

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public MeassureFase getFase1() {
        return fase1;
    }

    public void setFase1(MeassureFase fase1) {
        this.fase1 = fase1;
    }

    public MeassureFase getFase2() {
        return fase2;
    }

    public void setFase2(MeassureFase fase2) {
        this.fase2 = fase2;
    }

    @Override
    public String toString() {
        return "MeassureGraphView{" +
                "createdDate=" + createdDate +
                ", fase1=" + fase1 +
                ", fase2=" + fase2 +
                '}';
    }
}
