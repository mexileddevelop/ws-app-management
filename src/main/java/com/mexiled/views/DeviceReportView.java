package com.mexiled.views;

import java.io.Serializable;
import java.util.Date;

public class DeviceReportView implements Serializable{
    private Long deviceId;
    private String macAddress;
    private String code;
    private Double latitude;
    private Double longitude;
    private Date createdDate = new Date();
    private String installationStatus;//TODO view catalog
    private Integer timeBeforeNotify;
    private Integer timeToMarkAsDisconnected;
    private Integer lamps;
    private String ligthOnHour;
    private String ligthOffHour;
    private String controlNumber;
    private Date lastActivity;
    private PolygonView polygon;

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getInstallationStatus() {
        return installationStatus;
    }

    public void setInstallationStatus(String installationStatus) {
        this.installationStatus = installationStatus;
    }

    public Integer getTimeBeforeNotify() {
        return timeBeforeNotify;
    }

    public void setTimeBeforeNotify(Integer timeBeforeNotify) {
        this.timeBeforeNotify = timeBeforeNotify;
    }

    public Integer getTimeToMarkAsDisconnected() {
        return timeToMarkAsDisconnected;
    }

    public void setTimeToMarkAsDisconnected(Integer timeToMarkAsDisconnected) {
        this.timeToMarkAsDisconnected = timeToMarkAsDisconnected;
    }

    public Integer getLamps() {
        return lamps;
    }

    public void setLamps(Integer lamps) {
        this.lamps = lamps;
    }



    public String getLigthOnHour() {
        return ligthOnHour;
    }

    public void setLigthOnHour(String ligthOnHour) {
        this.ligthOnHour = ligthOnHour;
    }

    public String getLigthOffHour() {
        return ligthOffHour;
    }

    public void setLigthOffHour(String ligthOffHour) {
        this.ligthOffHour = ligthOffHour;
    }

    public String getControlNumber() {
        return controlNumber;
    }

    public void setControlNumber(String controlNumber) {
        this.controlNumber = controlNumber;
    }

    public Date getLastActivity() {
        return lastActivity;
    }

    public void setLastActivity(Date lastActivity) {
        this.lastActivity = lastActivity;
    }

    public PolygonView getPolygon() {
        return polygon;
    }

    public void setPolygon(PolygonView polygon) {
        this.polygon = polygon;
    }
}
