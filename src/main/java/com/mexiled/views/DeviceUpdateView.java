package com.mexiled.views;

import java.io.Serializable;

public class DeviceUpdateView implements Serializable{

    private Integer timeBeforeNotify;
    private Integer timeToMarkAsDisconnected;
    private String ligthOnHour;
    private String ligthOffHour;
    private Long projectId;
    private Long sectionId;
    private Long polygonId;
    private Boolean active;

    public Integer getTimeBeforeNotify() {
        return timeBeforeNotify;
    }

    public void setTimeBeforeNotify(Integer timeBeforeNotify) {
        this.timeBeforeNotify = timeBeforeNotify;
    }

    public Integer getTimeToMarkAsDisconnected() {
        return timeToMarkAsDisconnected;
    }

    public void setTimeToMarkAsDisconnected(Integer timeToMarkAsDisconnected) {
        this.timeToMarkAsDisconnected = timeToMarkAsDisconnected;
    }

    public String getLigthOnHour() {
        return ligthOnHour;
    }

    public void setLigthOnHour(String ligthOnHour) {
        this.ligthOnHour = ligthOnHour;
    }

    public String getLigthOffHour() {
        return ligthOffHour;
    }

    public void setLigthOffHour(String ligthOffHour) {
        this.ligthOffHour = ligthOffHour;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public Long getSectionId() {
        return sectionId;
    }

    public void setSectionId(Long sectionId) {
        this.sectionId = sectionId;
    }

    public Long getPolygonId() {
        return polygonId;
    }

    public void setPolygonId(Long polygonId) {
        this.polygonId = polygonId;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "DeviceUpdateView{" +
                "timeBeforeNotify=" + timeBeforeNotify +
                ", timeToMarkAsDisconnected=" + timeToMarkAsDisconnected +
                ", ligthOnHour='" + ligthOnHour + '\'' +
                ", ligthOffHour='" + ligthOffHour + '\'' +
                ", projectId=" + projectId +
                ", sectionId=" + sectionId +
                ", polygonId=" + polygonId +
                ", active=" + active +
                '}';
    }
}
