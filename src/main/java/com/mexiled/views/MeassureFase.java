package com.mexiled.views;

import java.io.Serializable;

public class MeassureFase implements Serializable {

    //voltage
    private Double voltaje;
    //amperes
    private Double corriente;
    //watts
    private Double potencia;


    public Double getVoltaje() {
        return voltaje;
    }

    public void setVoltaje(Double voltaje) {
        this.voltaje = voltaje;
    }

    public Double getCorriente() {
        return corriente;
    }

    public void setCorriente(Double corriente) {
        this.corriente = corriente;
    }

    public Double getPotencia() {
        return potencia;
    }

    public void setPotencia(Double potencia) {
        this.potencia = potencia;
    }

    @Override
    public String toString() {
        return "MeassureFase{" +
                "voltaje=" + voltaje +
                ", corriente=" + corriente +
                ", potencia=" + potencia +
                '}';
    }
}
