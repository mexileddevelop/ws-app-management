package com.mexiled.views;


import org.hibernate.validator.constraints.NotEmpty;
/**
 * @author Victor de la Cruz
 * @version 1.0.0
 */
public class ModulePermissionView {

    private Long modulePermissionId;
    @NotEmpty(message = "Nombre de permiso requerido")
    private String name;
    @NotEmpty(message = "Código de permiso requerido")
    private String code;

    public Long getModulePermissionId() {
        return modulePermissionId;
    }

    public void setModulePermissionId(Long modulePermissionId) {
        this.modulePermissionId = modulePermissionId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "ModulePermissionView{" +
                "modulePermissionId=" + modulePermissionId +
                ", name='" + name + '\'' +
                ", code='" + code + '\'' +
                '}';
    }
}
