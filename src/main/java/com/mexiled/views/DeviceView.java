package com.mexiled.views;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

public class DeviceView implements Serializable {

    private Long deviceId;
    @NotEmpty
    private String macAddress;
    private String code;
    private Double latitude;
    private Double longitude;
    private Date createdDate = new Date();
    private String installationStatus;//TODO view catalog
    private Integer timeBeforeNotify;
    private Integer timeToMarkAsDisconnected;
    private Integer lamps;
    @NotNull(message = "Especifique el proyecto del dispositivo")
    private ProjectView project;
    @NotNull(message = "Especifique la sección del dispositivo")
    private SectionView section;
    @NotNull(message = "Especifique el poligono del dispositivo")
    private PolygonView polygonView;
    private String ligthOnHour;
    private String ligthOffHour;
    @NotEmpty(message = "Ingrese el número de control")
    private String controlNumber;

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getInstallationStatus() {
        return installationStatus;
    }

    public void setInstallationStatus(String installationStatus) {
        this.installationStatus = installationStatus;
    }

    public Integer getTimeBeforeNotify() {
        return timeBeforeNotify;
    }

    public void setTimeBeforeNotify(Integer timeBeforeNotify) {
        this.timeBeforeNotify = timeBeforeNotify;
    }

    public Integer getTimeToMarkAsDisconnected() {
        return timeToMarkAsDisconnected;
    }

    public void setTimeToMarkAsDisconnected(Integer timeToMarkAsDisconnected) {
        this.timeToMarkAsDisconnected = timeToMarkAsDisconnected;
    }

    public Integer getLamps() {
        return lamps;
    }

    public void setLamps(Integer lamps) {
        this.lamps = lamps;
    }

    public ProjectView getProject() {
        return project;
    }

    public void setProject(ProjectView project) {
        this.project = project;
    }

    public SectionView getSection() {
        return section;
    }

    public void setSection(SectionView section) {
        this.section = section;
    }

    public PolygonView getPolygonView() {
        return polygonView;
    }

    public void setPolygonView(PolygonView polygonView) {
        this.polygonView = polygonView;
    }

    public String getLigthOnHour() {
        return ligthOnHour;
    }

    public void setLigthOnHour(String ligthOnHour) {
        this.ligthOnHour = ligthOnHour;
    }

    public String getLigthOffHour() {
        return ligthOffHour;
    }

    public void setLigthOffHour(String ligthOffHour) {
        this.ligthOffHour = ligthOffHour;
    }

    public String getControlNumber() {
        return controlNumber;
    }

    public void setControlNumber(String controlNumber) {
        this.controlNumber = controlNumber;
    }

    @Override
    public String toString() {
        return "DeviceView{" +
                "deviceId=" + deviceId +
                ", macAddress='" + macAddress + '\'' +
                ", code='" + code + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", createdDate=" + createdDate +
                ", installationStatus='" + installationStatus + '\'' +
                ", timeBeforeNotify=" + timeBeforeNotify +
                ", timeToMarkAsDisconnected=" + timeToMarkAsDisconnected +
                ", lamps=" + lamps +
                ", project=" + project +
                ", section=" + section +
                ", polygonView=" + polygonView +
                ", ligthOnHour='" + ligthOnHour + '\'' +
                ", ligthOffHour='" + ligthOffHour + '\'' +
                '}';
    }
}
