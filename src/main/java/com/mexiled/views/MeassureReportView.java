package com.mexiled.views;

import java.io.Serializable;
import java.util.Date;

public class MeassureReportView implements Serializable{

    private Long id;
    private MeassureFase fase1;
    private MeassureFase fase2;
    private Double potenciaAcumulada;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public MeassureFase getFase1() {
        return fase1;
    }

    public void setFase1(MeassureFase fase1) {
        this.fase1 = fase1;
    }

    public MeassureFase getFase2() {
        return fase2;
    }

    public void setFase2(MeassureFase fase2) {
        this.fase2 = fase2;
    }

    public Double getPotenciaAcumulada() {
        return potenciaAcumulada;
    }

    public void setPotenciaAcumulada(Double potenciaAcumulada) {
        this.potenciaAcumulada = potenciaAcumulada;
    }

    @Override
    public String toString() {
        return "MeassureReportView{" +
                "id=" + id +
                ", fase1=" + fase1 +
                ", fase2=" + fase2 +
                ", potenciaAcumulada=" + potenciaAcumulada +
                '}';
    }
}
