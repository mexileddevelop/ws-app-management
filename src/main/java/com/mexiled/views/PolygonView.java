package com.mexiled.views;


import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
/**
 * @author Victor de la Cruz
 * @version 1.0.0
 */
public class PolygonView implements Serializable{

    private Long polygonId;
    @NotEmpty(message = "Agregue el nombre del poligono")
    private String name;
    @NotEmpty(message = "Agregue el código del poligono")
    private String code;
    private Date createdDate = new Date();
    private Boolean active = Boolean.TRUE;

    @NotNull(message = "Agregue la sección del poligono")
    private SectionView section;

    public Long getPolygonId() {
        return polygonId;
    }

    public void setPolygonId(Long polygonId) {
        this.polygonId = polygonId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public SectionView getSection() {
        return section;
    }

    public void setSection(SectionView section) {
        this.section = section;
    }

    @Override
    public String toString() {
        return "PolygonView{" +
                "polygonId=" + polygonId +
                ", name='" + name + '\'' +
                ", code='" + code + '\'' +
                ", createdDate=" + createdDate +
                ", active=" + active +
                ", section=" + section +
                '}';
    }
}
