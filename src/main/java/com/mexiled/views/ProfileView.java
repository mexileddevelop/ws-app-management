package com.mexiled.views;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * @author Victor de la Cruz
 * @version 1.0.0
 * **/
public class ProfileView implements Serializable {

    private Long profileId;
    private String profileName;
    private Boolean active = Boolean.TRUE;
    private List<NodeTree> tree = new ArrayList<>();
    private List<Long> permissions;
    private Date createdDate = new Date();

    public Long getProfileId() {
        return profileId;
    }

    public void setProfileId(Long profileId) {
        this.profileId = profileId;
    }

    public String getProfileName() {
        return profileName;
    }

    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public List<NodeTree> getTree() {
        return tree;
    }

    public void setTree(List<NodeTree> tree) {
        this.tree = tree;
    }

    public List<Long> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<Long> permissions) {
        this.permissions = permissions;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Override
    public String toString() {
        return "ProfileView{" +
                "profileId=" + profileId +
                ", profileName='" + profileName + '\'' +
                ", active=" + active +
                ", tree=" + tree +
                ",permissions="+permissions+
                '}';
    }
}
