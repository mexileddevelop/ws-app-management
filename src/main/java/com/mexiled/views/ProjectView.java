package com.mexiled.views;

import org.hibernate.validator.constraints.NotEmpty;
import java.io.Serializable;
import java.util.Date;

public class ProjectView implements Serializable{

    private Long projectId;
    @NotEmpty(message = "Ingrese el nombre del proyecto")
    private String name;
    @NotEmpty(message = "Ingrese el código para el proyecto")
    private String code;
    private Date createdDate = new Date();
    private Boolean active = Boolean.TRUE;

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "ProjectView{" +
                "projectId=" + projectId +
                ", name='" + name + '\'' +
                ", code='" + code + '\'' +
                ", createdDate=" + createdDate +
                ", active=" + active +
                '}';
    }
}
