package com.mexiled.views;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Victor de la Cruz
 * @version 1.0.0
 * **/
public class UserAppView implements Serializable{

    private Long userId;

    @NotEmpty(message = "El usuario es obligatorio")
    private String username;

    @NotEmpty(message = "El nombre es obligatorio")
    private String name;

    private String email;

    private String password;
    private Date createdDate;
    @NotNull(message = "Es necesario el perfil de usuario")
    private Long profileId;
    private String profileName;
    private Boolean active = Boolean.TRUE;

    //TODO verificar relación usuario - projectos

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Long getProfileId() {
        return profileId;
    }

    public void setProfileId(Long profileId) {
        this.profileId = profileId;
    }

    public String getProfileName() {
        return profileName;
    }

    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "UserAppView{" +
                "userId=" + userId +
                ", username='" + username + '\'' +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", createdDate=" + createdDate +
                ", profileId=" + profileId +
                ", profileName='" + profileName + '\'' +
                ", active=" + active +
                '}';
    }
}
