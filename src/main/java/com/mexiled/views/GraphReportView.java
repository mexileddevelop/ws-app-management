package com.mexiled.views;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GraphReportView implements Serializable{
    private Long deviceId;
    private String code;
    private Double acumulattedWatts=0.0;
    private Double lastWatts=0.0;
    private String status;
    private MeassureGraphView lastRecords;
    private List<MeassureGraphView> records = new ArrayList<>();

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Double getAcumulattedWatts() {
        return acumulattedWatts;
    }

    public void setAcumulattedWatts(Double acumulattedWatts) {
        this.acumulattedWatts = acumulattedWatts;
    }

    public Double getLastWatts() {
        return lastWatts;
    }

    public void setLastWatts(Double lastWatts) {
        this.lastWatts = lastWatts;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public MeassureGraphView getLastRecords() {
        return lastRecords;
    }

    public void setLastRecords(MeassureGraphView lastRecords) {
        this.lastRecords = lastRecords;
    }

    public List<MeassureGraphView> getRecords() {
        return records;
    }

    public void setRecords(List<MeassureGraphView> records) {
        this.records = records;
    }

    @Override
    public String toString() {
        return "GraphReportView{" +
                "deviceId=" + deviceId +
                ", code='" + code + '\'' +
                ", acumulattedWatts=" + acumulattedWatts +
                ", lastWatts=" + lastWatts +
                ", status=" + status +
                ", lastRecords=" + lastRecords +
                ", records=" + records +
                '}';
    }
}
