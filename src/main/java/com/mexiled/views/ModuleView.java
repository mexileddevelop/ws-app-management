package com.mexiled.views;

import org.hibernate.validator.constraints.NotEmpty;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Victor de la Cruz
 * @version 1.0.0
 */
public class ModuleView {

    private Long moduleId;
    @NotEmpty(message = "Nombre de módulo requerido")
    private String name;

    private List<ModulePermissionView> permissions = new ArrayList<>();

    public Long getModuleId() {
        return moduleId;
    }

    public void setModuleId(Long moduleId) {
        this.moduleId = moduleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ModulePermissionView> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<ModulePermissionView> permissions) {
        this.permissions = permissions;
    }

    @Override
    public String toString() {
        return "ModuleView{" +
                "moduleId=" + moduleId +
                ", name='" + name + '\'' +
                ", permissions=" + permissions +
                '}';
    }
}
