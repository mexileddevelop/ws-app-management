package com.mexiled.views;

import org.hibernate.validator.constraints.NotEmpty;

import java.io.Serializable;
import java.util.Date;


public class SectionView implements Serializable{
    private Long sectionId;
    @NotEmpty(message = "Proporcione el nombre de la sección")
    private String name;
    @NotEmpty(message = "Agreue un código a la sección")
    private String code;
    private Boolean active = Boolean.TRUE;
    private Date createdDate = new Date();
    private ProjectView project;

    public Long getSectionId() {
        return sectionId;
    }

    public void setSectionId(Long sectionId) {
        this.sectionId = sectionId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public ProjectView getProject() {
        return project;
    }

    public void setProject(ProjectView project) {
        this.project = project;
    }

    @Override
    public String toString() {
        return "SectionView{" +
                "sectionId=" + sectionId +
                ", name='" + name + '\'' +
                ", code='" + code + '\'' +
                ", active=" + active +
                ", createdDate=" + createdDate +
                ", project=" + project +
                '}';
    }
}
