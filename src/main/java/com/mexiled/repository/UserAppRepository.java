package com.mexiled.repository;

import com.mexiled.domain.UserApp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * @author Victor de la Cruz
 * @version 1.0.0
 */
public interface UserAppRepository extends JpaRepository<UserApp,Long>,JpaSpecificationExecutor{

    UserApp findByUsername(String username) throws Exception;
}
