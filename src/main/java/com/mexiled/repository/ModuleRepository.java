package com.mexiled.repository;

import com.mexiled.domain.Module;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * @author Victor de la Cruz
 * @version 1.0.0
 */
public interface ModuleRepository extends JpaRepository<Module,Long>,JpaSpecificationExecutor{
}
