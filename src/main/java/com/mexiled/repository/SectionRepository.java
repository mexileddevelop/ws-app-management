package com.mexiled.repository;


import com.mexiled.domain.Section;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author Victor de la Cruz
 * @version 1.0.0
 */
public interface SectionRepository extends JpaRepository<Section,Long>,JpaSpecificationExecutor{
}
