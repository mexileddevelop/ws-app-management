package com.mexiled.repository;

import com.mexiled.domain.DeviceHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface DeviceHistoryRepository extends JpaRepository<DeviceHistory,Long>,JpaSpecificationExecutor{
}
