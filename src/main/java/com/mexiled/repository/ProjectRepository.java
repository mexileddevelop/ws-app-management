package com.mexiled.repository;


import com.mexiled.domain.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;


public interface ProjectRepository extends JpaRepository<Project,Long>,JpaSpecificationExecutor{
}
