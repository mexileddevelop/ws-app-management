package com.mexiled.repository;

import com.mexiled.domain.DeviceRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Date;
import java.util.List;

/**
 * @author Victor de la Cruz
 * @version 1.0.0
 */
public interface DeviceRecordRepository extends JpaRepository<DeviceRecord,Long>,JpaSpecificationExecutor{

    List<DeviceRecord> findFirst2ByDevice_DeviceIdOrderByCreatedDateDesc(Long deviceId);
    List<DeviceRecord> findByDevice_DeviceIdAndCreatedDateAfterAndDeviceRecordIdIsLessThanEqualOrderByCreatedDateAsc(Long deviceId, Date createdDate,Long deviceRecordId);
}
