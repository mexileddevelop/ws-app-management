package com.mexiled.repository;

import com.mexiled.domain.ProfilePermission;
import com.mexiled.domain.ProfilePermissionId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Victor de la Cruz
 * @version 1.0.0
 */
public interface ProfilePermissionRepository extends JpaRepository<ProfilePermission,ProfilePermissionId>{
}
