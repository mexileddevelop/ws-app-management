package com.mexiled.repository;

import com.mexiled.domain.Polygon;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author Victor de la Cruz
 * @version 1.0.0
 */
public interface PolygonRepository extends JpaRepository<Polygon,Long>,JpaSpecificationExecutor{
}
