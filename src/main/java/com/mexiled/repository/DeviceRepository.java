package com.mexiled.repository;

import com.mexiled.domain.Device;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author Victor de la Cruz
 * @version 1.0.0
 */
public interface DeviceRepository extends JpaRepository<Device,Long>,JpaSpecificationExecutor{

    Device findByMacAddress(String macAddress) throws Exception;

}
