package com.mexiled.repository;

import com.mexiled.domain.ModulePermission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Victor de la Cruz
 * @version 1.0.0
 */
public interface ModulePermissionRepository extends JpaRepository<ModulePermission,Long>{
}
