package com.mexiled.repository;

import com.mexiled.domain.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Victor de la Cruz
 * @version 1.0.0
 */
public interface ProfileRepository extends JpaRepository<Profile,Long>{
}
