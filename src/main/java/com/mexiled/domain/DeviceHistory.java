package com.mexiled.domain;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table
public class DeviceHistory implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long deviceHistoryId;
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastRegistered;
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastActivity;

    @Temporal(TemporalType.TIMESTAMP)
    private Date firstRegistered;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(nullable = false,name = "deviceId")
    private Device device;

    private String historyStatus = "";

    public Long getDeviceHistoryId() {
        return deviceHistoryId;
    }

    public void setDeviceHistoryId(Long deviceHistoryId) {
        this.deviceHistoryId = deviceHistoryId;
    }

    public Date getLastRegistered() {
        return lastRegistered;
    }

    public void setLastRegistered(Date lastRegistered) {
        this.lastRegistered = lastRegistered;
    }

    public Date getLastActivity() {
        return lastActivity;
    }

    public void setLastActivity(Date lastActivity) {
        this.lastActivity = lastActivity;
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    public Date getFirstRegistered() {
        return firstRegistered;
    }

    public void setFirstRegistered(Date firstRegistered) {
        this.firstRegistered = firstRegistered;
    }

    public String getHistoryStatus() {
        return historyStatus;
    }

    public void setHistoryStatus(String historyStatus) {
        this.historyStatus = historyStatus;
    }

    @Override
    public String toString() {
        return "DeviceHistory{" +
                "deviceHistoryId=" + deviceHistoryId +
                ", lastRegistered=" + lastRegistered +
                ", lastActivity=" + lastActivity +
                '}';
    }
}
