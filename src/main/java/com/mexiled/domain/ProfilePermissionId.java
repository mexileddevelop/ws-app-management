package com.mexiled.domain;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * @author Victor de la Cruz
 * @version 1.0.0
 * **/
@Embeddable
public class ProfilePermissionId implements Serializable{

    @ManyToOne
    private Profile profile;

    @ManyToOne
    private ModulePermission modulePermission;

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public ModulePermission getModulePermission() {
        return modulePermission;
    }

    public void setModulePermission(ModulePermission modulePermission) {
        this.modulePermission = modulePermission;
    }
}
