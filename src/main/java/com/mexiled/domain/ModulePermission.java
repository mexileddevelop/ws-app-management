package com.mexiled.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Victor de la Cruz
 * @version 1.0.0
 * **/
@Entity
public class ModulePermission implements Serializable{

    @Id()
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long modulePermissionId;
    @NotEmpty()
    private String name;
    @NotEmpty()
    @Column(unique = true)
    private String code;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "moduleId",nullable = false)
    private Module module;

    @OneToMany(fetch = FetchType.LAZY,mappedBy = "permissionId.modulePermission",cascade = {CascadeType.ALL})
    @JsonIgnore
    private List<ProfilePermission> permissionList = new ArrayList<>();

    public Long getModulePermissionId() {
        return modulePermissionId;
    }

    public void setModulePermissionId(Long modulePermissionId) {
        this.modulePermissionId = modulePermissionId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Module getModule() {
        return module;
    }

    public void setModule(Module module) {
        this.module = module;
    }

    public List<ProfilePermission> getPermissionList() {
        return permissionList;
    }

    public void setPermissionList(List<ProfilePermission> permissionList) {
        this.permissionList = permissionList;
    }

    @Override
    public String toString() {
        return "ModulePermission{" +
                "modulePermissionId=" + modulePermissionId +
                ", name='" + name + '\'' +
                ", code='" + code + '\'' +
                '}';
    }
}
