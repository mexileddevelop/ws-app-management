package com.mexiled.domain;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.io.Serializable;


/**
 * @author Victor de la Cruz
 * @version 1.0.0
 * **/
@Entity
public class DeviceExtraData implements Serializable{

    @Id()
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long deviceExtraId;
    @NotEmpty
    @Column(name = "key_")
    private String key;
    @Column(name = "value_")
    @NotEmpty
    private String value;

    @OneToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JoinColumn(name = "fieldConfigurationId",nullable = false)
    private FieldConfiguration field;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(nullable = false,name = "deviceId")
    private Device device;

    public Long getDeviceExtraId() {
        return deviceExtraId;
    }

    public void setDeviceExtraId(Long deviceExtraId) {
        this.deviceExtraId = deviceExtraId;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public FieldConfiguration getField() {
        return field;
    }

    public void setField(FieldConfiguration field) {
        this.field = field;
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    @Override
    public String toString() {
        return "DeviceExtraData{" +
                "deviceEtraId=" + deviceExtraId +
                ", key='" + key + '\'' +
                ", value='" + value + '\'' +
                ", field=" + field +
                '}';
    }
}
