package com.mexiled.domain;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


/**
 * @author Victor de la Cruz
 * @version 1.0.0
 * **/
@Entity
@DynamicUpdate
public class Device implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long deviceId;
    @NotEmpty
    @Column(unique = true,nullable = false)
    private String macAddress;
    @Column(unique = true,nullable = false)
    private String code;
    private Double latitude;
    private Double longitude;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate = new Date();
    @Temporal(TemporalType.TIME)
    private Date ligthOnHour;
    @Temporal(TemporalType.TIME)
    private Date ligthOffHour;
    private String installationStatus="Proyectado";//TODO view catalog
    private String deviceStatus="";
    private Integer timeBeforeNotify;
    private Integer timeToMarkAsDisconnected;
    private Integer lamps;
    @NotEmpty
    private String controlNumber;
    @OneToOne(fetch = FetchType.LAZY,cascade = CascadeType.DETACH)
    @JoinColumn(nullable = false,name = "projectId")
    private Project project;
    @OneToOne(fetch = FetchType.LAZY,cascade = CascadeType.DETACH)
    @JoinColumn(nullable = false,name = "sectionId")
    private Section section;
    @JoinColumn(nullable = false,name = "polygonId")
    @OneToOne(fetch = FetchType.LAZY,cascade = CascadeType.DETACH)
    private Polygon polygon;

    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "device")
    private List<DeviceExtraData> dataList = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY,mappedBy = "device")
    private List<DeviceRecord> deviceRecords = new ArrayList<>();

    @OneToOne(mappedBy = "device",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private DeviceHistory deviceHistory;

    @Column(columnDefinition = "int default 0")
    private Integer numberOffRequests = 0;

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getInstallationStatus() {
        return installationStatus;
    }

    public void setInstallationStatus(String installationStatus) {
        this.installationStatus = installationStatus;
    }

    public Integer getTimeBeforeNotify() {
        return timeBeforeNotify;
    }

    public void setTimeBeforeNotify(Integer timeBeforeNotify) {
        this.timeBeforeNotify = timeBeforeNotify;
    }

    public Integer getTimeToMarkAsDisconnected() {
        return timeToMarkAsDisconnected;
    }

    public void setTimeToMarkAsDisconnected(Integer timeToMarkAsDisconnected) {
        this.timeToMarkAsDisconnected = timeToMarkAsDisconnected;
    }

    public Integer getLamps() {
        return lamps;
    }

    public void setLamps(Integer lamps) {
        this.lamps = lamps;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public Section getSection() {
        return section;
    }

    public void setSection(Section section) {
        this.section = section;
    }

    public Polygon getPolygon() {
        return polygon;
    }

    public void setPolygon(Polygon polygon) {
        this.polygon = polygon;
    }

    public List<DeviceExtraData> getDataList() {
        return dataList;
    }

    public void setDataList(List<DeviceExtraData> dataList) {
        this.dataList = dataList;
    }

    public List<DeviceRecord> getDeviceRecords() {
        return deviceRecords;
    }

    public void setDeviceRecords(List<DeviceRecord> deviceRecords) {
        this.deviceRecords = deviceRecords;
    }

    public Date getLigthOnHour() {
        return ligthOnHour;
    }

    public void setLigthOnHour(Date ligthOnHour) {
        this.ligthOnHour = ligthOnHour;
    }

    public Date getLigthOffHour() {
        return ligthOffHour;
    }

    public void setLigthOffHour(Date ligthOffHour) {
        this.ligthOffHour = ligthOffHour;
    }

    public String getControlNumber() {
        return controlNumber;
    }

    public void setControlNumber(String controlNumber) {
        this.controlNumber = controlNumber;
    }

    public DeviceHistory getDeviceHistory() {
        return deviceHistory;
    }

    public void setDeviceHistory(DeviceHistory deviceHistory) {
        this.deviceHistory = deviceHistory;
    }

    public String getDeviceStatus() {
        return deviceStatus;
    }

    public void setDeviceStatus(String deviceStatus) {
        this.deviceStatus = deviceStatus;
    }

    public Integer getNumberOffRequests() {
        return numberOffRequests != null ? numberOffRequests : 0;//prevent null pointer
    }

    public void setNumberOffRequests(Integer numberOffRequests) {
        this.numberOffRequests = numberOffRequests;
    }

    @Override
    public String toString() {
        return "Device{" +
                "deviceId=" + deviceId +
                ", macAddress='" + macAddress + '\'' +
                ", code='" + code + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", createdDate=" + createdDate +
                ", ligthOnHour=" + ligthOnHour +
                ", ligthOffHour=" + ligthOffHour +
                ", installationStatus='" + installationStatus + '\'' +
                ", timeBeforeNotify=" + timeBeforeNotify +
                ", timeToMarkAsDisconnected=" + timeToMarkAsDisconnected +
                ", lamps=" + lamps +
                ", project=" + project +
                ", section=" + section +
                ", polygon=" + polygon +
                ", dataList=" + dataList +
                ", deviceRecords=" + deviceRecords +
                '}';
    }
}
