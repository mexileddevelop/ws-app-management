package com.mexiled.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Victor de la Cruz
 * @version 1.0.0
 * **/
@Entity
public class Profile implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long profileId;
    @NotEmpty
    private String profileName;
    @NotNull
    private Boolean active = Boolean.TRUE;

    private Date createdDate = new Date();

    @OneToMany(cascade = {CascadeType.ALL},fetch = FetchType.LAZY,mappedBy = "permissionId.profile")
    private List<ProfilePermission> permissionList = new ArrayList<>();

    public Long getProfileId() {
        return profileId;
    }

    public void setProfileId(Long profileId) {
        this.profileId = profileId;
    }

    public String getProfileName() {
        return profileName;
    }

    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public List<ProfilePermission> getPermissionList() {
        return permissionList;
    }

    public void setPermissionList(List<ProfilePermission> permissionList) {
        this.permissionList = permissionList;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Override
    public String toString() {
        return "Profile{" +
                "profileId=" + profileId +
                ", profileName='" + profileName + '\'' +
                ", active=" + active +
                ", permissionList=" + permissionList +
                '}';
    }
}
