package com.mexiled.domain;


import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @author Victor de la Cruz
 * @version 1.0.0
 * **/
@Entity
public class RecordMeasurement {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long recordMeasurementId;
    @NotEmpty
    private String type;
    @NotNull
    private Double voltage;
    @NotNull
    private Double amperes;
    @NotNull
    private Double watts;


    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "deviceRecordId",nullable = false)
    private DeviceRecord deviceRecord;

    public Long getRecordMeasurementId() {
        return recordMeasurementId;
    }

    public void setRecordMeasurementId(Long recordMeasurementId) {
        this.recordMeasurementId = recordMeasurementId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getVoltage() {
        return voltage;
    }

    public void setVoltage(Double voltage) {
        this.voltage = voltage;
    }

    public Double getAmperes() {
        return amperes;
    }

    public void setAmperes(Double amperes) {
        this.amperes = amperes;
    }

    public Double getWatts() {
        return watts;
    }

    public void setWatts(Double watts) {
        this.watts = watts;
    }


    public DeviceRecord getDeviceRecord() {
        return deviceRecord;
    }

    public void setDeviceRecord(DeviceRecord deviceRecord) {
        this.deviceRecord = deviceRecord;
    }

    @Override
    public String toString() {
        return "RecordMeasurement{" +
                "recordMeasurementId=" + recordMeasurementId +
                ", type='" + type + '\'' +
                ", voltage=" + voltage +
                ", amperes=" + amperes +
                ", watts=" + watts +
                '}';
    }
}
