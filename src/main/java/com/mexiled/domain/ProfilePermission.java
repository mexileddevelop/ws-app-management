package com.mexiled.domain;

import javax.persistence.*;
import java.util.Date;

/**
 * @author Victor de la Cruz
 * @version 1.0.0
 * **/
@Entity
@AssociationOverrides({
        @AssociationOverride(name = "permissionId.profile",joinColumns =  @JoinColumn(name="profile_id")),
        @AssociationOverride(name= "permissionId.modulePermission",joinColumns = @JoinColumn(name = "module_permission_id"))
})
public class ProfilePermission {

    @EmbeddedId
    private ProfilePermissionId permissionId = new ProfilePermissionId();

    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    public ProfilePermissionId getPermissionId() {
        return permissionId;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setPermissionId(ProfilePermissionId permissionId) {
        this.permissionId = permissionId;
    }

    @Transient
    public Profile getProfile(){
        return getPermissionId().getProfile();
    }

    public void setProfile(Profile p){
        getPermissionId().setProfile(p);
    }

    @Transient
    public ModulePermission getModulePermission(){
        return getPermissionId().getModulePermission();
    }

    public void setModulePermission(ModulePermission m){
        this.getPermissionId().setModulePermission(m);
    }
}
