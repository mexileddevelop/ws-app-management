package com.mexiled.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * @author Victor de la Cruz
 * @version 1.0.0
 * **/
@Entity
public class DeviceRecord implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long deviceRecordId;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    @NotNull
    private Double acumulattedWatts;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(nullable = false,name = "deviceId")
    private Device device;

    @OneToMany(mappedBy = "deviceRecord",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<RecordMeasurement> measurements = new ArrayList<>();

    private String deviceStatusOnRecord = "";

    public Long getDeviceRecordId() {
        return deviceRecordId;
    }

    public void setDeviceRecordId(Long deviceRecordId) {
        this.deviceRecordId = deviceRecordId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    public List<RecordMeasurement> getMeasurements() {
        return measurements;
    }

    public void setMeasurements(List<RecordMeasurement> measurements) {
        this.measurements = measurements;
    }

    public Double getAcumulattedWatts() {
        return acumulattedWatts;
    }

    public void setAcumulattedWatts(Double acumulattedWatts) {
        this.acumulattedWatts = acumulattedWatts;
    }

    public String getDeviceStatusOnRecord() {
        return deviceStatusOnRecord;
    }

    public void setDeviceStatusOnRecord(String deviceStatusOnRecord) {
        this.deviceStatusOnRecord = deviceStatusOnRecord;
    }

    @Override
    public String toString() {
        return "DeviceRecord{" +
                "deviceRecordId=" + deviceRecordId +
                ", createdDate=" + createdDate +
                ", acumulattedWatts=" + acumulattedWatts +
                ", measurements=" + measurements +
                '}';
    }
}
