package com.mexiled.domain;


import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Victor de la Cruz
 * @version 1.0.0
 * **/
@Entity
@Table(name = "module_")
public class Module implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long moduleId;

    @NotEmpty()
    private String name;

    @OneToMany(mappedBy = "module",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private List<ModulePermission> modulePermissions = new ArrayList<>();

    public Long getModuleId() {
        return moduleId;
    }

    public void setModuleId(Long moduleId) {
        this.moduleId = moduleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ModulePermission> getModulePermissions() {
        return modulePermissions;
    }

    public void setModulePermissions(List<ModulePermission> modulePermissions) {
        this.modulePermissions = modulePermissions;
    }

    @Override
    public String toString() {
        return "Module{" +
                "moduleId=" + moduleId +
                ", name='" + name + '\'' +
                ", modulePermissions=" + modulePermissions +
                '}';
    }
}
