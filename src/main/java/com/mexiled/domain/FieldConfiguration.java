package com.mexiled.domain;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.io.Serializable;


/**
 * @author Victor de la Cruz
 * @version 1.0.0
 * **/
@Entity
public class FieldConfiguration implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long fieldConfigurationId;
    @Column(nullable = false,unique = true,name = "key_")
    private String key;
    @NotEmpty
    @Column(name = "legend_")
    private String legend;
    private Boolean active = Boolean.TRUE;

    public Long getFieldConfigurationId() {
        return fieldConfigurationId;
    }

    public void setFieldConfigurationId(Long fieldConfigurationId) {
        this.fieldConfigurationId = fieldConfigurationId;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getLegend() {
        return legend;
    }

    public void setLegend(String legend) {
        this.legend = legend;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "FieldConfiguration{" +
                "fieldConfigurationId=" + fieldConfigurationId +
                ", key='" + key + '\'' +
                ", legend='" + legend + '\'' +
                ", active=" + active +
                '}';
    }
}
