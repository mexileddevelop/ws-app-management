package com.mexiled.domain;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Victor de la Cruz
 * @version 1.0.0
 * **/
@Entity
public class Polygon implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long polygonId;
    @NotEmpty
    private String name;
    @NotEmpty
    private String code;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate = new Date();
    private Boolean active = Boolean.TRUE;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(nullable = false,name = "sectionId")
    private Section section;

    public Long getPolygonId() {
        return polygonId;
    }

    public void setPolygonId(Long polygonId) {
        this.polygonId = polygonId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Section getSection() {
        return section;
    }

    public void setSection(Section section) {
        this.section = section;
    }

    @Override
    public String toString() {
        return "Polygon{" +
                "polygonId=" + polygonId +
                ", name='" + name + '\'' +
                ", code='" + code + '\'' +
                ", createdDate=" + createdDate +
                ", active=" + active +
                '}';
    }
}
