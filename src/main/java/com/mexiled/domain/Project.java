package com.mexiled.domain;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * @author Victor de la Cruz
 * @version 1.0.0
 * **/
@Entity
public class Project implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long projectId;
    @NotEmpty
    private String name;
    @NotEmpty
    @Column(unique = true)
    private String code;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate = new Date();
    private Boolean active = Boolean.TRUE;

    @OneToMany(cascade = CascadeType.ALL,mappedBy = "project",fetch = FetchType.LAZY)
    private List<Section> sections = new ArrayList<>();

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public List<Section> getSeactions() {
        return sections;
    }

    public void setSeactions(List<Section> seactions) {
        this.sections = seactions;
    }

    @Override
    public String toString() {
        return "Project{" +
                "projectId=" + projectId +
                ", name='" + name + '\'' +
                ", code='" + code + '\'' +
                ", createdDate=" + createdDate +
                ", active=" + active +
                ", seactions=" + sections +
                '}';
    }
}
