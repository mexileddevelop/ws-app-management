CREATE DATABASE devices_management;


--create table device_extra_data (device_extra_id bigint not null auto_increment, key varchar(255) not null, value varchar(255) not null, device_id bigint not null, field_configuration_id bigint not null, primary key (device_extra_id))


-- crear tablas oauth
create table oauth_client_token (
  token_id VARCHAR(255),
  token BLOB,
  authentication_id VARCHAR(255),
  user_name VARCHAR(255),
  client_id VARCHAR(255)
);

create table oauth_access_token (
  token_id VARCHAR(255),
  token BLOB,
  authentication_id VARCHAR(255),
  user_name VARCHAR(255),
  client_id VARCHAR(255),
  authentication BLOB,
  refresh_token VARCHAR(255)
);

create table oauth_refresh_token (
  token_id VARCHAR(255),
  token BLOB,
  authentication BLOB
);

create table oauth_code (
  code VARCHAR(255), authentication BLOB
);

-- use this for drop unused column
alter table record_measurement drop column acumulatted_watts;
alter table device_record drop column last_activity;
alter table device_record drop column last_registered;

-- agregar indice
create index idx_record_date ON device_record(created_date);
